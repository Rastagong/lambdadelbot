use lambdata;
db.createCollection("games");
db.games.createIndex({"gamecode":1}, {"unique":true});
db.games.createIndex({"players.username":1}, {"unique":true, "partialFilterExpression": { "players.username": { $exists: true } }});

db.createCollection("authRequests");
db.authRequests.createIndex({"state":1}, {"unique":true});

db.createCollection("hosts");
db.hosts.createIndex({"username":1}, {"unique":true});

db.createCollection("messages");
db.messages.createIndex({"name":1}, {"unique":true});

db.createCollection("config");
db.config.createIndex({"always_null":1}, {"unique":true});

db.createCollection("manualCycles");
db.manualCycles.createIndex({"gamecode":1}, {"unique":true});

db.createCollection("logs", { capped : true, size : 5000192 } );
