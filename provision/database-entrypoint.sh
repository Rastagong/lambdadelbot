#!/bin/bash
set -Eeuo pipefail

SECRETS_DB_FILEPATH=${SECRETS_DB_FILEPATH:-}
SECRETS_DB_ROOT_FILEPATH=${SECRETS_DB_ROOT_FILEPATH:-}

if [ ! -z "$SECRETS_DB_FILEPATH" ] && \
	[ ! -z "$SECRETS_DB_ROOT_FILEPATH" ] && \
	[ -f "$SECRETS_DB_FILEPATH" ] && \ 
	[ -f "$SECRETS_DB_ROOT_FILEPATH" ]  && \
	[ ! -z "$(< $SECRETS_DB_FILEPATH)" ] && \
	[ ! -z "$(< $SECRETS_DB_ROOT_FILEPATH)" ]; then
   echo "Both .env config files (SECRETS_DB_FILEPATH=\"$SECRETS_DB_ROOT_FILEPATH\" and SECRETS_DB_ROOT_FILEPATH=\"$SECRETS_DB_ROOT_FILEPATH\") exist!";
   echo "Sourcing them to set the Lambdadelbot database environments vars."
   source $SECRETS_DB_FILEPATH;
   source $SECRETS_DB_ROOT_FILEPATH;
   echo "Now the Lambdadelbot database environment vars should be set!"
else
   echo "The SECRETS_DB_FILEPATH=\"$SECRETS_DB_ROOT_FILEPATH\" and SECRETS_DB_ROOT_FILEPATH=\"$SECRETS_DB_ROOT_FILEPATH\" config vars aren't set (or the files don't exist)";
   echo "Expecting the Lambdadelbot database environment vars they should contain to be already set";
fi

echo "Attempting to set Bitnami environment vars using the Lambdadelbot database env vars (which should be set)";
echo "If it fails with 'unbound variable', it means the Lambdadelbot database env vars were not set"
export MONGODB_DATABASE="$LAMBDADELBOT_DATABASE_NAME";
export MONGODB_USERNAME="$LAMBDADELBOT_DATABASE_USERNAME";
export MONGODB_PASSWORD="$LAMBDADELBOT_DATABASE_PASSWORD";
export MONGODB_ROOT_PASSWORD="$LAMBDADELBOT_DATABASE_ROOT_PASSWORD";
echo "All Bitnami environment vars are set! Launching the Bitnami original entrypoint now.";
exec /opt/bitnami/scripts/mongodb/entrypoint.sh "$@"
