
# Todo
* Admin: form to add hosts, remove the line which makes someone a host from create_game in the controller
* Markdown documentation pages
* Version number on the left side of the page
* Formatting of messages when writing to wiki: take into account Markdown headers in blockquotes too
* Remove useless parts from docker-compose.yml
* Clear old todo

# Old Todo

## Players storage in DB
When adding a player, there should be 2 checks:
* That the player is not a regular user (a host or admin), because same problem, then reddit tokens would be duplicated in different parts
* That the player isn't involved in any other game (then problem would be games which reuse former players) => Done through unique index

## Fetch
* Fix the fact that last seen params aren't saved/all PMs are always collected
* Less important
	* Document the endTime/startTime
	* Global cycle must wait for individual game cycles to end
	* asyncio.wait for player routines (routes?) with a new <username> param, maybe <sent>/<received> too
	* Finding a solution for game cycle names being unique (maybe just timestamp hash+gamecode since no two game cycles at the same time)

