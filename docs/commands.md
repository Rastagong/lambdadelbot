# Logs of a given service
sudo docker logs $(sudo docker ps -a -f "name=lambda_worker" --format "{{.ID}}" | head -n 1)

# Updating a given service
sudo docker service update --force lambda_web 

# Updating the stack
sudo docker-compose config | sudo docker stack deploy --compose-file - lambda 


# Finding all calls to the HTTP client in the source code via grep and exporting them to HTML using aha
grep --color=always -nR "\.\(retrieveRedditTokens\|refreshRedditTokens\|revokeRedditTokens\|getRedditUsername\|getRedditUserInfo\|getSubredditInfo\|getSubredditMods\|getModdedSubreddits\|acceptWikiModInvite\|writeWikiPage\|getLatestWikiPageRevision\|getWikiPagePermissions\|setWikiPagePermissions\|toggleWikiRevisionVisibility\)" --include="*.py" . | aha  --black --title "Calls to HTTP Client" > ../../docs/clientCalls.html

