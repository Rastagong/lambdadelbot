#!/usr/bin/env python3
# coding: utf-8
import asyncio, sys
from lambdadelbot.web.web import web_entrypoint
from lambdadelbot.worker.worker import worker_entrypoint


def main(task:str) -> None:
    if task == "web":
        asyncio.run(web_entrypoint())
    elif task == "worker":
        asyncio.run(worker_entrypoint())
    else:
        raise ValueError(f"Unknown task: {task}")


if __name__ == "__main__":
    if len(sys.argv) < 2:
        raise ValueError(f"You must supply a task to launch (either 'web' or 'worker')")
    main(sys.argv[1])
