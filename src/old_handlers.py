
class GameLogsHandler(AdminHandler, route=r"/(.+)/admin/logs", name="adminLogs"):

    @HostOnlyPageValidator
    @GamePageValidator
    async def get(self, gamecode):
        logsToken = self.r.generateRandomString(url=False)
        logger.info(f"Generation the streaming logs token {logsToken} for game {gamecode}.")
        self.application.streamedLogs[gamecode] = logsToken
        self._renderPage("adminLogs", gameCode=gamecode, gameTitle=await self.r.getGameTitle(gamecode), token=logsToken)
        return


class GameStreamLogsHandler(BaseHandler, WebSocketHandler, route=r"/(.+)/admin/stream-logs", name="adminGameLogsStream"):
    introStr = "Fetch routine started with the following command: {}"

    def initialize(self):
        self.streaming, self.closed = False, False

    async def on_message(self, message):
        """We expect a single JSON opening <message>: the <game> and its <token>."""
        if self.streaming or self.closed: #Already opened or closed
            return
        message = self._validateJSON(message)
        if message is None:
            self.close()
            return
        game, token = message.get("game"), message.get("token")
        if not game or not token or game not in self.application.streamedLogs:
            self.close()
            return
        elif self.application.streamedLogs[game] != token:
            self.close()
            return
        self.application.streamedLogs.pop(game)
        logger.info(f"Logs token for {game} correctly received")
        self.streaming = True
        await self.streamLogs(game)
        return

    def on_close(self):
        self.streaming, self.closed = False, True

    def writeLog(self, doc, event="log"):
        if not self.streaming: #User may have left (WebSocket closed), so we check they're open
            return
        if event != "log":
            self.write_message({"message":doc, "event":event})
            return
        msg = f"{doc['asctime']} - {doc['levelname']} - {doc['msg']}"
        self.write_message({"message":msg, "event":event, "cn":doc.get("cycleName", "")})

    async def streamLogs(self, gamecode:str) -> None:
        logger.info("Starting up the stream of logs.")
        async for doc in self.r.findLogs(gamecode):
            self.writeLog(doc)
        self.writeLog("End.", event="close")
        logger.info("End of the stream of logs.")
        if self.streaming:
            self.close()

