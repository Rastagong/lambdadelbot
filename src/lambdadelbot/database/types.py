#coding:utf-8
from __future__ import annotations
import re
from abc import ABC, ABCMeta
from datetime import datetime
from typing import (Any, AsyncIterator, cast, Dict, final, Generic, Generator, Iterator, List, Literal, Mapping, Optional, overload,
        Pattern, Sequence, Tuple, Type, TypedDict, TYPE_CHECKING, TypeVar, Union)
from pymongo.cursor import CursorType
if TYPE_CHECKING:
    from pymongo import SortDirection
    from pymongo.cursor import CursorType, CT
else:
    SortDirection, CT = "SortDirection", "CT"
from ..constants.configuration import config
from ..constants.types import *
from .results import InsertOneResult, InsertManyResult, UpdateResultPositive, UpdateResultNegative, DeleteResultPositive, DeleteResultNegative


Filter = Literal["filter"]
UpdateSet = Literal["update_set"]
UpdatePush = Literal["update_push"]
UpdatePull = Literal["update_pull"]
UpdateIncrement = Literal["update_increment"]
Projection = Literal["projection"]
DocumentKind = TypeVar("DocumentKind", Filter, UpdateSet, UpdatePush, UpdatePull, UpdateIncrement, Projection, covariant=True)


GamesCollection = Literal["games"]
AuthRequestsCollection = Literal["auth_requests"]
HostsCollection = Literal["hosts"]
MessagesCollection = Literal["messages"]
ConfigCollection = Literal["config"]
ManualCyclesCollection = Literal["manualCycles"]
LogsCollection = Literal["logs"]
Collection = TypeVar("Collection", GamesCollection, AuthRequestsCollection, HostsCollection, MessagesCollection, ConfigCollection, ManualCyclesCollection, LogsCollection, covariant=True)


class Document(Dict[str, object], Generic[DocumentKind, Collection], metaclass=ABCMeta):
    pass
"""class DocumentMixinFilter(Document[Filter, Collection]):
    pass"""


@final
class N(ABC):
    pass

Nothing = Type[N]
V = TypeVar("V") 
O = Union[Nothing, V]


OperatorEach = Mapping[Literal["$each"], Sequence[V]]
PushValue = Union[V, OperatorEach[V]]
OPushValue = Union[Nothing, PushValue[V]]

DocFilter = Document[Filter, Collection]
DocPushed = Document[UpdatePush, Collection]
DocSetted = Document[UpdateSet, Collection]
DocIncremented = Document[UpdateIncrement, Collection]
DocPulled = Document[UpdatePull, Collection]
OperatorOr = Mapping[Literal["$or"], Sequence[Document[Filter, Collection]]]
OperatorAnd = Mapping[Literal["$and"], Sequence[Document[Filter, Collection]]]
FilterQuery = Union[OperatorOr[Collection], OperatorAnd[Collection], DocFilter[Collection]] 


UpdateDocumentSet = Mapping[Literal["$set"], DocSetted[Collection]]
UpdateDocumentPush = Mapping[Literal["$push"], DocPushed[Collection]]
UpdateDocumentPull = Mapping[Literal["$pull"], DocPulled[Collection]]
UpdateDocumentIncrement = Mapping[Literal["$inc"], DocIncremented[Collection]]
UpdateDocument = Union[UpdateDocumentSet[Collection], UpdateDocumentPush[Collection],
        UpdateDocumentPull[Collection], UpdateDocumentIncrement[Collection]]
StorageDocument = TypeVar("StorageDocument", bound=StrMapping)
StorageDocumentTotal = TypeVar("StorageDocumentTotal", bound=StrMapping)
OSD = Optional[StorageDocument]
OSDT = Optional[StorageDocumentTotal]
UOSD_T = Union[OSD, OSDT]



MessageKeys = Union[Literal["root_id"], Literal["createdOn"]]
LogKeys = Union[Literal["gamecode"], Literal["asctime"]]


class DBCursor(AsyncIterator[StorageDocument], Generic[Collection, StorageDocument]):
    alive:bool

    async def to_list(self, length:int) -> List[StorageDocument]:
        return None #type:ignore

    def __aiter__(self) -> AsyncIterator[StorageDocument]:
        return None #type:ignore

    async def __anext__(self) -> StorageDocument: 
        return None #type:ignore

    @overload
    def sort(self:DBCursor[MessagesCollection, StorageDocument], keys:List[Tuple[MessageKeys, SortDirection]]) -> DBCursor[Collection, StorageDocument]: ...
    @overload
    def sort(self:DBCursor[LogsCollection, StorageDocument], keys:List[Tuple[LogKeys, SortDirection]]) -> DBCursor[Collection, StorageDocument]: ...
    def sort(self, keys:Union[List[Tuple[LogKeys, SortDirection]], List[Tuple[MessageKeys, SortDirection]]]) -> DBCursor[Collection, StorageDocument]:
        return None #type:ignore

    @overload
    async def distinct(self:DBCursor[HostsCollection, StorageDocument], key:Literal["username"]) -> StrList: ... 
    @overload
    async def distinct(self:DBCursor[AuthRequestsCollection, StorageDocument], key:Literal["state"]) -> StrList: ... 
    @overload
    async def distinct(self:DBCursor[GamesCollection, StorageDocument], key:Literal["gamecode"]) -> StrList: ... 
    async def distinct(self, key:str) -> List[Any]:
        return None #type:ignore


class DBCollection(Generic[Collection, StorageDocument, StorageDocumentTotal]):

    @overload
    def find(self, filterQuery:FilterQuery[Collection], projection:None = None,
            cursor_type:CT = CursorType.NON_TAILABLE) -> DBCursor[Collection, StorageDocumentTotal]: ...
    @overload
    def find(self, filterQuery:FilterQuery[Collection], projection:Document[Projection, Collection],
            cursor_type:CT = CursorType.NON_TAILABLE) -> DBCursor[Collection, StorageDocument]: ...
    def find(self, filterQuery:FilterQuery[Collection],
            projection:Optional[Document[Projection, Collection]] = None,
            cursor_type:CT = CursorType.NON_TAILABLE) -> Union[DBCursor[Collection, StorageDocument], DBCursor[Collection, StorageDocumentTotal]]:
        return None #type:ignore

    @overload
    async def find_one(self, filterQuery:FilterQuery[Collection],
            projection:None = None) -> Optional[StorageDocumentTotal]: ...
    @overload
    async def find_one(self, filterQuery:FilterQuery[Collection],
            projection:Document[Projection, Collection]) -> Optional[StorageDocument]: ...
    async def find_one(self, filterQuery:FilterQuery[Collection],
            projection:Optional[Document[Projection, Collection]] = None) -> UOSD_T:
        return None

    async def insert_one(self, document:StorageDocumentTotal) -> InsertOneResult:
        return None #type:ignore

    async def insert_many(self, documents:Sequence[StorageDocumentTotal], ordered:bool = True) -> InsertManyResult:
        return None #type:ignore

    async def delete_one(self, filterQuery:FilterQuery[Collection]) -> Union[DeleteResultPositive, DeleteResultNegative]:
        return None #type:ignore

    async def delete_many(self, filterQuery:FilterQuery[Collection]) -> Union[DeleteResultPositive, DeleteResultNegative]:
        return None #type:ignore

    async def update_one(self, filterQuery:FilterQuery[Collection],
            updateDoc:UpdateDocument[Collection]) -> Union[UpdateResultPositive, UpdateResultNegative]:
        return None #type:ignore

    async def update_many(self, filterQuery:FilterQuery[Collection],
            updateDoc:UpdateDocument[Collection]) -> Union[UpdateResultPositive, UpdateResultNegative]:
        return None #type:ignore

    @overload
    async def find_one_and_update(self, filterQuery:FilterQuery[Collection],
            updateDoc:UpdateDocument[Collection], projection:None = None) -> Optional[StorageDocumentTotal]: ...
    @overload
    async def find_one_and_update(self, filterQuery:FilterQuery[Collection],
            updateDoc:UpdateDocument[Collection], projection:Document[Projection, Collection]) -> Optional[StorageDocument]: ...
    async def find_one_and_update(self, filterQuery:FilterQuery[Collection],
            updateDoc:UpdateDocument[Collection], projection:Optional[Document[Projection, Collection]] = None) -> UOSD_T:
        return None

    @overload
    async def find_one_and_replace(self, filterQuery:FilterQuery[Collection],
            replacementDoc:StorageDocument, projection:None = None,
            upsert:bool = False)  -> Optional[StorageDocumentTotal]: ...
    @overload
    async def find_one_and_replace(self, filterQuery:FilterQuery[Collection],
            replacementDoc:StorageDocument, projection:Document[Projection, Collection],
            upsert:bool = False)  -> Optional[StorageDocument]: ...
    async def find_one_and_replace(self, filterQuery:FilterQuery[Collection],
            replacementDoc:StorageDocument, projection:Optional[Document[Projection, Collection]] = None,
            upsert:bool = False)  -> UOSD_T:
        return None

    @overload
    async def find_one_and_delete(self, filterQuery:FilterQuery[Collection],
            projection:None = None) -> Optional[StorageDocumentTotal]: ...
    @overload
    async def find_one_and_delete(self, filterQuery:FilterQuery[Collection],
            projection:Document[Projection, Collection]) -> Optional[StorageDocument]: ...
    async def find_one_and_delete(self, filterQuery:FilterQuery[Collection],
            projection:Optional[Document[Projection, Collection]] = None) -> UOSD_T:
        return None

    async def count_documents(self, filterQuery:FilterQuery[Collection]) -> int:
        return None #type:ignore

    @overload
    async def distinct(self:DBCollection[HostsCollection, StorageDocument, StorageDocumentTotal], key:Literal["username"]) -> StrList: ... 
    @overload
    async def distinct(self:DBCollection[AuthRequestsCollection, StorageDocument, StorageDocumentTotal], key:Literal["state"]) -> StrList: ... 
    @overload
    async def distinct(self:DBCollection[GamesCollection, StorageDocument, StorageDocumentTotal], key:Literal["gamecode"]) -> StrList: ... 
    async def distinct(self, key:str) -> List[Any]:
        return None #type:ignore


OperatorExists = Mapping[Literal["$exists"], bool]
OperatorNotEqual = Mapping[Literal["$ne"], V]
OperatorRegex = Mapping[Literal["$regex"], Pattern[str]]
#OperatorNotEqualRegex = TypedDict("OperatorNotEqualRegex", {"$ne":str, "$regex":Pattern[str]})
OperatorIn = Mapping[Literal["$in"], Sequence[V]]
OperatorNotIn = Mapping[Literal["$nin"], Sequence[V]]
OperatorGreaterOrEqual = Mapping[Literal["$gte"], V]
OperatorLessThan = Mapping[Literal["$lt"], V]
OperatorLT_GTE = Mapping[Union[Literal["$lt"], Literal["$gte"]], V]
OperatorElemMatch = Mapping[Literal["$elemMatch"], V]
QueryOperators = Union[OperatorExists, OperatorRegex, OperatorNotEqual[V],
        OperatorIn[V], OperatorNotIn[V], OperatorGreaterOrEqual[V], OperatorLessThan[V],
        OperatorLT_GTE[V]]
Query = Union[V, QueryOperators[V]]
OQuery = Union[Query[V], Nothing]
PBit = Union[Literal[0], Literal[1]]
OPBit = Union[PBit, Nothing]
All = Union[OQuery[V], OPBit, O[V], Nothing]
AllElem = Union[OQuery[V], O[V], Nothing]
VArrayCondition = TypeVar("VArrayCondition", bound=StrMapping)
OQueryArray = Union[OQuery[V], OperatorElemMatch[VArrayCondition]]
AllArray = Union[OQuery[List[V]], O[List[V]], O[V], OperatorEach[V], OPBit, VArrayCondition, OperatorElemMatch[VArrayCondition]]

conditionExists:OperatorExists = {"$exists":True} 
conditionNotExists:OperatorExists = {"$exists":False} 

def elem_match(v:VArrayCondition) -> OperatorElemMatch[VArrayCondition]:
    return {"$elemMatch":v}

def conditionIn(*values:V) -> OperatorIn[V]:
    return {"$in":values}

def conditionNotIn(*values:V) -> OperatorNotIn[V]:
    return {"$nin":values}

def conditionNotEqual(value:V) -> OperatorNotEqual[V]:
    return {"$ne":value}

def conditionGreaterOrEqual(value:V) -> OperatorGreaterOrEqual[V]:
    return {"$gte":value}

def conditionLessThan(value:V) -> OperatorLessThan[V]:
    return {"$lt":value}

def conditionLT_GTE(value1:V, value2:V) -> OperatorLT_GTE[V]:
    return {"$lt":value1, "$gte":value2}

def conditionOr(*filterDocuments:Document[Filter, Collection]) -> OperatorOr[Collection]:
    return {"$or":filterDocuments}

def conditionAnd(*filterDocuments:Document[Filter, Collection]) -> OperatorAnd[Collection]:
    return {"$and":filterDocuments}

def conditionRegex(pattern:Pattern[str]) -> OperatorRegex:
    return {"$regex":pattern}

def each(*pushed_docs:V) -> OperatorEach[V]:
    return {"$each":pushed_docs}

"""def mixin_filters(filter_doc1:DocFilter[Collection],
        filter_doc2:DocFilter[Collection]) -> DocumentMixinFilter[Collection]:
    mixin = {**filter_doc1, **filter_doc2}
    return cast(DocumentMixinFilter[Collection], z)"""

class TokensDocument(TypedDict):
    access_token: str
    refresh_token: str
    expires_by: datetime

OptTokensDocument = Optional[TokensDocument]

