# coding:utf-8
from __future__ import annotations
from datetime import datetime
from typing import Any, List, Literal, Optional, overload, TypedDict, Union
from ..constants.configuration import config
from ..constants.types import (OptStr)
from .types import (DBCollection, MessagesCollection, OQuery, Document, DocumentKind,
        UpdateSet, UpdateDocumentSet, Projection, Filter,  PBit, OPBit, O, N, Nothing, All)
__all__ = ["MessageDocument", "MessageFilter", "MessageUpdateSet", "MessageProjection",
        "MessageSDocument", "MessageSDocumentTotal", "Msgs"]


class MessageSDocument(TypedDict, total=False):
    gamecode:str
    name:str
    root_id:OptStr
    author:str
    dest:str
    user1:str
    user2:str
    subject:str
    body:str
    createdOn:datetime

class MessageSDocumentTotal(TypedDict):
    gamecode:str
    name:str
    root_id:OptStr
    author:str
    dest:str
    user1:str
    user2:str
    subject:str
    body:str
    createdOn:datetime
Msgs = List[MessageSDocumentTotal]


class MessageDocument(Document[DocumentKind, MessagesCollection]):

    @overload
    def __init__(self:MessageDocument[UpdateSet], kind:DocumentKind, _id:O[str] = N, gamecode:O[str] = N,
            name:O[str] = N, root_id:O[OptStr] = N,
            author:O[str] = N, dest:O[str] = N, user1:O[str] = N, user2:O[str] = N,
            subject:O[str] = N, body:O[str] = N, createdOn:O[datetime] = N) -> None: ...

    @overload
    def __init__(self:MessageDocument[Filter], kind:DocumentKind, _id:OQuery[str] = N, gamecode:OQuery[str] = N, name:OQuery[str] = N,
            root_id:OQuery[OptStr] = N,
            author:OQuery[str] = N, dest:OQuery[str] = N, user1:OQuery[str] = N, user2:OQuery[str] = N,
            subject:OQuery[str] = N, body:OQuery[str] = N, createdOn:OQuery[datetime] = N) -> None: ...

    @overload
    def __init__(self:MessageDocument[Projection], kind:DocumentKind, _id:OPBit = N, gamecode:OPBit = N, name:OPBit = N, root_id:OPBit = N,
            author:OPBit = N, dest:OPBit = N, user1:OPBit = N, user2:OPBit = N,
            subject:OPBit = N, body:OPBit = N, createdOn:OPBit = N) -> None: ...

    def __init__(self, kind:DocumentKind, _id:All[str] = N, gamecode:All[str] = N, name:All[str] = N, root_id:All[OptStr] = N,
            author:All[str] = N, dest:All[str] = N,
            user1:All[str] = N, user2:All[str] = N, subject:All[str] = N, body:All[str] = N, createdOn:All[datetime] = N) -> None:
        if kind == "projection" and _id is N:
            _id = 0
        if _id is not N:
            self["_id"] = _id
        if name is not N:
            self["name"] = name
        if gamecode is not N:
            self["gamecode"] = gamecode
        if root_id is not N:
            self["root_id"] = root_id
        if author is not N:
            self["author"] = author
        if dest is not N:
            self["dest"] = dest
        if author is not N:
            self["author"] = author
        if user1 is not N:
            self["user1"] = user1
        if user2 is not N:
            self["user2"] = user2
        if subject is not N:
            self["subject"] = subject
        if body is not N:
            self["body"] = body
        if createdOn is not N:
            self["createdOn"] = createdOn


def MessageUpdateSet(_id:O[str] = N, gamecode:O[str] = N, name:O[str] = N, root_id:O[OptStr] = N, author:O[str] = N, dest:O[str] = N,
        user1:O[str] = N, user2:O[str] = N,
        subject:O[str] = N, body:O[str] = N, createdOn:O[datetime] = N) -> UpdateDocumentSet[MessagesCollection]:
    doc = MessageDocument[UpdateSet]("update_set", _id=_id,  gamecode=gamecode, name=name, root_id=root_id, author=author, dest=dest,
            user1=user1, user2=user2, subject=subject, body=body, createdOn=createdOn)
    return {"$set":doc}

def MessageFilter(_id:OQuery[str] = N, gamecode:OQuery[str] = N, name:OQuery[str] = N,
        root_id:OQuery[OptStr] = N, author:OQuery[str] = N, dest:OQuery[str] = N,
        user1:OQuery[str] = N, user2:OQuery[str] = N, subject:OQuery[str] = N, body:OQuery[str] = N,
        createdOn:OQuery[datetime] = N) -> MessageDocument[Filter]:
    doc = MessageDocument[Filter]("filter", _id=_id, gamecode=gamecode, name=name,
            root_id=root_id, author=author, dest=dest, user1=user1, user2=user2,
            subject=subject, body=body, createdOn=createdOn)
    return doc

def MessageProjection(_id:OPBit = N, gamecode:OPBit = N, name:OPBit = N,
        root_id:OPBit = N, author:OPBit = N, dest:OPBit = N, user1:OPBit = N, user2:OPBit = N,
        subject:OPBit = N, body:OPBit = N, createdOn:OPBit = N) -> MessageDocument[Projection]:
    doc = MessageDocument[Projection]("projection", _id=_id, gamecode=gamecode, name=name, root_id=root_id, author=author, dest=dest,
            user1=user1, user2=user2, subject=subject, body=body, createdOn=createdOn)
    return doc
