# coding:utf-8
from __future__ import annotations
from datetime import datetime
from typing import Any, List, Literal, Optional, overload, TypedDict, Union
from ..constants.configuration import config
from ..constants.types import *
from .types import (DBCollection, AuthRequestsCollection, OQuery, Document, DocumentKind,
        UpdateSet, UpdateDocumentSet, Projection, Filter,  PBit, OPBit, O, N, Nothing, All)
__all__ = ["AuthRequestDocument", "AuthRequestFilter", "AuthRequestUpdateSet", "AuthRequestProjection",
        "AuthRequestSDocument", "AuthRequestSDocumentTotal"]


class AuthRequestSDocument(TypedDict, total=False):
    username:str
    state:str
    expires_by:datetime
    role:Role

class AuthRequestSDocumentTotal(TypedDict):
    username:str
    state:str
    expires_by:datetime
    role:Role


class AuthRequestDocument(Document[DocumentKind, AuthRequestsCollection]):

    @overload
    def __init__(self:AuthRequestDocument[UpdateSet], kind:DocumentKind,
            _id:O[str] = N, username:O[str] = N, state:O[str] = N,
            expires_by:O[datetime] = N, role:O[Role] = N) -> None: ...

    @overload
    def __init__(self:AuthRequestDocument[Filter], kind:DocumentKind,
            _id:OQuery[str] = N, username:OQuery[str] = N, state:OQuery[str] = N,
            expires_by:OQuery[datetime] = N, role:OQuery[Role] = N) -> None: ...

    @overload
    def __init__(self:AuthRequestDocument[Projection], kind:DocumentKind,
            _id:OPBit = N, username:OPBit = N, state:OPBit = N,
            expires_by:OPBit = N, role:OPBit = N) -> None: ...

    def __init__(self, kind:DocumentKind, _id:All[str] = N,
            username:All[str] = N, state:All[str] = N,
            expires_by:All[datetime] = N, role:All[Role] = N) -> None:
        if kind == "projection" and _id is N:
            _id = 0
        if _id is not N:
            self["_id"] = _id
        if username is not N:
            self["username"] = username
        if state is not N:
            self["state"] = state
        if expires_by is not N:
            self["expires_by"] = expires_by
        if role is not N:
            self["role"] = role


def AuthRequestUpdateSet(_id:O[str] = N, username:O[str] = N, state:O[str] = N,
        expires_by:O[datetime] = N, role:O[Role] = N) -> UpdateDocumentSet[AuthRequestsCollection]:
    doc = AuthRequestDocument[UpdateSet]("update_set", _id=_id, username=username, state=state,
            expires_by=expires_by, role=role)
    return {"$set":doc}

def AuthRequestFilter(_id:OQuery[str] = N, username:OQuery[str] = N, state:OQuery[str] = N,
        expires_by:OQuery[datetime] = N, role:OQuery[Role] = N) -> AuthRequestDocument[Filter]:
    doc = AuthRequestDocument[Filter]("filter", _id=_id, username=username, state=state,
            expires_by=expires_by, role=role)
    return doc

def AuthRequestProjection(_id:OPBit = N, username:OPBit = N, state:OPBit = N,
        expires_by:OPBit = N, role:OPBit = N) -> AuthRequestDocument[Projection]:
    doc = AuthRequestDocument[Projection]("projection", _id=_id, username=username, state=state,
            expires_by=expires_by, role=role)
    return doc


