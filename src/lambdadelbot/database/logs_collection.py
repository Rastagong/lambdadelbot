# coding:utf-8
from __future__ import annotations
from datetime import datetime
from typing import overload, TypedDict
from ..constants.configuration import config
from ..constants.types import (OptStr, OptBool, OptInt)
from .types import (DBCollection, LogsCollection, OQuery, Document, DocumentKind,
        UpdateSet, UpdateDocumentSet, Projection, Filter,  PBit, OPBit, O, N, Nothing, All)
__all__ = ["LogDocument", "LogFilter", "LogUpdateSet", "LogProjection",
        "LogSDocument", "LogSDocumentTotal"]

class LogSDocument(TypedDict, total=False):
    asctime:datetime
    exc_text:OptStr
    filename:str
    funcName:str
    levelname:str
    levelno:int
    lineno:int
    message:str
    module:str
    pathname:OptStr
    process:OptInt
    processName:OptStr
    gamecode:OptStr
    globalCycleName:OptStr
    gameCycleName:OptStr
    lock:OptBool
    network:OptBool

class LogSDocumentTotal(TypedDict):
    asctime:datetime
    exc_text:OptStr
    filename:str
    funcName:str
    levelname:str
    levelno:int
    lineno:int
    message:str
    module:str
    pathname:OptStr
    process:OptInt
    processName:OptStr
    gamecode:OptStr
    globalCycleName:OptStr
    gameCycleName:OptStr
    lock:OptBool
    network:OptBool


class LogDocument(Document[DocumentKind, LogsCollection]):

    @overload
    def __init__(self:LogDocument[UpdateSet], kind:DocumentKind, _id:O[str] = N,
            asctime:O[datetime] = N, exc_text:O[OptStr] = N, filename:O[str] = N, funcName:O[str] = N, levelname:O[str] = N,
            levelno:O[int] = N, lineno:O[int] = N, message:O[str] = N, module:O[str] = N, pathname:O[OptStr] = N, process:O[OptInt] = N,
            processName:O[OptStr] = N, gamecode:O[OptStr] = N, globalCycleName:O[OptStr] = N, gameCycleName:O[OptStr] = N,
            lock:O[OptBool] = N, network:O[OptBool] = N) -> None: ...

    @overload
    def __init__(self:LogDocument[Filter], kind:DocumentKind, _id:OQuery[str] = N,
            asctime:OQuery[datetime] = N, exc_text:OQuery[OptStr] = N, filename:OQuery[str] = N, funcName:OQuery[str] = N,
            levelname:OQuery[str] = N, levelno:OQuery[int] = N, lineno:OQuery[int] = N, message:OQuery[str] = N,
            module:OQuery[str] = N, pathname:OQuery[OptStr] = N,
            process:OQuery[OptInt] = N, processName:OQuery[OptStr] = N, gamecode:OQuery[OptStr] = N, globalCycleName:OQuery[OptStr] = N,
            gameCycleName:OQuery[OptStr] = N, lock:OQuery[OptBool] = N, network:OQuery[OptBool] = N) -> None: ...

    @overload
    def __init__(self:LogDocument[Projection], kind:DocumentKind, _id:OPBit = N,
            asctime:OPBit = N, exc_text:OPBit = N, filename:OPBit = N, funcName:OPBit = N,
            levelname:OPBit = N, levelno:OPBit = N, lineno:OPBit = N, message:OPBit = N, module:OPBit = N, pathname:OPBit = N,
            process:OPBit = N, processName:OPBit = N, gamecode:OPBit = N, globalCycleName:OPBit = N,
            gameCycleName:OPBit = N, lock:OPBit = N, network:OPBit = N) -> None: ...

    def __init__(self, kind:DocumentKind, _id:All[str] = N, asctime:All[datetime] = N, exc_text:All[OptStr] = N, filename:All[str] = N,
            funcName:All[str] = N, levelname:All[str] = N, levelno:All[int] = N, lineno:All[int] = N, message:All[str] = N,
            module:All[str] = N,
            pathname:All[OptStr] = N, process:All[OptInt] = N, processName:All[OptStr] = N, gamecode:All[OptStr] = N,
            globalCycleName:All[OptStr] = N, gameCycleName:All[OptStr] = N, lock:All[OptBool] = N, network:All[OptBool] = N) -> None:
        if kind == "projection" and _id is N:
            _id = 0
        if _id is not N:
            self["_id"] = _id
        if asctime is not N:
            self["asctime"] = asctime
        if exc_text is not N:
            self["exc_text"] = exc_text
        if filename is not N:
            self["filename"] = filename
        if funcName is not N:
            self["funcName"] = funcName
        if levelname is not N:
            self["levelname"] = levelname
        if levelno is not N:
            self["levelno"] = levelno
        if lineno is not N:
            self["lineno"] = lineno
        if message is not N:
            self["message"] = message
        if module is not N:
            self["module"] = module
        if pathname is not N:
            self["pathname"] = pathname
        if process is not N:
            self["process"] = process
        if processName is not N:
            self["processName"] = processName
        if gamecode is not N:
            self["gamecode"] = gamecode
        if globalCycleName is not N:
            self["globalCycleName"] = globalCycleName
        if gameCycleName is not N:
            self["gameCycleName"] = gameCycleName
        if lock is not N:
            self["lock"] = lock
        if network is not N:
            self["network"] = network


def LogUpdateSet(_id:O[str] = N, asctime:O[datetime] = N, exc_text:O[OptStr] = N, filename:O[str] = N, funcName:O[str] = N,
        levelname:O[str] = N, levelno:O[int] = N, lineno:O[int] = N, message:O[str] = N,
        module:O[str] = N, pathname:O[OptStr] = N, process:O[OptInt] = N,
        processName:O[OptStr] = N, gamecode:O[OptStr] = N, globalCycleName:O[OptStr] = N, gameCycleName:O[OptStr] = N,
        lock:O[OptBool] = N, network:O[OptBool] = N) -> UpdateDocumentSet[LogsCollection]:
    doc = LogDocument[UpdateSet]("update_set", _id=_id, asctime=asctime, exc_text=exc_text, filename=filename, funcName=funcName,
            levelname=levelname, levelno=levelno, lineno=lineno, message=message, module=module, pathname=pathname, process=process,
            processName=processName, gamecode=gamecode, globalCycleName=globalCycleName, gameCycleName=gameCycleName,
            lock=lock, network=network)
    return {"$set":doc}

def LogFilter(_id:OQuery[str] = N, asctime:OQuery[datetime] = N, exc_text:OQuery[OptStr] = N, filename:OQuery[str] = N,
        funcName:OQuery[str] = N, levelname:OQuery[str] = N, levelno:OQuery[int] = N, lineno:OQuery[int] = N, message:OQuery[str] = N,
        module:OQuery[str] = N,
        pathname:OQuery[OptStr] = N, process:OQuery[OptInt] = N, processName:OQuery[OptStr] = N, gamecode:OQuery[OptStr] = N,
        globalCycleName:OQuery[OptStr] = N, gameCycleName:OQuery[OptStr] = N, lock:OQuery[OptBool] = N,
        network:OQuery[OptBool] = N) -> LogDocument[Filter]:
    doc = LogDocument[Filter]("filter", _id=_id, asctime=asctime, exc_text=exc_text, filename=filename, funcName=funcName,
            levelname=levelname, levelno=levelno, lineno=lineno, message=message, module=module, pathname=pathname, process=process,
            processName=processName, gamecode=gamecode, globalCycleName=globalCycleName, gameCycleName=gameCycleName,
            lock=lock, network=network)
    return doc

def LogProjection(_id:OPBit = N, asctime:OPBit = N, exc_text:OPBit = N, filename:OPBit = N, funcName:OPBit = N,
        levelname:OPBit = N, levelno:OPBit = N, lineno:OPBit = N, message:OPBit = N, module:OPBit = N, pathname:OPBit = N,
        process:OPBit = N, processName:OPBit = N, gamecode:OPBit = N, globalCycleName:OPBit = N,
        gameCycleName:OPBit = N, lock:OPBit = N, network:OPBit = N) -> LogDocument[Projection]:
    doc = LogDocument[Projection]("projection", _id=_id, asctime=asctime, exc_text=exc_text, filename=filename, funcName=funcName,
            levelname=levelname, levelno=levelno, lineno=lineno, message=message, module=module, pathname=pathname, process=process,
            processName=processName, gamecode=gamecode, globalCycleName=globalCycleName, gameCycleName=gameCycleName,
            lock=lock, network=network)
    return doc

