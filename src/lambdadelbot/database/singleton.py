# coding: utf-8
import re, logging, time, asyncio, pymongo
from typing import (Any, AsyncIterator, Awaitable, cast, Generic, List, Literal, Mapping,
        Optional, overload, Sequence, Tuple, Type, TypeVar, Union, TYPE_CHECKING)
from datetime import datetime, timedelta
from pymongo.cursor import CursorType
from pymongo.errors import BulkWriteError
if TYPE_CHECKING:
    from pymongo import SortDirection
    AsyncIOMotorClient:Any
else:
    SortDirection = ""
    from motor.motor_asyncio import AsyncIOMotorClient
from ..constants.configuration import config
from ..constants.logging_configuration import X
from ..constants.types import (Bot, BotRole, GameRoutineParams, GameState, GameStateValue,
        GameStatesSequence, Host, OptBool, OptInt, OptStr, OptGameState,
        Player, Role, StrList, StrTuple, TTrue, FFalse,)
from ..structs.week_time import WeekTime
from ..structs.path_keys import PathKeys
from ..structs.tokens import RedditTokens
from ..structs.wiki_url import WikiUrlSubreddit
from .exceptions import *
from .types import (Collection, DBCollection, DocFilter,
        each, conditionNotEqual, conditionRegex, conditionAnd, conditionIn, conditionNotIn, conditionOr,
        conditionExists, conditionNotExists, conditionGreaterOrEqual, conditionLessThan,
        conditionLT_GTE, elem_match,
        OperatorIn, TokensDocument, OptTokensDocument,
        GamesCollection, AuthRequestsCollection, HostsCollection, MessagesCollection,
        ConfigCollection, ManualCyclesCollection, LogsCollection)
from .games_collection import (GameSDocument, GameSDocumentTotal,
        GameFilter, GameProjection, GameUpdateSet, GameUpdatePush, GameUpdatePull, GameUpdateIncrement,
        ScheduleDocument, ScheduleDict, PathKeyDocument, PathKeyDefaultDocument, PathKeySingleDocument, 
        PathKeyDocumentCondition, PathHashDict, PlayerDict, PlayerDictCondition,
        players_as_mapping, PlayersMapping)
from .authrequests_collection import (AuthRequestSDocument, AuthRequestSDocumentTotal,
        AuthRequestFilter, AuthRequestProjection)
from .hosts_collection import (HostSDocument, HostSDocumentTotal, HostFilter, HostProjection, HostUpdateSet)
from .messages_collection import (MessageSDocument, MessageSDocumentTotal, MessageFilter, MessageProjection, MessageUpdateSet, Msgs)
from .config_collection import (GlobalRoutineDocument, GlobalRoutineTuple,
        ConfigSDocument, ConfigSDocumentTotal,
        ConfigFilter, ConfigUpdateSet, ConfigProjection)
from .manualcycles_collection import (ManualCycleSDocument, ManualCycleSDocumentTotal, ManualCycleFilter, ManualCycleProjection,
        ManualCycleUpdateSet)
from .logs_collection import (LogSDocument, LogSDocumentTotal, LogFilter, LogProjection, LogUpdateSet)
from .results import (_WriteResult, UpdateResultPositive, UpdateResultNegative,
        DeleteResultPositive, DeleteResultNegative, InsertOneResult, InsertManyResult)
__all__ = ["DBSingleton"]
logger = logging.getLogger(config.loggerName)


class DBVirtualClient:
    def __init__(self) -> None:
        self.games = DBCollection[GamesCollection, GameSDocument, GameSDocumentTotal]()
        self.auth_requests = DBCollection[AuthRequestsCollection, AuthRequestSDocument, AuthRequestSDocumentTotal]()
        self.hosts = DBCollection[HostsCollection, HostSDocument, HostSDocumentTotal]()
        self.messages = DBCollection[MessagesCollection, MessageSDocument, MessageSDocumentTotal]()
        self.config = DBCollection[ConfigCollection, ConfigSDocument, ConfigSDocumentTotal]()
        self.manualCycles = DBCollection[ManualCyclesCollection, ManualCycleSDocument, ManualCycleSDocumentTotal]()
        self.logs = DBCollection[LogsCollection, LogSDocument, LogSDocumentTotal]()


LastSeenTuple = Tuple[OptStr, OptStr]

def ensure_effective_insert_one(collection:Collection, result:InsertOneResult) -> InsertOneResult[TTrue]:
    if not result.acknowledged:
        raise UnacknowledgedError(collection, None, result)
    return result

def ensure_effective_insert_many(collection:Collection, result:InsertManyResult,
        expected_count:OptInt = None) -> InsertManyResult[TTrue]:
    if not result.acknowledged:
        raise UnacknowledgedError(collection, None, result)
    elif expected_count is None:
        return result
    actual_count = len(result.inserted_ids)
    if actual_count != expected_count:
        raise NotEnoughModificationsError(collection, expected_count, actual_count, result)
    return result


MatchedCountType = Literal["matched_count"]
MatchedCount:MatchedCountType = "matched_count"
ModifiedCountType = Literal["modified_count"]
ModifiedCount:ModifiedCountType = "modified_count"
CountField = TypeVar("CountField", MatchedCountType, ModifiedCountType, None)

def ensure_effective_update(collection:Collection, filter_doc:DocFilter[Collection],
        result:Union[UpdateResultPositive, UpdateResultNegative], count_field:CountField) -> UpdateResultPositive:
    if not result.acknowledged:
        raise UnacknowledgedError(collection, filter_doc, result)
    elif count_field is None:
        return result
    elif count_field == "modified_count" and result.modified_count <= 0:
        raise NoModificationError(collection, filter_doc, result)
    elif count_field == "matched_count" and result.matched_count <= 0:
        raise NoMatchError(collection, filter_doc, result)
    return result

def ensure_effective_deletion(collection:Collection, filter_doc:DocFilter[Collection],
        result:Union[DeleteResultPositive, DeleteResultNegative],
        ensure_deleted:bool = False) -> DeleteResultPositive:
    if not result.acknowledged:
        raise UnacknowledgedError(collection, filter_doc, result)
    elif ensure_deleted and result.deleted_count <= 0:
        raise NoDeletionError(collection, filter_doc, result)
    return result


MissingType = Literal[-666]
Missing:MissingType = -666
A = TypeVar("A")
D = TypeVar("D")

async def handle_db(awaitable:Awaitable[A], ErrorClass:Type[Exception], default_value:D) -> Union[A, D]:
    try:
        return_value = await awaitable
    except ErrorClass:
        return default_value
    else:
        return return_value


class DBSingleton:
    def __init__(self) -> None:
        super().__init__()
        logger.info(f"DB url: {config.database.connectionURI}")
        self._client:DBVirtualClient = AsyncIOMotorClient(config.database.connectionURI)[config.database.dbName]

    async def get_usernames(self) -> StrList:
        return await self._client.hosts.distinct("username")

    async def is_any_host(self, username:str) -> bool:
        filter_doc = HostFilter(username=username)
        return await self._client.hosts.count_documents(filter_doc) == 1

    async def get_game_documents(self, gamecode:OptStr = None) -> Sequence[GameSDocumentTotal]:
        filter_doc = GameFilter()
        if gamecode is not None:
            filter_doc = GameFilter(gamecode=gamecode)
        cur = self._client.games.find(filter_doc)
        return await cur.to_list(length=config.app.maxGames) 

    async def check_game_exists(self, gamecode:str) -> bool:
        count = await self._client.games.count_documents(GameFilter(gamecode=gamecode))
        return count == 1

    async def get_game_essential_info(self, gamecode:str) -> Tuple[str, str]:
        filter_doc = GameFilter(gamecode=gamecode)
        projection = GameProjection(host_username=1, title=1)
        game_doc = await self._client.games.find_one(filter_doc, projection)
        if game_doc is None:
            raise DocumentNotFoundError("games", filter_doc)
        return (game_doc["host_username"], game_doc["title"])

    async def get_game_title(self, gamecode:str) -> str:
        filter_doc = GameFilter(gamecode=gamecode)
        projection = GameProjection(title=1)
        game_doc = await self._client.games.find_one(filter_doc, projection)
        if game_doc is None:
            raise DocumentNotFoundError("games", filter_doc)
        return game_doc["title"]

    async def checkOtherGameTitleExists(self, gamecode:str, title:str) -> bool:
        #query:OperatorNotEqual[str] = {"$ne":gamecode}
        return await self._client.games.count_documents(GameFilter(gamecode=conditionNotEqual(gamecode), title=title)) > 0

    async def update_game_title(self, gamecode:str, game_title:str) -> UpdateResultPositive:
        filter_doc = GameFilter(gamecode=gamecode)
        result = await self._client.games.update_one(filter_doc, GameUpdateSet(title=game_title))
        return ensure_effective_update("games", filter_doc, result, MatchedCount)

    async def update_game_host_username(self, gamecode:str, host_username:str) -> bool:
        filter_doc = GameFilter(gamecode=gamecode)
        update_doc = GameUpdateSet(host_username=host_username)
        game_doc = await self._client.games.find_one_and_update(filter_doc, update_doc)
        return game_doc is not None

    async def update_games_host_username_by_old_host_username(self, host_username_old:str, host_username_new:str) -> UpdateResultPositive:
        filter_doc = GameFilter(host_username=host_username_old)
        result = await self._client.games.update_many(filter_doc, GameUpdateSet(host_username=host_username_new))
        return ensure_effective_update("games", filter_doc, result, None)

    async def update_display_code(self, gamecode:str, gamecode_display:str) -> UpdateResultPositive:
        filter_doc = GameFilter(gamecode=gamecode)
        update_doc = GameUpdateSet(gamecode_display=gamecode_display)
        result = await self._client.games.update_one(filter_doc, update_doc)
        return ensure_effective_update("games", filter_doc, result, MatchedCount)
    
    async def get_schedule(self, gamecode:str) -> ScheduleDict:
        game_doc = await self._client.games.find_one(GameFilter(gamecode=gamecode), GameProjection(schedule=1))
        if game_doc is None:
            raise DocumentNotFoundError("games", GameFilter(gamecode=gamecode))
        return {"start":WeekTime(standardisedDatetime=game_doc["schedule"]["start"]),
                "end":WeekTime(standardisedDatetime=game_doc["schedule"]["end"])}

    async def update_schedule(self, gamecode:str, start:WeekTime, end:WeekTime) -> UpdateResultPositive:
        start_datetime:datetime = start
        end_datetime:datetime = end
        schedule_doc:ScheduleDocument = {"start":start_datetime, "end":end_datetime}
        filter_doc = GameFilter(gamecode=gamecode)
        result = await self._client.games.update_one(filter_doc, GameUpdateSet(schedule=schedule_doc))
        return ensure_effective_update("games", filter_doc, result, MatchedCount)

    async def insert_game_document(self, gamecode:str, gamecode_display:str, title:str, host_username:str,
            subreddit_name:str, default_week_time:WeekTime,
            default_key:PathKeyDefaultDocument) -> InsertOneResult[TTrue]:
        schedule:ScheduleDocument = {"start":default_week_time, "end":default_week_time}
        path_keys:List[PathKeyDocument] = [default_key]
        doc:GameSDocumentTotal = {
                "gamecode":gamecode, "gamecode_display":gamecode_display,
                "title":title, "host_username":host_username,
                "subreddit_name":subreddit_name, "schedule":schedule,
                "game_state":GameState.paused.value, "path_hashes":[], "players":[],
                "path_keys":path_keys, "update_count":0, "excluded_messages":[],
                "lock":  {"free":True, "process":None, "acquiredTime":None, "opName":None} }
        result = await self._client.games.insert_one(doc)
        return ensure_effective_insert_one("games", result)

    async def get_subreddit_name(self, gamecode:str) -> str:
        game_doc = await self._client.games.find_one(GameFilter(gamecode=gamecode), GameProjection(subreddit_name=1))
        if game_doc is None:
            raise DocumentNotFoundError("games", GameFilter(gamecode=gamecode))
        return game_doc["subreddit_name"]

    async def update_subreddit_name(self, gamecode:str, subreddit_name:str) -> UpdateResultPositive:
        filter_doc = GameFilter(gamecode=gamecode)
        result = await self._client.games.update_one(filter_doc, GameUpdateSet(subreddit_name=subreddit_name))
        return ensure_effective_update("games", filter_doc, result, MatchedCount)

    async def set_game_state(self, gamecode:str, game_state:GameState) -> UpdateResultPositive:
        filter_doc = GameFilter(gamecode=gamecode)
        result = await self._client.games.update_one(filter_doc, GameUpdateSet(game_state=game_state.value))
        return ensure_effective_update("games", filter_doc, result, MatchedCount)

    async def get_game_state(self, gamecode:str) -> GameState:
        filter_doc = GameFilter(gamecode=gamecode)
        projection = GameProjection(game_state=1)
        game_doc = await self._client.games.find_one(filter_doc, projection)
        if game_doc is None:
            raise DocumentNotFoundError("games", GameFilter(gamecode=gamecode))
        return GameState(game_doc["game_state"])

    async def get_host_username(self, gamecode:str) -> str:
        filter_doc = GameFilter(gamecode=gamecode)
        projection = GameProjection(host_username=1)
        game_doc = await self._client.games.find_one(filter_doc, projection)
        if game_doc is None:
            raise DocumentNotFoundError("games", filter_doc)
        return game_doc["host_username"]

    async def get_current_auth_states(self) -> StrList:
        return await self._client.auth_requests.distinct("state")

    async def clearAuthRequestForHostname(self, username:str) -> None:
        await self._client.auth_requests.delete_many(AuthRequestFilter(username=username))

    async def add_auth_request(self, state:str, username:str, role:Role) -> InsertOneResult[TTrue]:
        expires_by = datetime.utcnow() + timedelta(seconds=config.webapp.auth_requests_expiration_delay)
        result = await self._client.auth_requests.insert_one({"state":state, "username":username,
            "expires_by":expires_by, "role":role})
        return ensure_effective_insert_one("auth_requests", result)

    async def check_user_exists(self, username:str) -> bool:
        return await self._client.hosts.count_documents(HostFilter(username=username)) == 1 

    async def get_user_role_in_game(self, username:str) -> Role:
        condition1 = GameFilter(host_username=username)
        condition2 = GameFilter(players_username=username)
        condition = conditionOr(condition1, condition2)
        projection = GameProjection(host_username=1)
        doc = await self._client.games.find_one(condition, projection)
        if doc is None:
            raise DocumentNotFoundError("games", condition)
        return Host if doc["host_username"] == username else Player

    async def auth_request_authenticate_success(self, state:str) -> Tuple[str, Role]:
        filter_doc = AuthRequestFilter(state=state, expires_by=conditionGreaterOrEqual(datetime.utcnow()))
        projection = AuthRequestProjection(username=1, role=1)
        doc = await self._client.auth_requests.find_one_and_delete(filter_doc, projection)
        if doc is None:
            raise DocumentNotFoundError("auth_requests", filter_doc)
        return (doc["username"], doc["role"])

    async def clear_expired_auth_requests(self) -> DeleteResultPositive:
        filter_doc = AuthRequestFilter(expires_by=conditionLessThan(datetime.utcnow()))
        result = await self._client.auth_requests.delete_many(filter_doc)
        return ensure_effective_deletion("auth_requests", filter_doc, result, False)

    async def clear_all_auth_requests(self) -> DeleteResultPositive:
        filter_doc = AuthRequestFilter()
        result = await self._client.auth_requests.delete_many(filter_doc)
        return ensure_effective_deletion("auth_requests", filter_doc, result, False)

    async def get_reddit_tokens_for_player(self, gamecode:OptStr, username:str) -> TokensDocument:
        if gamecode is not None:
            filter_doc = GameFilter(gamecode=gamecode, players_username=username)
        else:
            filter_doc = GameFilter(players_username=username)
        projection = GameProjection(players_elem=1)
        game_doc = await self._client.games.find_one(filter_doc, projection)
        if game_doc is None:
            raise DocumentNotFoundError("games", filter_doc)
        tokens_doc = game_doc["players"][0]["reddit_tokens"]
        if tokens_doc is None:
            raise DocumentNotFoundError("games", filter_doc)
        return tokens_doc

    async def store_reddit_tokens_for_player(self, gamecode:OptStr, username:str, access_token:str,
            refresh_token:str, expires_by:datetime) -> UpdateResultPositive:
        tokens:TokensDocument = {"access_token":access_token, "refresh_token":refresh_token, "expires_by":expires_by}
        if gamecode is not None:
            filter_doc = GameFilter(gamecode=gamecode, players_username=username)
        else:
            filter_doc = GameFilter(players_username=username)
        result = await self._client.games.update_one(filter_doc, GameUpdateSet(players_reddit_tokens=tokens))
        return ensure_effective_update("games", filter_doc, result, MatchedCount)

    async def erase_reddit_tokens_for_player(self, gamecode:OptStr, username:str) -> UpdateResultPositive:
        if gamecode is not None:
            filter_doc = GameFilter(gamecode=gamecode, players_username=username)
        else:
            filter_doc = GameFilter(players_username=username)
        update_doc = GameUpdateSet(players_reddit_tokens=None)
        result = await self._client.games.update_one(filter_doc, update_doc)
        return ensure_effective_update("games", filter_doc, result, MatchedCount)

    async def get_players(self, gamecode:str) -> PlayersMapping:
        filter_doc = GameFilter(gamecode=gamecode)
        projection = GameProjection(players=1)
        game_doc = await self._client.games.find_one(filter_doc, projection)
        if game_doc is None:
            raise DocumentNotFoundError("games", filter_doc)
        return players_as_mapping(game_doc["players"])

    def _get_lock_acquisition_filter_doc(self, gamecode:str, game_states:Optional[GameStatesSequence]) -> DocFilter[GamesCollection]:
        if game_states is None:
            return GameFilter(gamecode=gamecode, lockFree=True)
        game_state_values = tuple(game_state.value for game_state in game_states)
        game_states_condition:OperatorIn[GameStateValue] = conditionIn(*game_state_values)
        return GameFilter(gamecode=gamecode, lockFree=True, game_state=game_states_condition)

    async def acquire_game_lock(self, gamecode:str, process:str, opName:str,
            game_states:Optional[GameStatesSequence]) -> bool:
        filter_doc = self._get_lock_acquisition_filter_doc(gamecode, game_states)
        #logger.debug(f"Filter doc for game lock acquisition: {filter_doc}")
        update_doc = GameUpdateSet(lockFree=False, lockProcess=process,
                lockAcquiredTime=datetime.now(), lockOpName=opName)
        #logger.debug(f"Update doc for game lock acquisition: {update_doc}")
        result:Optional[GameSDocumentTotal] = None
        result = await self._client.games.find_one_and_update(filter_doc, update_doc)
        #logger.debug(f"Result for game lock acquisition: {result}")
        return result is not None

    async def release_game_lock(self, gamecode:str, process:str) -> bool:
        filter_doc = GameFilter(gamecode=gamecode, lockProcess=process, lockFree=False)
        update_doc = GameUpdateSet(lockFree=True, lockProcess=None, lockAcquiredTime=None, lockOpName=None)
        result = await self._client.games.find_one_and_update(filter_doc, update_doc)
        return result is not None

    async def release_all_game_locks(self, process:str) -> UpdateResultPositive:
        filter_doc = GameFilter(lockProcess=process)
        update_doc = GameUpdateSet(lockFree=True, lockProcess=None, lockAcquiredTime=None, lockOpName=None)
        result = await self._client.games.update_many(filter_doc, update_doc)
        return ensure_effective_update("games", filter_doc, result, None)

    async def acquire_all_game_locks(self, process:str, opName:str) -> UpdateResultPositive:
        filter_doc = GameFilter(lockFree=True, game_state=conditionLT_GTE(GameState.finished.value, GameState.running.value))
        update_doc = GameUpdateSet(lockFree=False, lockProcess=process, lockAcquiredTime=datetime.utcnow(), lockOpName=opName)
        result = await self._client.games.update_many(filter_doc, update_doc)
        return ensure_effective_update("games", filter_doc, result, None)
            
    async def get_acquired_games(self, process:str, opName:str) -> StrTuple:
        cur = self._client.games.find(GameFilter(lockOpName=opName, lockProcess=process, lockFree=False), GameProjection(gamecode=1))
        return tuple(doc["gamecode"] for doc in await cur.to_list(config.app.maxGames))

    async def insert_config_document(self, rtokens:RedditTokens[BotRole]) -> InsertOneResult[TTrue]:
        tokens_doc:TokensDocument = {"access_token":rtokens.inner_tokens.access_token,
                "refresh_token":rtokens.inner_tokens._refresh_token,
                "expires_by":rtokens.inner_tokens._expires_by}
        start_time = datetime(year=1970, month=1, day=1, hour=9, minute=1)
        end_time   = start_time + timedelta(minutes=1)
        global_routine_doc = GlobalRoutineDocument(start_time=start_time, end_time=end_time)
        doc = ConfigSDocumentTotal(always_null=None,
                                  bot_reddit_tokens=tokens_doc,
                                  global_routine=global_routine_doc)
        result = await self._client.config.insert_one(doc)
        return ensure_effective_insert_one("config", result)

    async def is_config_ready(self) -> bool:
        filter_doc = ConfigFilter(always_null=None)
        count = await self._client.config.count_documents(filter_doc)
        if count < 0:
            raise ValueError(f"Invalid documents count in the config collection")
        elif count > 1:
            raise ValueError(f"Invalid documents count in the config collection, there should be only one, not {count}")
        return count == 1

    async def get_last_global_routine_exec_times(self) -> GlobalRoutineTuple:
        filter_doc = ConfigFilter(always_null=None)
        projection = ConfigProjection(global_routine=1)
        doc = await self._client.config.find_one(filter_doc, projection)
        if doc is None:
            raise DocumentNotFoundError("config", filter_doc)
        return (doc["global_routine"]["start_time"], doc["global_routine"]["end_time"])

    async def store_global_routine_start_time(self, timestamp:datetime, x:X) -> UpdateResultPositive:
        logger.info(f"Global routine start time updated to {timestamp}", extra=x)
        filter_doc = ConfigFilter(always_null=None)
        update_doc = ConfigUpdateSet(global_routine_start_time=timestamp, global_routine_end_time=None)
        result = await self._client.config.update_one(filter_doc, update_doc)
        return ensure_effective_update("config", filter_doc, result, MatchedCount)

    async def store_global_routine_end_time(self, timestamp:datetime, x:X) -> UpdateResultPositive:
        logger.info(f"Global routine end time updated to {timestamp}", extra=x)
        filter_doc = ConfigFilter(always_null=None)
        update_doc = ConfigUpdateSet(global_routine_end_time=None)
        result = await self._client.config.update_one(filter_doc, update_doc)
        return ensure_effective_update("config", filter_doc, result, MatchedCount)

    async def get_reddit_tokens_for_bot(self) -> TokensDocument:
        filter_doc = ConfigFilter(always_null=None)
        projection = ConfigProjection(bot_reddit_tokens=1)
        doc = await self._client.config.find_one(filter_doc, projection)
        if doc is None:
            raise DocumentNotFoundError("config", filter_doc)
        return doc["bot_reddit_tokens"]

    async def store_reddit_tokens_for_bot(self, access_token:str, refresh_token:str,
            expires_by:datetime) -> UpdateResultPositive:
        tokens:TokensDocument = {"access_token":access_token, "refresh_token":refresh_token, "expires_by":expires_by}
        filter_doc = ConfigFilter(always_null=None)
        update_doc = ConfigUpdateSet(bot_reddit_tokens=tokens)
        result = await self._client.config.update_one(filter_doc, update_doc)
        return ensure_effective_update("config", filter_doc, result, MatchedCount)

    async def get_reddit_tokens_for_host(self, username:str) -> TokensDocument:
        filter_doc = HostFilter(username=username)
        projection = HostProjection(reddit_tokens=1)
        host_doc = await self._client.hosts.find_one(filter_doc, projection)
        if host_doc is None:
            raise DocumentNotFoundError("hosts", filter_doc)
        tokens_doc = host_doc["reddit_tokens"]
        if tokens_doc is None:
            raise DocumentNotFoundError("hosts", filter_doc)
        return tokens_doc

    async def store_reddit_tokens_for_host(self, username:str, access_token:str,
            refresh_token:str, expires_by:datetime) -> UpdateResultPositive:
        tokens:TokensDocument = {"access_token":access_token, "refresh_token":refresh_token, "expires_by":expires_by}
        filter_doc = HostFilter(username=username)
        update_doc = HostUpdateSet(reddit_tokens=tokens)
        result = await self._client.hosts.update_one(filter_doc, update_doc)
        return ensure_effective_update("hosts", filter_doc, result, MatchedCount)

    async def erase_reddit_tokens_for_host(self, username:str) -> UpdateResultPositive:
        filter_doc = HostFilter(username=username)
        update_doc = HostUpdateSet(reddit_tokens=None)
        result = await self._client.hosts.update_one(filter_doc, update_doc)
        return ensure_effective_update("hosts", filter_doc, result, MatchedCount)

    """async def get_conected_player_names(self, gamecode:str, includeHost:bool = False) -> StrList:
        filter_doc = RoleFilter(gamecode=gamecode, active=True) if includeHost else RoleFilter(role=Player, gamecode=gamecode, active=True)
        cur = self._client.roles.find(filter_doc, RoleProjection(username=1))
        return await cur.distinct("username")

    async def count_connected_players(self, gamecode:str, includeHost:bool = False) -> int:
        filter_doc = RoleFilter(gamecode=gamecode, active=True) if includeHost else RoleFilter(role=Player, gamecode=gamecode, active=True)
        return await self._client.roles.count_documents(filter_doc)"""

    async def get_player_names(self, gamecode:str) -> StrTuple:
        filter_doc = GameFilter(gamecode=gamecode)
        projection = GameProjection(players=1)
        game_doc = await self._client.games.find_one(filter_doc, projection)
        if game_doc is None:
            raise DocumentNotFoundError("games", filter_doc)
        return tuple(player["username"] for player in game_doc["players"])

    async def count_connected_active_players(self, gamecode:str) -> int:
        # TODO: document that this counts ***game documents*** not player documents
        # This works for i == 1, but not for higher numbers
        filter_doc = GameFilter(gamecode=gamecode,
                    players=elem_match(PlayerDictCondition(active=True, reddit_tokens=conditionNotEqual(None))))
        return await self._client.games.count_documents(filter_doc)

    async def get_host_usernames(self) -> Sequence[str]:
        filter_doc = HostFilter()
        projection = HostProjection(username=1)
        cur = self._client.hosts.find(filter_doc, projection)
        return tuple(host["username"] for host in await cur.to_list(config.app.maxGames))

    async def add_user(self, username:str) -> InsertOneResult[TTrue]:
        result = await self._client.hosts.insert_one({"username":username, "reddit_tokens":None})
        return ensure_effective_insert_one("hosts", result)

    async def add_player(self, gamecode:str, username:str, path_keys:Sequence[PathKeyDocument]) -> UpdateResultPositive:
        filter_doc = GameFilter(gamecode=gamecode, players_username=conditionNotEqual(username))
        player_doc:PlayerDict = {"username":username, "active":True, "sort_order":0,
                "boldened_name":False, "last_seen_inbox":None, "last_seen_sent":None, "reddit_tokens":None} 
        if (path_keys_count := len(path_keys)) > 1:
            update_doc = GameUpdatePush(players=player_doc, path_keys=each(*path_keys))
        elif path_keys_count == 1:
            update_doc = GameUpdatePush(players=player_doc, path_keys=path_keys[0])
        else:
            raise ValueError(f"Expected to have at least 1 path key document to push, but got none ({path_keys})")
        result = await self._client.games.update_one(filter_doc, update_doc)
        return ensure_effective_update("games", filter_doc, result, ModifiedCount)

    async def remove_player(self, gamecode:str, username:str) -> UpdateResultPositive:
        filter_doc = GameFilter(gamecode=gamecode, players_username=username)
        players_condition = PlayerDictCondition(username=username)
        update_doc = GameUpdatePull(players=players_condition)
        result = await self._client.games.update_one(filter_doc, update_doc)
        return ensure_effective_update("games", filter_doc, result, MatchedCount)

    async def delete_host(self, username:str) -> DeleteResultPositive:
        filter_doc = HostFilter(username=username)
        result = await self._client.hosts.delete_one(filter_doc)
        return ensure_effective_deletion("hosts", filter_doc, result)

    async def delete_game(self, gamecode:str) -> DeleteResultPositive:
        filter_doc = GameFilter(gamecode=gamecode)
        result = await self._client.games.delete_one(filter_doc)
        return ensure_effective_deletion("games", filter_doc, result, False)

    async def set_player_active_state(self, gamecode:OptStr, username:str, active:bool) -> UpdateResultPositive:
        if gamecode is None:
            filter_doc = GameFilter(players_username=username)
        else:
            filter_doc = GameFilter(gamecode=gamecode, players_username=username)
        update_doc = GameUpdateSet(players_active=active)
        result = await self._client.games.update_one(filter_doc, update_doc)
        return ensure_effective_update("games", filter_doc, result, MatchedCount)

    async def set_player_sort_order(self, gamecode:OptStr, username:str, sort_order:int) -> UpdateResultPositive:
        if gamecode is None:
            filter_doc = GameFilter(players_username=username)
        else:
            filter_doc = GameFilter(gamecode=gamecode, players_username=username)
        update_doc = GameUpdateSet(players_sort_order=sort_order)
        result = await self._client.games.update_one(filter_doc, update_doc)
        return ensure_effective_update("games", filter_doc, result, MatchedCount)

    async def set_player_boldened_name(self, gamecode:OptStr, username:str, boldened_name:bool) -> UpdateResultPositive:
        if gamecode is None:
            filter_doc = GameFilter(players_username=username)
        else:
            filter_doc = GameFilter(gamecode=gamecode, players_username=username)
        update_doc = GameUpdateSet(players_boldened_name=boldened_name)
        result = await self._client.games.update_one(filter_doc, update_doc)
        return ensure_effective_update("games", filter_doc, result, MatchedCount)

    async def get_last_seen_ids(self, gamecode:str, username:str) -> LastSeenTuple:
        filter_doc = GameFilter(gamecode=gamecode, players_username=username)
        projection = GameProjection(players_elem=1)
        doc = await self._client.games.find_one(filter_doc, projection)
        if doc is None:
            raise DocumentNotFoundError("games", filter_doc)
        return (doc["players"][0]["last_seen_inbox"], doc["players"][0]["last_seen_sent"])

    async def update_last_seen_ids(self, gamecode:str, username:str,
            last_seen_inbox:OptStr, last_seen_sent:OptStr) -> UpdateResultPositive:
        filter_doc = GameFilter(gamecode=gamecode, players_username=username)
        update_doc = GameUpdateSet(players_last_seen_inbox=last_seen_inbox, players_last_seen_sent=last_seen_sent)
        result = await self._client.games.update_one(filter_doc, update_doc)
        return ensure_effective_update("games", filter_doc, result, MatchedCount)

    async def get_path_keys_for_game(self, gamecode:str) -> List[PathKeyDocument]:
        doc = await self._client.games.find_one(GameFilter(gamecode=gamecode), GameProjection(path_keys=1))
        if doc is None:
            raise DocumentNotFoundError("games", GameFilter(gamecode=gamecode))
        return doc["path_keys"]

    async def add_path_keys_for_game(self, gamecode:str, *path_keys:PathKeyDocument) -> UpdateResultPositive:
        count = len(path_keys)
        if count <= 0:
            raise DBError("games", f"No path keys to add: {path_keys}")
        elif count == 1:
            update_doc = GameUpdatePush(path_keys=path_keys[0])
        else:
            update_doc = GameUpdatePush(path_keys=each(*path_keys))
        filter_doc = GameFilter(gamecode=gamecode)
        result = await self._client.games.update_one(filter_doc, update_doc)
        return ensure_effective_update("games", filter_doc, result, ModifiedCount)

    async def get_path_hash(self, gamecode:str, url:WikiUrlSubreddit) -> str:
        filter_doc = GameFilter(gamecode=gamecode, path_hashes_url=str(url))
        projection_doc = GameProjection(path_hashes_elem=1)
        game_doc = await self._client.games.find_one(filter_doc, projection_doc)
        if game_doc is None:
            raise DocumentNotFoundError("games", filter_doc)
        return game_doc["path_hashes"][0]["hash_string"]

    async def update_path_hash(self, gamecode:str, url:WikiUrlSubreddit, new_hash:str) -> UpdateResultPositive:
        filter_doc = GameFilter(gamecode=gamecode, path_hashes_url=str(url))
        update_doc = GameUpdateSet(path_hashes_hash_string=new_hash)
        result = await self._client.games.update_one(filter_doc, update_doc)
        return ensure_effective_update("games", filter_doc, result, MatchedCount)

    async def add_path_hash(self, gamecode:str, url:WikiUrlSubreddit, new_hash:str) -> UpdateResultPositive:
        filter_doc = GameFilter(gamecode=gamecode)
        path_hash_doc:PathHashDict = {"url":str(url), "hash_string":new_hash}
        result = await self._client.games.update_one(filter_doc, GameUpdatePush(path_hashes=path_hash_doc))
        return ensure_effective_update("games", filter_doc, result, ModifiedCount)

    async def get_game_document(self, gamecode:str, schedule_tuple:bool = False) -> GameSDocumentTotal:
        #TODO: check schedule_tuple param
        doc = await self._client.games.find_one(GameFilter(gamecode=gamecode))
        if doc is None:
            raise DocumentNotFoundError("games", GameFilter(gamecode=gamecode))
        return doc

    async def get_messages(self, gamecode:str, excluded_messages:StrTuple = ()) -> Msgs:
        """Sorts by: root first, then creation date, so the messages in DB always come in chronological and logical order.
        Excluded PMs are excluded via this query, so the resulting list of PMs contains only those we actually want."""
        #TODO: ensure sorting is OK & document it
        #TODO: move the sorting keys creation in another submethod
        filter_doc = MessageFilter(gamecode=gamecode, name=conditionNotIn(*excluded_messages), root_id=conditionNotIn(*excluded_messages))
        sort1:Tuple[Literal["root_id"], SortDirection] = ("root_id", pymongo.ASCENDING)
        sort2:Tuple[Literal["createdOn"], SortDirection] = ("createdOn", pymongo.ASCENDING)
        sortKeys:List[Tuple[Union[Literal["root_id"], Literal["createdOn"]], SortDirection]] = [sort1, sort2]
        cur = self._client.messages.find(filter_doc).sort(sortKeys)
        return await cur.to_list(config.app.maxMessages)

    async def insert_new_messages(self, gamecode:str, *docs:MessageSDocumentTotal) -> Optional[InsertManyResult[TTrue]]:
        #TODO: document that excluded PMs are still stored in DB, exclusion is performed only at the writing stage of the game cycle
        #TODO: document the way we introspect BulkWriteError by searching for 'duplicate key error'
        #We could also use .startswith() with the MongoDB duplicate key error code (but again no hard guarantee)
        #Alternatively, we could rely on many insert_one calls (since we could just catch DuplicateKeyError), but that's inefficient
        if not docs:
            return None
        try:
            result = await self._client.messages.insert_many(docs, ordered=False)
        except BulkWriteError as error:
            details = error.details
            if details["writeConcernErrors"]:
                raise error
            for write_error in details["writeErrors"]:
                if "duplicate key error" not in write_error["errmsg"]:
                    raise error
            return None
        else:
            return ensure_effective_insert_many("messages", result, None)

    async def increment_game_update_count(self, gamecode:str) -> UpdateResultPositive:
        filter_doc = GameFilter(gamecode=gamecode)
        result = await self._client.games.update_one(filter_doc, GameUpdateIncrement(update_count=1))
        return ensure_effective_update("games", filter_doc, result, ModifiedCount)

    async def reset_last_seen_ids(self, gamecode:str) -> UpdateResultPositive:
        filter_doc = GameFilter(gamecode=gamecode)
        update_doc = GameUpdateSet(players_last_seen_inbox=None, players_last_seen_sent=None)
        result = await self._client.games.update_many(filter_doc, update_doc)
        return ensure_effective_update("games", filter_doc, result, MatchedCount)

    async def write_logs(self, *logs:LogSDocumentTotal) -> Optional[InsertManyResult[TTrue]]:
        if not logs:
            return None
        count = len(logs)
        result = await self._client.logs.insert_many(logs, ordered=True)
        return ensure_effective_insert_many("logs", result, count)

    async def find_logs(self, gamecode:str, sleepInterval:float = 5.0) -> AsyncIterator[LogSDocumentTotal]:
        filter_doc =  conditionOr(LogFilter(gamecode=gamecode, gameCycleName=conditionExists),
                LogFilter(gamecode=conditionNotExists, gameCycleName=conditionExists))
        cur = self._client.logs.find(filter_doc, cursor_type=CursorType.TAILABLE_AWAIT)
        while cur.alive:
            i = 0
            async for doc in cur:
                i += 1
                yield doc
            await asyncio.sleep(sleepInterval)

    async def send_manual_cycle_request(self, gamecode:str, opts:GameRoutineParams) -> InsertOneResult[TTrue]:
        doc:ManualCycleSDocumentTotal = {"gamecode":gamecode, "opts":opts, "communicationStep":0, "answer":None}
        result = await self._client.manualCycles.insert_one(doc)
        return ensure_effective_insert_one("manualCycles", result)

    async def await_manual_cycle_requests(self) -> AsyncIterator[ManualCycleSDocument]:
        filter_doc = ManualCycleFilter(communicationStep=0)
        update_doc = ManualCycleUpdateSet(communicationStep=1)
        projection = ManualCycleProjection(gamecode=1, opts=1)
        while True:
            next_doc = await self._client.manualCycles.find_one_and_update(filter_doc, update_doc, projection)
            if next_doc is None:
                await asyncio.sleep(1)
                continue
            yield next_doc

    async def answer_manual_cycle_request(self, gamecode:str, answer:bool) -> UpdateResultPositive:
        filter_doc = ManualCycleFilter(gamecode=gamecode, communicationStep=1)
        update_doc = ManualCycleUpdateSet(communicationStep=2, answer=answer)
        result = await self._client.manualCycles.update_one(filter_doc, update_doc)
        return ensure_effective_update("manualCycles", filter_doc, result, ModifiedCount)

    async def await_manual_cycle_answer(self, gamecode:str, timeout:float = 2.0) -> OptBool:
        #TODO: adapt wait durations/timeout from config object
        waitDuration, totalWait = 1.0, 0.0
        doc:Optional[ManualCycleSDocument] = None
        filter_doc = ManualCycleFilter(gamecode=gamecode, communicationStep=2)
        projection = ManualCycleProjection(answer=1)
        while doc is None and totalWait < timeout:
            doc = await self._client.manualCycles.find_one_and_delete(filter_doc, projection)
            logger.info(f"Cycle request document with answer: {doc}")
            if doc is None:
                await asyncio.sleep(waitDuration)
                totalWait += waitDuration
        if doc is None:
            return None
        elif doc["answer"] is None: 
            raise DBError("manualCycles", f"The answer from the worker should have been a boolean value, but got None: this is unexpected")
        return doc["answer"]

    async def remove_all_cycle_requests(self) -> DeleteResultPositive:
        filter_doc = ManualCycleFilter()
        result = await self._client.manualCycles.delete_many(filter_doc)
        return ensure_effective_deletion("manualCycles", filter_doc, result, ensure_deleted=False)
