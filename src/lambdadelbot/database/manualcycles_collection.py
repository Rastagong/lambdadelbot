# coding:utf-8
from __future__ import annotations
from datetime import datetime
from typing import overload, TypedDict
from ..constants.configuration import config
from ..constants.types import *
from .types import (DBCollection, ManualCyclesCollection, OQuery, Document, DocumentKind,
        UpdateSet, UpdateDocumentSet, Projection, Filter,  PBit, OPBit, O, N, Nothing, All)
__all__ = ["ManualCycleDocument", "ManualCycleFilter", "ManualCycleUpdateSet", "ManualCycleProjection",
        "ManualCycleSDocument", "ManualCycleSDocumentTotal"]

class ManualCycleSDocument(TypedDict, total=False):
    gamecode: str
    opts:GameRoutineParams
    communicationStep:int
    answer:OptBool

class ManualCycleSDocumentTotal(TypedDict):
    gamecode: str
    opts:GameRoutineParams
    communicationStep:int
    answer:OptBool


class ManualCycleDocument(Document[DocumentKind, ManualCyclesCollection]):

    @overload
    def __init__(self:ManualCycleDocument[UpdateSet], kind:DocumentKind, _id:O[str] = N,
            gamecode:O[str] = N, opts:O[GameRoutineParams] = N, communicationStep:O[int] = N, answer:O[OptBool] = N) -> None: ...

    @overload
    def __init__(self:ManualCycleDocument[Filter], kind:DocumentKind, _id:OQuery[str] = N,
            gamecode:OQuery[str] = N, opts:OQuery[GameRoutineParams] = N, communicationStep:OQuery[int] = N,
            answer:OQuery[OptBool] = N) -> None: ...

    @overload
    def __init__(self:ManualCycleDocument[Projection], kind:DocumentKind, _id:OPBit = N, gamecode:OPBit = N, opts:OPBit = N,
            communicationStep:OPBit = N, answer:OPBit = N) -> None: ...

    def __init__(self, kind:DocumentKind, _id:All[str] = N, gamecode:All[str] = N, opts:All[GameRoutineParams] = N,
            communicationStep:All[int] = N, answer:All[OptBool] = N) -> None:
        if _id is not N:
            self["_id"] = _id
        if gamecode is not N:
            self["gamecode"] = gamecode
        if opts is not N:
            self["opts"] = opts
        if communicationStep is not N:
            self["communicationStep"] = communicationStep
        if answer is not N:
            self["answer"] = answer


def ManualCycleUpdateSet(_id:O[str] = N, gamecode:O[str] = N, opts:O[GameRoutineParams] = N,
        communicationStep:O[int] = N, answer:O[OptBool] = N) -> UpdateDocumentSet[ManualCyclesCollection]:
    doc = ManualCycleDocument[UpdateSet]("update_set", _id=_id, gamecode=gamecode, opts=opts, communicationStep=communicationStep,
            answer=answer)
    return {"$set":doc}

def ManualCycleFilter(_id:OQuery[str] = N, gamecode:OQuery[str] = N, opts:OQuery[GameRoutineParams] = N,
        communicationStep:OQuery[int] = N, answer:OQuery[OptBool] = N) -> ManualCycleDocument[Filter]:
    filterDoc = ManualCycleDocument[Filter]("filter", _id=_id, gamecode=gamecode, opts=opts, communicationStep=communicationStep,
            answer=answer)
    return filterDoc

def ManualCycleProjection(_id:OPBit = N, gamecode:OPBit = N, opts:OPBit = N, communicationStep:OPBit = N,
        answer:OPBit = N) -> ManualCycleDocument[Projection]:
    doc = ManualCycleDocument[Projection]("projection", _id=_id, gamecode=gamecode, opts=opts, communicationStep=communicationStep, answer=answer)
    return doc


