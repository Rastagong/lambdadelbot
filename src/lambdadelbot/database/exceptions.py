#coding:utf-8
from typing import (Generic, Optional)
from ..constants.logging_configuration import X, OX
from ..constants.exceptions import LambdaError
from ..constants.types import (FFalse, TTrue)
from .results import _WriteResult, InsertManyResult, UpdateResultPositive, DeleteResultPositive
from .types import FilterQuery, Collection
__all__ = ["DBError", "DocumentNotFoundError",
        "UnacknowledgedError", "NotEnoughModificationsError", "NoMatchError",
        "NoModificationError", "NoDeletionError"]

class DBError(LambdaError, Generic[Collection]):
    def __init__(self, collection:Collection, msg:str, x:OX = None) -> None:
        self.collection:Collection = collection
        super().__init__(msg, x=x)


class DocumentNotFoundError(DBError[Collection]):
    def __init__(self, collection:Collection, filter_doc:FilterQuery[Collection], x:OX = None) -> None:
        self.filter_doc:FilterQuery[Collection] = filter_doc
        super().__init__(collection, f"Document not found in collection {collection} with filter {filter_doc}", x=x) #type:ignore


class UnacknowledgedError(DBError[Collection]):
    def __init__(self, collection:Collection, filter_doc:Optional[FilterQuery[Collection]],
            result:_WriteResult[FFalse], x:OX = None) -> None:
        self.filter_doc:Optional[FilterQuery[Collection]] = filter_doc
        self.result = result
        error_string = f"Failed to write to to the {collection} collection with filter {filter_doc}, the result was not acknowledged"
        super().__init__(collection, error_string, x=x) #type:ignore


class NotEnoughModificationsError(DBError[Collection]):
    def __init__(self, collection:Collection, expected_count:int, actual_count:int, 
            result:InsertManyResult[TTrue], x:OX = None) -> None:
        self.result, self.expected_count, self.actual_count = result, expected_count, actual_count
        error_string  = f"Failed to insert the {expected_count} documents in the {collection} collection, "
        error_string += f"only {actual_count} documents were inserted"
        super().__init__(collection, error_string, x=x) #type:ignore


class NoMatchError(DBError[Collection]):
    def __init__(self, collection:Collection, filter_doc:FilterQuery[Collection],
            result:UpdateResultPositive, x:OX = None) -> None:
        self.filter_doc:FilterQuery[Collection] = filter_doc
        self.result = result
        error_string = f"Failed to update any documents in the {collection} collection, no match with filter {filter_doc}"
        super().__init__(collection, error_string, x=x) #type:ignore


class NoModificationError(DBError[Collection]):
    def __init__(self, collection:Collection, filter_doc:FilterQuery[Collection],
            result:UpdateResultPositive, x:OX = None) -> None:
        self.filter_doc:FilterQuery[Collection] = filter_doc
        self.result = result
        error_string = f"Failed to update any documents in the {collection} collection, no actual modification with filter {filter_doc}"
        super().__init__(collection, error_string, x=x) #type:ignore


class NoDeletionError(DBError[Collection]):
    def __init__(self, collection:Collection, filter_doc:FilterQuery[Collection],
            result:DeleteResultPositive, x:OX = None) -> None:
        self.filter_doc:FilterQuery[Collection] = filter_doc
        self.result = result
        error_string = f"Failed to delete any documents in the {collection} collection, no match with filter {filter_doc}"
        super().__init__(collection, error_string, x=x) #type:ignore

