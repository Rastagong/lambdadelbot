# coding:utf-8
from __future__ import annotations
from datetime import datetime
from typing import Any, List, Literal, Optional, overload, TypedDict, Union
from ..constants.configuration import config
from ..constants.types import *
from .types import (DBCollection, HostsCollection, OQuery, Document, DocumentKind,
        TokensDocument, OptTokensDocument,
        UpdateSet, UpdateDocumentSet, Projection, Filter,  PBit, OPBit, O, N, Nothing, All)
__all__ = ["HostDocument", "HostFilter", "HostUpdateSet", "HostProjection", "HostSDocument", "HostSDocumentTotal",
        "TokensDocument", "TokensDocumentTemplate", "OptTokensDocument"]


class HostSDocument(TypedDict, total=False):
    username:str
    reddit_tokens:OptTokensDocument

class HostSDocumentTotal(TypedDict):
    username:str
    reddit_tokens:OptTokensDocument


class HostDocument(Document[DocumentKind, HostsCollection]):

    @overload
    def __init__(self:HostDocument[UpdateSet], kind:DocumentKind, _id:O[str] = N, username:O[str] = N,
            reddit_tokens:O[OptTokensDocument] = N) -> None: ...

    @overload
    def __init__(self:HostDocument[Filter], kind:DocumentKind, _id:OQuery[str] = N, username:OQuery[str] = N,
            reddit_tokens:OQuery[OptTokensDocument] = N) -> None: ...

    @overload
    def __init__(self:HostDocument[Projection], kind:DocumentKind, _id:OPBit = N, username:OPBit = N, reddit_tokens:OPBit = N) -> None: ...

    def __init__(self, kind:DocumentKind, _id:All[str] = N, username:All[str] = N, reddit_tokens:All[OptTokensDocument] = N) -> None:
        if kind == "projection" and _id is N:
            _id = 0
        if _id is not N:
            self["_id"] = _id
        if username is not N:
            self["username"] = username
        if reddit_tokens is not N:
            self["reddit_tokens"] = reddit_tokens


def HostUpdateSet(_id:O[str] = N, username:O[str] = N,
        reddit_tokens:O[OptTokensDocument] = N) -> UpdateDocumentSet[HostsCollection]:
    doc = HostDocument[UpdateSet]("update_set", _id=_id, username=username, reddit_tokens=reddit_tokens)
    return {"$set":doc}

def HostFilter(_id:OQuery[str] = N, username:OQuery[str] = N, reddit_tokens:OQuery[OptTokensDocument] = N) -> HostDocument[Filter]:
    filterDoc = HostDocument[Filter]("filter", _id=_id, username=username, reddit_tokens=reddit_tokens)
    return filterDoc

def HostProjection(_id:OPBit = N, username:OPBit = N, reddit_tokens:OPBit = N) -> HostDocument[Projection]:
    doc = HostDocument[Projection]("projection", _id=_id, username=username, reddit_tokens=reddit_tokens)
    return doc

