#coding:utf-8
from abc import abstractmethod
from typing import Any, Generic, Literal, Mapping, Optional, Protocol, Sequence, TypeVar
from ..constants.types import *

A = TypeVar("A", TTrue, FFalse)

class _WriteResult(Generic[A]):
    acknowledged: A


class InsertOneResult(_WriteResult[A]):
    inserted_id: int

class InsertManyResult(_WriteResult[A]):
    inserted_ids: Sequence[int]


class UpdateResultABC:
    @abstractmethod
    def raw_result(self) -> Mapping[str, Any]: ...

class UpdateResultPositive(UpdateResultABC, _WriteResult[TTrue]):
    matched_count: int
    modified_count: int
    upserted_id: Optional[int]

class UpdateResultNegative(UpdateResultABC, _WriteResult[FFalse]):
    pass

UpdateResult = TypeVar("UpdateResult", UpdateResultPositive, UpdateResultNegative)


class DeleteResultABC:
    @abstractmethod
    def raw_result(self) -> Mapping[str, Any]: ...

class DeleteResultPositive(DeleteResultABC, _WriteResult[TTrue]):
    deleted_count: int

class DeleteResultNegative(DeleteResultABC, _WriteResult[FFalse]):
    pass

DeleteResult = TypeVar("DeleteResult", DeleteResultPositive, DeleteResultNegative)

