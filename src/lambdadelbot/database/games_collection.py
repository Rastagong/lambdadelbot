# coding:utf-8
from __future__ import annotations
from datetime import datetime
from typing import Any, List, Literal, Mapping, NamedTuple, Optional, overload, Sequence, Tuple, TypedDict, TypeVar, Union
from ..constants.configuration import config
from ..constants.types import (FFalse, GameStateValue, OptDatetime, OptStr, StrList, TTrue)
from ..structs.week_time import WeekTime
from .types import (DBCollection, GamesCollection, Query, OQuery, OQueryArray, Document, DocumentKind,
        TokensDocument, OptTokensDocument, OperatorIn,
        UpdateSet, UpdatePush, UpdatePull, UpdateIncrement,
        UpdateDocumentSet, UpdateDocumentPush, UpdateDocumentPull, UpdateDocumentIncrement, 
        Projection, Filter,  OPBit, O, N, Nothing, All, AllArray, AllElem, OPushValue)
__all__ = ["ScheduleDocument", "ScheduleDict", "ScheduleDictTemplate", "PathHashDict", "PathHashTemplate", "GameLockDict",
        "PathKeyDefaultDict", "PathKeySingleDict", "PathKeySingleDict", "PathKeyCoupleDict", "PathKeyDocument", "PathKeyDocumentCondition", "PK",
        "GameDocument", "GameFilter", "GameUpdateSet", "GameUpdatePush", "GameUpdatePull", "GameUpdateIncrement", "GameProjection",
        "GameSDocument", "GameSDocumentTotal", "GameDict", "GameDictTemplate", "makeGameDict"]

class ScheduleDocument(TypedDict):
    start: datetime
    end: datetime

class ScheduleDict(TypedDict):
    start: WeekTime
    end: WeekTime


class PathHashDict(TypedDict):
    url: str
    hash_string: str

class PathHashDictCondition(TypedDict, total=False):
    url:str
    hash_string:str


class GameLockDict(TypedDict):
    free: bool
    process: OptStr
    acquiredTime: OptDatetime
    opName: OptStr


PKTypeSingle = Tuple[str, None, FFalse]
PKTypeCouple = Tuple[str, str, FFalse]
PKTypeDefault = Tuple[None, None, TTrue]

class PathKeyDefaultDocument(TypedDict):
    key:str
    users: PKTypeDefault

class PathKeySingleDocument(TypedDict):
    key:str
    users:PKTypeSingle

class PathKeyCoupleDocument(TypedDict):
    key:str
    users:PKTypeCouple

PathKeyDocument = Union[PathKeyDefaultDocument, PathKeySingleDocument, PathKeyCoupleDocument]

PathKeyDocumentCondition = TypedDict("PathKeyDocumentCondition",
    {
        "key":str,
        "users":Union[PathKeyDocument, OperatorIn[Union[OptStr, bool]]],
        "users.0":OptStr,
        "users.1":OptStr,
        "users.2":bool
    }, total=False)


class PlayerDict(TypedDict):
    username: str
    active: bool
    sort_order: int
    boldened_name: bool
    last_seen_inbox:OptStr
    last_seen_sent:OptStr
    reddit_tokens:OptTokensDocument

class PlayerDictCondition(TypedDict, total=False):
    username: Query[str]
    active: Query[bool]
    sort_order: Query[int]
    boldened_name: Query[bool]
    last_seen_inbox:Query[OptStr]
    last_seen_sent:Query[OptStr]
    reddit_tokens:Query[OptTokensDocument]


class GameDocument(Document[DocumentKind, GamesCollection]):

    @overload
    def __init__(self:GameDocument[UpdateIncrement], kind:DocumentKind, _id:Nothing = N,
            gamecode:Nothing = N, gamecode_display:Nothing = N,
            title:Nothing = N, host_username:Nothing = N,
            subreddit_name:Nothing = N, schedule:Nothing = N,
            path_hashes:Nothing = N, path_keys:Nothing = N, players:Nothing = N,
            path_hashes_hash_string:Nothing = N, path_hashes_url:Nothing = N,
            players_username:Nothing = N, players_active:Nothing = N, players_sort_order:Nothing = N,
            players_boldened_name:Nothing = N, players_reddit_tokens:Nothing = N,
            players_last_seen_inbox:Nothing = N, players_last_seen_sent:Nothing = N,
            path_hashes_elem:Nothing = N, path_keys_elem:Nothing = N, players_elem:Nothing = N, 
            update_count:int = 0, excluded_messages:Nothing = N, lock:Nothing = N, game_state:Nothing = N,
            lockFree:Nothing = N, lockProcess:Nothing = N, lockOpName:Nothing = N, lockAcquiredTime:Nothing = N) -> None: ...

    @overload
    def __init__(self:GameDocument[UpdateSet], kind:DocumentKind, _id:O[str] = N,
            gamecode:O[str] = N, gamecode_display:O[str] = N,
            title:O[str] = N, host_username:O[str] = N,
            subreddit_name:O[str] = N, schedule:O[ScheduleDocument] = N,
            path_hashes:O[List[PathHashDict]] = N, path_keys:O[List[PathKeyDocument]] = N, players:O[List[PlayerDict]] = N,
            path_hashes_hash_string:O[str] = N, path_hashes_url:O[str] = N,
            players_username:O[str] = N, players_active:O[bool] = N,
            players_sort_order:O[int] = N, players_boldened_name:O[bool] = N,
            players_reddit_tokens:O[OptTokensDocument] = N,
            players_last_seen_inbox:O[OptStr] = N, players_last_seen_sent:O[OptStr] = N,
            path_hashes_elem:Nothing = N, path_keys_elem:Nothing = N, players_elem:Nothing = N,
            update_count:O[int] = N, excluded_messages:O[StrList] = N, lock:O[GameLockDict] = N, game_state:O[GameStateValue] = N,
            lockFree:O[bool] = N, lockProcess:O[OptStr] = N, lockOpName:O[OptStr] = N,
            lockAcquiredTime:O[OptDatetime] = N) -> None: ...

    @overload
    def __init__(self:GameDocument[Filter], kind:DocumentKind, _id:OQuery[str] = N,
            gamecode:OQuery[str] = N, gamecode_display:OQuery[str] = N,
            title:OQuery[str] = N, host_username:OQuery[str] = N,
            subreddit_name:OQuery[str] = N, schedule:OQuery[ScheduleDocument] = N,
            path_hashes:OQuery[List[PathHashDict]] = N, path_keys:OQuery[List[PathKeyDocument]] = N,
                 players:OQueryArray[List[PlayerDict], PlayerDictCondition] = N,
            path_hashes_hash_string:OQuery[str] = N, path_hashes_url:OQuery[str] = N,
            players_username:OQuery[str] = N, players_active:OQuery[bool] = N,
            players_sort_order:OQuery[int] = N, players_boldened_name:OQuery[bool] = N,
            players_reddit_tokens:OQuery[OptTokensDocument] = N,
            players_last_seen_inbox:OQuery[OptStr] = N, players_last_seen_sent:OQuery[OptStr] = N,
            path_hashes_elem:Nothing = N, path_keys_elem:Nothing = N, players_elem:Nothing = N,
            update_count:OQuery[int] = N, excluded_messages:OQuery[StrList] = N, lock:OQuery[GameLockDict] = N, game_state:OQuery[GameStateValue] = N,
            lockFree:OQuery[bool] = N, lockProcess:OQuery[OptStr] = N, lockOpName:OQuery[OptStr] = N,
            lockAcquiredTime:OQuery[OptDatetime] = N) -> None: ...

    @overload
    def __init__(self:GameDocument[UpdatePush], kind:DocumentKind, _id:Nothing = N,
            gamecode:Nothing = N, gamecode_display:Nothing = N,
            title:Nothing = N, host_username:Nothing = N,
            subreddit_name:Nothing = N,  schedule:Nothing = N,
            path_hashes:OPushValue[PathHashDict] = N, path_keys:OPushValue[PathKeyDocument] = N, players:OPushValue[PlayerDict] = N,
            path_hashes_hash_string:Nothing = N, path_hashes_url:Nothing = N,
            players_username:Nothing = N, players_active:Nothing = N,
            players_sort_order:Nothing = N, players_boldened_name:Nothing = N,
            players_reddit_tokens:Nothing = N,
            players_last_seen_inbox:Nothing = N, players_last_seen_sent:Nothing = N,
            path_hashes_elem:Nothing = N, path_keys_elem:Nothing = N, players_elem:Nothing = N,
            update_count:Nothing = N, excluded_messages:Nothing = N, lock:Nothing = N, game_state:Nothing = N,
            lockFree:Nothing = N, lockProcess:Nothing = N, lockOpName:Nothing = N, lockAcquiredTime:Nothing = N) -> None: ...

    @overload
    def __init__(self:GameDocument[UpdatePull], kind:DocumentKind, _id:Nothing = N,
            gamecode:Nothing = N, gamecode_display:Nothing = N,
            title:Nothing = N, host_username:Nothing = N,
            subreddit_name:Nothing = N,  schedule:Nothing = N,
            path_hashes:O[PathHashDictCondition] = N, path_keys:O[PathKeyDocumentCondition] = N, players:O[PlayerDictCondition] = N,
            path_hashes_hash_string:Nothing = N, path_hashes_url:Nothing = N,
            players_username:Nothing = N, players_active:Nothing = N,
            players_sort_order:Nothing = N, players_boldened_name:Nothing = N,
            players_reddit_tokens:Nothing = N,
            players_last_seen_inbox:Nothing = N, players_last_seen_sent:Nothing = N,
            path_hashes_elem:Nothing = N, path_keys_elem:Nothing = N, players_elem:Nothing = N,
            update_count:Nothing = N, excluded_messages:Nothing = N, lock:Nothing = N, game_state:Nothing = N,
            lockFree:Nothing = N, lockProcess:Nothing = N, lockOpName:Nothing = N, lockAcquiredTime:Nothing = N) -> None: ...

    @overload
    def __init__(self:GameDocument[Projection], kind:DocumentKind, _id:OPBit = N,
            gamecode:OPBit = N, gamecode_display:OPBit = N,
            title:OPBit = N, host_username:OPBit = N,
            subreddit_name:OPBit = N,
            schedule:OPBit = N, path_hashes:OPBit = N, path_keys:OPBit = N, players:OPBit = N,
            path_hashes_hash_string:Nothing = N, path_hashes_url:Nothing = N,
            players_username:Nothing = N, players_active:Nothing = N,
            players_sort_order:Nothing = N, players_boldened_name:Nothing = N,
            players_reddit_tokens:Nothing = N,
            players_last_seen_inbox:Nothing = N, players_last_seen_sent:Nothing = N,
            path_hashes_elem:OPBit = N, path_keys_elem:OPBit = N, players_elem:OPBit = N,
            update_count:OPBit = N, excluded_messages:OPBit = N, lock:OPBit = N, game_state:OPBit = N,
            lockFree:OPBit = N, lockProcess:OPBit = N, lockOpName:OPBit = N, lockAcquiredTime:OPBit = N) -> None: ...

    def __init__(self, kind:DocumentKind, _id:All[str] = N,
            gamecode:All[str] = N, gamecode_display:All[str] = N,
            title:All[str] = N, host_username:All[str] = N,
            subreddit_name:All[str] = N, schedule:All[ScheduleDocument] = N,
            path_hashes:AllArray[PathHashDict, PathHashDictCondition] = N,
            path_keys:AllArray[PathKeyDocument, PathKeyDocumentCondition] = N,
            players:AllArray[PlayerDict, PlayerDictCondition] = N,
            path_hashes_hash_string:AllElem[str] = N, path_hashes_url:AllElem[str] = N,
            players_username:AllElem[str] = N, players_active:AllElem[bool] = N,
            players_sort_order:AllElem[int] = N, players_boldened_name:AllElem[bool] = N,
            players_reddit_tokens:AllElem[OptTokensDocument] = N,
            players_last_seen_inbox:AllElem[OptStr] = N, players_last_seen_sent:AllElem[OptStr] = N,
            path_hashes_elem:OPBit = N, path_keys_elem:OPBit = N, players_elem:OPBit = N,
            update_count:All[int] = N, excluded_messages:All[StrList] = N, lock:All[GameLockDict] = N, game_state:All[GameStateValue] = N,
            lockFree:All[bool] = N, lockProcess:All[OptStr] = N, lockOpName:All[OptStr] = N, lockAcquiredTime:All[OptDatetime] = N)-> None:
        if kind == "projection" and _id is N:
            _id = 0
        if path_hashes_hash_string is not N:
            if kind == "filter":
                self["path_hashes.hash_string"] = path_hashes_hash_string
            elif kind == "update_set":
                self["path_hashes.$.hash_string"] = path_hashes_hash_string
        if path_hashes_url is not N:
            if kind == "filter":
                self["path_hashes.url"] = path_hashes_url
            elif kind == "update_set":
                self["path_hashes.$.url"] = path_hashes_url
        if players_username is not N:
            if kind == "filter":
                self["players.username"] = players_username
            elif kind == "update_set":
                self["players.$.username"] = players_username
        if players_active is not N:
            if kind == "filter":
                self["players.active"] = players_active
            elif kind == "update_set":
                self["players.$.active"] = players_active
        if players_sort_order is not N:
            if kind == "filter":
                self["players.sort_order"] = players_sort_order
            elif kind == "update_set":
                self["players.$.sort_order"] = players_sort_order
        if players_boldened_name is not N:
            if kind == "filter":
                self["players.boldened_name"] = players_boldened_name
            elif kind == "update_set":
                self["players.$.boldened_name"] = players_boldened_name
        if players_reddit_tokens is not N:
            if kind == "filter":
                self["players.reddit_tokens"] = players_reddit_tokens
            elif kind == "update_set":
                self["players.$.reddit_tokens"] = players_reddit_tokens
        if players_last_seen_inbox is not N:
            if kind == "filter":
                self["players.last_seen_inbox"] = players_last_seen_inbox
            elif kind == "update_set":
                self["players.$.last_seen_inbox"] = players_last_seen_inbox
        if players_last_seen_sent is not N:
            if kind == "filter":
                self["players.last_seen_sent"] = players_last_seen_sent
            elif kind == "update_set":
                self["players.$.last_seen_sent"] = players_last_seen_sent
        if path_hashes_elem is not N:
            self["path_hashes.$"] = path_hashes_elem
        if path_keys_elem is not N:
            self["path_keys.$"] = path_keys_elem
        if players_elem is not N:
            self["players.$"] = players_elem
        if _id is not N:
            self["_id"] = _id
        if gamecode is not N:
            self["gamecode"] = gamecode
        if gamecode_display is not N:
            self["gamecode_display"] = gamecode_display
        if title is not N:
            self["title"] = title
        if host_username is not N:
            self["host_username"] = host_username
        if subreddit_name is not N:
            self["subreddit_name"] = subreddit_name
        if schedule is not N:
            self["schedule"] = schedule
        if path_hashes is not N:
            self["path_hashes"] = path_hashes
        if path_keys is not N:
            self["path_keys"] = path_keys
        if players is not N:
            self["players"] = players
        if update_count is not N:
            self["update_count"] = update_count
        if excluded_messages is not N:
            self["excluded_messages"] = excluded_messages
        if lock is not N:
            self["lock"] = lock
        if game_state is not N:
            self["game_state"] = game_state
        if lockFree is not N:
            self["lock.free"] = lockFree
        if lockProcess is not N:
            self["lock.process"] = lockProcess
        if lockOpName is not N:
            self["lock.opName"] = lockOpName
        if lockAcquiredTime is not N:
            self["lock.acquiredTime"] = lockAcquiredTime

def GameFilter(_id:OQuery[str] = N,
        gamecode:OQuery[str] = N, gamecode_display:OQuery[str] = N,
        title:OQuery[str] = N, host_username:OQuery[str] = N,
        subreddit_name:OQuery[str] = N,
        schedule:OQuery[ScheduleDocument] = N,
        path_hashes:OQuery[List[PathHashDict]] = N, path_keys:OQuery[List[PathKeyDocument]] = N,
        players:OQueryArray[List[PlayerDict], PlayerDictCondition] = N,
        path_hashes_hash_string:OQuery[str] = N, path_hashes_url:OQuery[str] = N, 
        players_username:OQuery[str] = N, players_active:OQuery[bool] = N,
        players_sort_order:OQuery[int] = N, players_boldened_name:OQuery[bool] = N,
        players_reddit_tokens:OQuery[OptTokensDocument] = N,
        players_last_seen_inbox:OQuery[OptStr] = N, players_last_seen_sent:OQuery[OptStr] = N,
       update_count:OQuery[int] = N, excluded_messages:OQuery[StrList] = N, lock:OQuery[GameLockDict] = N, game_state:OQuery[GameStateValue] = N,
       lockFree:OQuery[bool] = N, lockProcess:OQuery[OptStr] = N, lockOpName:OQuery[OptStr] = N,
       lockAcquiredTime:OQuery[OptDatetime] = N) -> GameDocument[Filter]:
    filterDoc = GameDocument[Filter]("filter", _id=_id,
            gamecode=gamecode, gamecode_display=gamecode_display,
            title=title, host_username=host_username,
            subreddit_name=subreddit_name, schedule=schedule,
            path_hashes=path_hashes, path_keys=path_keys, players=players,
            path_hashes_hash_string=path_hashes_hash_string, path_hashes_url=path_hashes_url,
            players_username=players_username, players_active=players_active,
            players_sort_order=players_sort_order, players_boldened_name=players_boldened_name,
            players_reddit_tokens=players_reddit_tokens,
            players_last_seen_inbox=players_last_seen_inbox, players_last_seen_sent=players_last_seen_sent,
            update_count=update_count, excluded_messages=excluded_messages, lock=lock, game_state=game_state,
            lockFree=lockFree, lockProcess=lockProcess, lockOpName=lockOpName, lockAcquiredTime=lockAcquiredTime)
    return filterDoc

def GameUpdateSet(_id:O[str] = N,
        gamecode:O[str] = N, gamecode_display:O[str] = N,
        title:O[str] = N, host_username:O[str] = N,
        subreddit_name:O[str] = N, schedule:O[ScheduleDocument] = N,
        path_hashes:O[List[PathHashDict]] = N, path_keys:O[List[PathKeyDocument]] = N, players:O[List[PlayerDict]] = N,
        path_hashes_hash_string:O[str] = N, path_hashes_url:O[str] = N,
        players_username:O[str] = N, players_active:O[bool] = N,
        players_sort_order:O[int] = N, players_boldened_name:O[bool] = N,
        players_reddit_tokens:O[OptTokensDocument] = N,
        players_last_seen_inbox:O[OptStr] = N, players_last_seen_sent:O[OptStr] = N,
        update_count:O[int] = N, excluded_messages:O[StrList] = N, lock:O[GameLockDict] = N, game_state:O[GameStateValue] = N,
        lockFree:O[bool] = N, lockProcess:O[OptStr] = N, lockOpName:O[OptStr] = N,
        lockAcquiredTime:O[OptDatetime] = N) -> UpdateDocumentSet[GamesCollection]:
    doc = GameDocument[UpdateSet]("update_set", _id=_id,
            gamecode=gamecode, gamecode_display=gamecode_display,
            title=title, host_username=host_username,
            subreddit_name=subreddit_name, schedule=schedule,
            path_hashes=path_hashes, path_keys=path_keys, players=players,
            path_hashes_hash_string=path_hashes_hash_string, path_hashes_url=path_hashes_url,
            players_username=players_username, players_active=players_active,
            players_sort_order=players_sort_order, players_boldened_name=players_boldened_name,
            players_reddit_tokens=players_reddit_tokens,
            players_last_seen_inbox=players_last_seen_inbox, players_last_seen_sent=players_last_seen_sent,
            update_count=update_count, excluded_messages=excluded_messages, lock=lock, game_state=game_state,
            lockFree=lockFree, lockProcess=lockProcess, lockOpName=lockOpName, lockAcquiredTime=lockAcquiredTime)
    return {"$set":doc}


def GameUpdatePush(path_hashes:OPushValue[PathHashDict] = N,
        path_keys:OPushValue[PathKeyDocument] = N,
        players:OPushValue[PlayerDict] = N) -> UpdateDocumentPush[GamesCollection]:
    doc = GameDocument[UpdatePush]("update_push", path_hashes=path_hashes, path_keys=path_keys, players=players)
    return {"$push":doc}


def GameUpdatePull(path_hashes:O[PathHashDictCondition] = N,
        path_keys:O[PathKeyDocumentCondition] = N,
        players:O[PlayerDictCondition] = N) -> UpdateDocumentPull[GamesCollection]:
    doc = GameDocument[UpdatePull]("update_pull", path_hashes=path_hashes, path_keys=path_keys, players=players)
    return {"$pull":doc}


def GameUpdateIncrement(update_count:int = 0) -> UpdateDocumentIncrement[GamesCollection]:
    doc = GameDocument[UpdateIncrement]("update_increment", update_count=update_count)
    return {"$inc":doc}


def GameProjection(_id:OPBit = N,
        gamecode:OPBit = N, gamecode_display:OPBit = N,
        title:OPBit = N, host_username:OPBit = N,
        subreddit_name:OPBit = N, schedule:OPBit = N,
        path_hashes:OPBit = N, path_keys:OPBit = N, players:OPBit = N,
        path_hashes_elem:OPBit = N, path_keys_elem:OPBit = N, players_elem:OPBit = N,
        update_count:OPBit = N, excluded_messages:OPBit = N, lock:OPBit = N, game_state:OPBit = N,
        lockFree:OPBit = N, lockProcess:OPBit = N, lockOpName:OPBit = N, lockAcquiredTime:OPBit = N) -> GameDocument[Projection]:
    doc = GameDocument[Projection]("projection", _id=_id,
            gamecode=gamecode, gamecode_display=gamecode_display,
            title=title, host_username=host_username,
            subreddit_name=subreddit_name, schedule=schedule,
            path_hashes=path_hashes, path_keys=path_keys, players=players,
            path_hashes_elem=path_hashes_elem, path_keys_elem=path_keys_elem, players_elem=players_elem,
            update_count=update_count, excluded_messages=excluded_messages, lock=lock, game_state=game_state,
            lockFree=lockFree, lockProcess=lockProcess, lockOpName=lockOpName, lockAcquiredTime=lockAcquiredTime)
    return doc

class GameSDocument(TypedDict, total=False):
    gamecode: str
    gamecode_display: str
    title: str
    host_username:str
    subreddit_name: str
    schedule: ScheduleDocument
    game_state: GameStateValue
    path_hashes: List[PathHashDict]
    path_keys: List[PathKeyDocument]
    players: List[PlayerDict]
    update_count: int
    excluded_messages: StrList
    lock: GameLockDict

class GameSDocumentTotal(TypedDict):
    gamecode: str
    gamecode_display: str
    title: str
    host_username:str
    subreddit_name: str
    schedule: ScheduleDocument
    game_state: GameStateValue
    path_hashes: List[PathHashDict]
    path_keys: List[PathKeyDocument]
    players: List[PlayerDict]
    update_count: int
    excluded_messages: StrList
    lock: GameLockDict


class PlayerStatus(NamedTuple):
    username:str
    active:bool
    sort_order:int
    boldened_name: bool
    connected:bool

PlayersMapping = Mapping[str, PlayerStatus]

def players_as_mapping(docs:Sequence[PlayerDict]) -> PlayersMapping:
    return {doc["username"] : PlayerStatus(doc["username"], doc["active"],
        doc["sort_order"], doc["boldened_name"], doc["reddit_tokens"] is not None)
            for doc in docs}

