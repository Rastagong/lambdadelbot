# coding:utf-8
from __future__ import annotations
from datetime import datetime
from typing import overload, Tuple, TypedDict
from ..constants.configuration import config
from ..constants.types import *
from .types import (DBCollection, ConfigCollection, OQuery, Document, DocumentKind,
        UpdateSet, UpdateDocumentSet, Projection, Filter,  PBit, OPBit, O, N, Nothing, All, AllElem,
        TokensDocument)
__all__ = ["ConfigDocument", "ConfigFilter", "ConfigUpdateSet", "ConfigProjection",
        "ConfigSDocument", "ConfigSDocumentTotal"]

class GlobalRoutineDocument(TypedDict):
    start_time: datetime
    end_time: OptDatetime
GlobalRoutineTuple = Tuple[datetime, OptDatetime]

class ConfigSDocumentTotal(TypedDict):
    always_null: None
    bot_reddit_tokens: TokensDocument
    global_routine: GlobalRoutineDocument

class ConfigSDocument(TypedDict, total=False):
    always_null: None
    bot_reddit_tokens: TokensDocument
    global_routine: GlobalRoutineDocument


class ConfigDocument(Document[DocumentKind, ConfigCollection]):

    @overload
    def __init__(self:ConfigDocument[UpdateSet], kind:DocumentKind, _id:O[str] = N,
            always_null:O[None] = N,
            bot_reddit_tokens:O[TokensDocument] = N,
            global_routine_start_time:O[datetime] = N,
            global_routine_end_time:O[OptDatetime] = N,
            global_routine:O[GlobalRoutineDocument] = N) -> None: ...

    @overload
    def __init__(self:ConfigDocument[Filter], kind:DocumentKind, _id:OQuery[str] = N,
            always_null:OQuery[None] = N,
            bot_reddit_tokens:OQuery[TokensDocument] = N,
            global_routine_start_time:OQuery[datetime] = N,
            global_routine_end_time:OQuery[OptDatetime] = N,
            global_routine:OQuery[GlobalRoutineDocument] = N) -> None: ...

    @overload
    def __init__(self:ConfigDocument[Projection], kind:DocumentKind, _id:OPBit = N,
            always_null:OPBit = N,
            bot_reddit_tokens:OPBit = N,
            global_routine_start_time:Nothing = N,
            global_routine_end_time:Nothing = N,
            global_routine:OPBit = N) -> None: ...

    def __init__(self, kind:DocumentKind, _id:All[str] = N,
            always_null:All[None] = N,
            bot_reddit_tokens:All[TokensDocument] = N,
            global_routine_start_time:AllElem[datetime] = N,
            global_routine_end_time:AllElem[OptDatetime] = N,
            global_routine:All[GlobalRoutineDocument] = N) -> None:
        if kind == "projection" and _id is N:
            _id = 0
        if _id is not N:
            self["_id"] = _id
        if always_null is not N:
            self["always_null"] = always_null
        if bot_reddit_tokens is not N:
            self["bot_reddit_tokens"] = bot_reddit_tokens
        if global_routine is not N:
            self["global_routine"] = global_routine
        if global_routine_start_time is not N:
            self["global_routine.start_time"] = global_routine_start_time
        if global_routine_end_time is not N:
            self["global_routine.end_time"] = global_routine_end_time


def ConfigUpdateSet(_id:O[str] = N, always_null:O[None] = N, bot_reddit_tokens:O[TokensDocument] = N,
        global_routine_start_time:O[datetime] = N, global_routine_end_time:O[OptDatetime] = N,
        global_routine:O[GlobalRoutineDocument] = N) -> UpdateDocumentSet[ConfigCollection]:
    doc = ConfigDocument[UpdateSet]("update_set", _id=_id, bot_reddit_tokens=bot_reddit_tokens,
            global_routine_start_time=global_routine_start_time,
            global_routine_end_time=global_routine_end_time, global_routine=global_routine)
    return {"$set":doc}

def ConfigFilter(_id:OQuery[str] = N, always_null:OQuery[None] = N,
        bot_reddit_tokens:OQuery[TokensDocument] = N,
        global_routine_start_time:OQuery[datetime] = N,
        global_routine_end_time:OQuery[OptDatetime] = N,
        global_routine:OQuery[GlobalRoutineDocument] = N) -> ConfigDocument[Filter]:
    doc = ConfigDocument[Filter]("filter", _id=_id, always_null=always_null,
            bot_reddit_tokens=bot_reddit_tokens,
            global_routine_start_time=global_routine_start_time,
            global_routine_end_time=global_routine_end_time, global_routine=global_routine)
    return doc

def ConfigProjection(_id:OPBit = N, always_null:OPBit = N,
        bot_reddit_tokens:OPBit = N, global_routine:OPBit = N) -> ConfigDocument[Projection]:
    doc = ConfigDocument[Projection]("projection", _id=_id, always_null=always_null,
            bot_reddit_tokens=bot_reddit_tokens, global_routine=global_routine)
    return doc


