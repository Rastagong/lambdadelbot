#!/usr/bin/env python3
# coding:utf-8
import asyncio, math, logging
from collections import deque
from datetime import datetime, timedelta
from typing import Callable, cast, Deque, Dict, List, NamedTuple, NoReturn, Optional, Set, Tuple
from ..constants.configuration import config
from ..constants.logging_configuration import X
from ..constants.types import (GameRoutineParams, GameState, GameStatesTuple, makeGameRoutineParams)
from ..connector import Connector
from ..structs.week_time import WeekTime
from ..structs.tokens import RedditTokens
from .fetch import gameCycle

lo = logging.getLogger(config.loggerName)


class CycleRequest(NamedTuple):
    gamecode:str
    opts:GameRoutineParams
    x:X
    lock:Optional[asyncio.Event]

QueueAppender = Callable[[CycleRequest], None]
QueueGetter = Callable[[], Tuple[CycleRequest, ...]]

def cyclesQueueGenerator() -> Tuple[QueueAppender, QueueGetter]:
    cyclesQueue:Deque[CycleRequest] = deque()

    def append(request:CycleRequest) -> None:
        cyclesQueue.append(request)

    def get() -> Tuple[CycleRequest, ...]:
        newRequests = tuple(cyclesQueue)
        cyclesQueue.clear()
        return newRequests

    return (append, get)

def checkScheduleForGame(gamecode:str, start:WeekTime, end:WeekTime, opts:GameRoutineParams, x:X) -> bool:
    """Returns <True> if we're still in the middle of the game session (routine to be launched), <False> otherwise.
    This is determined according to a game schedule (<start><end>). Current time is a WeekTime and always UTC+0.
    The function particularly pays attention to over-the-weekend cases, when the <end> of the game session is actually next week.
    In such a case:
        if the <currentTime> pushed forward by 1 week is before the <end>, we're still DURING_SESSION.
        Otherwise, we're BEFORE, in the first week."""
    nextWeekTime = WeekTime.utcnow(nextWeek=True)
    if opts["force_fetch"]:
        lo.info(f"Force fetch option for {gamecode} is active, starting game routine no matter what.", extra=x)
        return True
    elif end.isNextWeek and nextWeekTime < end: #Over-the-weekend case, and we're still during the session
        lo.info(f"Game session for {gamecode} is still taking place until {end}, launching the game routine", extra=x)
        return True
    else: #Regular week
        currentTime = WeekTime.utcnow()
        if currentTime < start:
            lo.info(f"Game session for {gamecode} has not started yet, will start at {start}", extra=x)
            return False
        elif start <= currentTime and currentTime <= end:
            lo.info(f"Game session for {gamecode} is still taking place until {end}, launching the game routine", extra=x)
            return True
        elif end < currentTime:
            lo.info(f"Game session for {gamecode} has already finished at {end}", extra=x)
            return False
        else:
            raise ValueError(f"Impossible case: {currentTime}, {start}, {end}")


def generateCycleName(r:Connector, startTime:datetime, isGlobal:bool = False, isGame:bool = False, isManual:bool = False) -> str:
    if isGlobal and isGame:
        cycleName = "Global Game Cycle"
    elif isGlobal:
        cycleName = "Global Cycle"
    elif isGame and isManual:
        cycleName = "Manual Game Cycle"
    else:
        raise ValueError(f"Invalid request to generate a cycle name: isGlobal {isGlobal}, isGame {isGame}, isManual {isManual}")
    return startTime.strftime(config.app.cycleNameFormat) + f" - {cycleName} no " + r.generateRandomString(url=False, length=4)


async def cyclesExecutor(r:Connector, getter:QueueGetter) -> NoReturn:
    tasks:List[asyncio.Task] = []
    prefix = "cyclesExecutor_"
    requests:Dict[str, CycleRequest] = {}
    while True:
        for request in getter():
            gamecode, opts, x, lock = request
            task_name = prefix + gamecode
            newTask = asyncio.create_task(gameCycle(r, gamecode, x, **opts), name=task_name)
            requests[task_name] = request
            tasks.append(newTask)
        if not tasks:
            await asyncio.sleep(1)
            continue
        done, pending = await asyncio.wait(tasks, timeout=5.0, return_when=asyncio.FIRST_EXCEPTION)
        for finishedTask in done:
            taskName = finishedTask.get_name()
            gamecode, opts, x, lock = requests.pop(taskName)
            exc = finishedTask.exception()
            if exc is not None:
                lo.critical(f"The game routine for {gamecode} crashed:", exc_info=exc, extra=x)
            if not await r.release_game_lock(gamecode, "worker"):
                lo.warning(f"Could not release the game lock for {gamecode}", extra=x)
            else:
                lo.info(f"Released the game lock for {gamecode}", extra=x)
            if lock is not None:
                lock.set()
        tasks = list(pending)

async def cyclesRequestClient(r:Connector, appender:QueueAppender) -> None:
    async for doc in r.await_manual_cycle_requests():
        gamecode = doc["gamecode"]
        startTime = datetime.utcnow()
        gameCycleName = generateCycleName(r, startTime, isGame=True, isManual=True)
        x = X(gameCycleName=gameCycleName, gamecode=gamecode)
        lo.info(f"Received a manual cycle request for {gamecode}: {doc}", extra=x)
        lo.info(f"Attempting to acquire lock for {gamecode}", extra=x)
        allowed_game_states:GameStatesTuple = (GameState.running, GameState.almostFinished)
        if config.webapp.allow_paused_games_manual_game_cycles:
            allowed_game_states += (GameState.paused,)
        lo.info(f"Allowed game states for manual requests: {allowed_game_states}")
        if not await r.acquire_game_lock(gamecode, "worker", cast(str, x["gameCycleName"]), game_states=allowed_game_states):
            lo.error(f"Could not acquire the game lock for {gamecode}", extra=x)
            await r.answer_manual_cycle_request(gamecode, False)
            continue
        lo.info(f"Lock acquired for {gamecode}, will schedule the game routine launch", extra=x)
        appender(CycleRequest(gamecode, doc["opts"], x, None))
        await r.answer_manual_cycle_request(gamecode, True)
        lo.info(f"A manual game cycle for {gamecode} will be started soon, a positive answer was transmitted to the web process", extra=x)


async def globalCycle(r:Connector, appender:QueueAppender, x:X) -> None:
    """Every hour (depending on Heroku settings), synchronously launches the schedule-checking routine per game.
    Each routine will fetch PMs or not depending on schedule/FORCE_FETCH.
    Launches the game routine only if it is meant to be launched according to schedule and state."""
    opts = makeGameRoutineParams()
    opName = cast(str, x["globalCycleName"])
    lo.info(f"The global routine will attempt to acquire locks for all running games", extra=x)
    await r.acquire_all_game_locks("worker", opName)
    locks:List[asyncio.Event] = []
    for gamecode in await r.get_acquired_games("worker", opName):
        gameCycleName = generateCycleName(r, datetime.utcnow(), isGlobal=True, isGame=True)
        x2 = x(gameCycleName=gameCycleName, gamecode=gamecode)
        lo.info(f"The <{gamecode}> game is running, a lock was acquired for it", extra=x2)
        info = await r.get_schedule(gamecode)
        if not checkScheduleForGame(gamecode, info["start"], info["end"], opts, x2):
            lo.info(f"Attempting to release the game lock for {gamecode}", extra=x2)
            if not await r.release_game_lock(gamecode, "worker"):
                lo.warning(f"Could not release the game lock for {gamecode}", extra=x2)
            continue
        locks.append(asyncio.Event())
        appender(CycleRequest(gamecode, opts, x2, locks[-1]))
    if not locks:
        lo.info(f"No locks were acquired (no running games, or all locks were acquired already): the global routine has finished already", extra=x)
        return
    lo.info(f"All cycle requests sent, now waiting for them to execute...", extra=x)
    await asyncio.gather(*(lock.wait() for lock in locks))
    lo.info(f"All cycle requests executed", extra=x)

def getElapsedSeconds(now:datetime) -> float:
    return now.minute * 60 + now.second + now.microsecond * 1e-6

def getSecondsTillNextSLTime(now:datetime) -> float:
    elapsedSeconds = getElapsedSeconds(now)
    nextSLTimeInSeconds = math.ceil(elapsedSeconds / config.app.globalRoutineIntervalSeconds) * config.app.globalRoutineIntervalSeconds
    return nextSLTimeInSeconds - elapsedSeconds

def getSecondsSincePreviousSLTime(now:datetime) -> float:
    elapsedSeconds = getElapsedSeconds(now)
    previousSLTimeInSeconds = math.floor(elapsedSeconds / config.app.globalRoutineIntervalSeconds) * config.app.globalRoutineIntervalSeconds
    return elapsedSeconds - previousSLTimeInSeconds 

def getNextSLTime(now:datetime) -> datetime:
    remainingSeconds = getSecondsTillNextSLTime(now)
    return now + timedelta(seconds=remainingSeconds)

def getPreviousSLTime(now:datetime) -> datetime:
    spentSeconds = getSecondsSincePreviousSLTime(now)
    return now - timedelta(seconds=spentSeconds)

async def globalCycleScheduler(r:Connector, appender:QueueAppender) -> NoReturn:
    """
    Asynchronous infinite loop which schedules the launch of the global routine at a regular interval.
    Within an hour, the global routine must be launched every GLOBAL_ROUTINE_INTERVAL_IN_SECONDS seconds, at fixed times.
    So if the interval is 15 minutes, the routine will be launched at the 4 following scheduled times per hour:
    :00, :15, :30 and :45 mins. This 15 minutes interval will be taken as example throughout this docstring.
    The rule is that each global routine should execute within its allocated 15-minute timeframe, not later.
    If a routine finishes early, the scheduler will sleep until the next scheduled launch time (SLTime).
    
    In each iteration, there are 4 cases depending on the <startTime> and <endTime> of the last global routine:
      1. The last global routine crashed (<endTime> is None)
         We immediately launch another global routine, regardless of the SLTime.
    
      2. The last global routine ended after (or right at) the SLTime that follows its start time.
         For instance, he routine started around :00, and ended at :15 or later (so it is now :18 for instance).
         This means we missed the :15 scheduled launch time, because the last routine lasted more than the allocated 15 mins.
         We immediately launch another global routine to make up for this missed SLTime.

      3. The last global routine ended earlier than the previous SLTime from now.
         For instance, it is :05, and the last global routine ended at :59 or earlier (say, at :50), and we missed the :00 SLTime.
         This case is rare enough: if the last routine ended early, the scheduler should have slept until the following SLTime (see case 4).
         Case 3 happens only when the last routine ended correctly, but was followed by a blank period where the scheduler wasn't running.
         The typical case is that the whole server was restarted/I redeployed the app (after the end of the routine), and we missed a SLTime.
         As you may expect, we immediately launch another global routine to make up for this missed SLTime.
    
     4. The final case is the default case when we fit in none of the 3 other cases. It's the most usual case.
        In practice, it means the last routine started & ended correctly within its 15 minutes timeframe, and there's still time left.
        For instance, the last routine started at :00, ended at :10, so there are still 5 mins until the :15 launch.
        In such a case, we sleep until the next SLTime (:15), where we will launch the next routine.
    """
    startTime, endTime = await r.get_last_global_routine_exec_times()
    while True:
        now = datetime.utcnow()
        x = X(globalCycleName="scheduler")
        lo.info(f"This is a new iteration of the scheduler", extra=x)
        lo.debug(f"{startTime} Start time of the last routine", extra=x)
        lo.debug(f"{endTime} End time of the last routine", extra=x)
        lo.debug(f"{now} Current time", extra=x)
        if endTime is None:
            lo.info(f"The last global routine was launched at {startTime} but it crashed", extra=x)
            lo.info(f"Launching another global routine right now to catch up on the crashed routine (case 1)", extra=x)
        elif endTime >= (missedSLTimeAfterStart := getNextSLTime(startTime)):
            lo.info(f"The last global routine ended at {endTime}, but we missed the launch scheduled at {missedSLTimeAfterStart}", extra=x)
            lo.info("Launching another global routine right now to catch up on the missed launch (case 2)", extra=x)
        elif endTime < (missedSLTimeBeforeNow := getPreviousSLTime(now)):
            lo.info(f"The last global routine ended at {endTime}, but we missed the launch scheduled at {missedSLTimeBeforeNow}", extra=x)
            lo.info(f"Launching another global routine right now to catch up on the missed launch (case 3)", extra=x)
        else:
            remainingSeconds = getSecondsTillNextSLTime(now)
            lo.info(f"Sleeping for {remainingSeconds/60:0.2f} minutes until the next scheduled launch time (case 4)...", extra=x)
            await asyncio.sleep(remainingSeconds)
        startTime = datetime.utcnow()
        globalCycleName = generateCycleName(r, startTime, isGlobal=True)
        x2 = x(globalCycleName=globalCycleName)
        await r.store_global_routine_start_time(startTime, x)
        lo.info(f"Launching the global routine now (at {startTime})", extra=x2)
        await globalCycle(r, appender, x2)
        endTime = datetime.utcnow()
        lo.info(f"The global routine finished running at {endTime}, individual game cycles will now be run", extra=x2)
        await r.store_global_routine_end_time(endTime, x)

async def worker_entrypoint() -> None:
    r = Connector()
    while not await r.is_config_ready():
        lo.info(f"The database is not ready to be used in the worker process, sleeping 60 seconds before checking again...")
        await asyncio.sleep(60)
    appender, getter = cyclesQueueGenerator()
    await r.remove_all_cycle_requests()
    await r.release_all_game_locks("worker")
    await asyncio.gather(config.mongoHandler.loop(),
                         globalCycleScheduler(r, appender),
                         cyclesRequestClient(r, appender),
                         cyclesExecutor(r, getter) )


