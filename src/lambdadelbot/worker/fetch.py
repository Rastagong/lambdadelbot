# coding: utf-8
import asyncio, logging
from datetime import datetime
from typing import List, Mapping, Sequence, Tuple
from ..constants.configuration import config
from ..constants.logging_configuration import X
from ..constants.types import (BotRole, Direction, GameRoutineParams, GameState, OptStr, PlayerRole, StrDict, StrMapping, StrSet)
from ..connector import Connector
from ..structs.path_keys import PKTCouple
from ..structs.game import Game, get_game
from ..structs.tokens import RedditTokens
from ..database.messages_collection import MessageSDocumentTotal, Msgs
from ..redditmessenger import MsgLikes, MsgLike
from .parlour import Parlour, WikiPublisher

lo = logging.getLogger(config.loggerName)

async def fetch_next_listing(x:X, r:Connector, gamecode:str, location:str, direction:Direction, lastSeenId0:OptStr,
        redditTokens:RedditTokens[PlayerRole], username:str, prevCount:int = 0, depth:int = 0) -> Tuple[OptStr, MsgLikes]:
    """Returns a listing of messages from a <location> in a specific <direction>, and recursively fetches the next listing.
    <lastSeenId> is the reference of the last message ID fetched in that direction.
    When calling on the next listing recursively, the last message ID from the current response is passed.
    In case of error, returns (<False>, <error>). Otherwise, returns (<True>, <messages>)."""
    lo.info(f"Making a request in {location} on behalf of {username}", extra=x)
    lastSeenId, messages = await r.fetchPMs(redditTokens, location, direction, prevCount, lastSeenId=lastSeenId0, x=x)
    count = len(messages)
    lo.info(f"New last seen ID {lastSeenId} in direction {direction} with {count} new messages for {username}:", extra=x)
    if lastSeenId is None and direction == "before" and count > 0: # Final (non-empty) listing of newer PMs
        # We return our listing and pick the most recent PM's ID as the new lastSeenId
        lo.info(f"Case 1a: we received the final listing of newer PMs, which is not empty", extra=x)
        lo.info(f"Returning the ID ({messages[0]['name']}) from the most recent PM of the listing", extra=x)
        return (messages[0]["name"], messages) 
    elif lastSeenId is None and direction == "before": # Final listing of newer PMs, which is also empty
        # We return the (empty) listing and the same last seen ID we used to get this listing
        # During the next fetch cycle, this means we'll start again from this PM
        lo.info(f"Case 1b: we received the final listing of newer PMs, which is also empty", extra=x)
        lo.info(f"Returning the same last seen ID we used to get to this listing ({lastSeenId0})", extra=x)
        return (lastSeenId0, messages)
    elif lastSeenId is None and depth == 0 and count > 0: # Final listing of older PMs at depth 0, not empty
        # We return the lastSeenId from the most recent PM
        # In the next cycle, we'll start again from this PM, but in the 'before' direction
        lo.info(f"Case 1c: final listing older PMs right away at depth 0, and it's not empty", extra=x)
        lo.info(f"Returning the last seen ID of the most recent PM from this single listing ({messages[0]['name']})", extra=x)
        return (messages[0]["name"], messages) 
    elif lastSeenId is None and depth == 0: # Final listing of older PMs at depth 0, and it's empty
        # We cannot save any lastSeenId since we received no PMs at all.
        # In the next cycle, we'll have to look in the 'after' direction again, and hope there are PMs
        lo.info(f"Case 1d: final listing of older PMs at depth 0, which is also empty...", extra=x)
        lo.info(f"...there are no PMs altogether for this user in this location & we never got any in previous cycles either", extra=x)
        lo.info(f"Returning None as lastSeenId since there are no PMs", extra=x)
        return (None, messages) 
    elif lastSeenId is None: # Final listing of older PMs at depth > 0 (listing may be empty or not)
        # No need to update lastSeenId 'cos we only keep the ID of the most recent PM (e.g at depth 0)
        lo.info(f"Case 1e: final listing of older PMs (we don't care whether it's empty or not)", extra=x)
        lo.info(f"Returning None as lastSeenId: for older PMs, we only save the ID received at depth 0 (case 1c)", extra=x)
        return (None, messages) 
    # From here onwards, there are more listings to fetch (in either direction): we get them recursively
    lo.debug(f"About to recursively fetch the listing in direction {direction} with {lastSeenId} as the lastSeenId", extra=x)
    lastSeenId2, messages2 = await fetch_next_listing(x, r, gamecode, location, direction, lastSeenId, redditTokens, username, prevCount=count, depth=depth+1)
    lo.debug(f"Got a new listing in direction {direction} with {lastSeenId2} as the lastSeenId", extra=x)
    # In all cases below, we return the current intermediate listing + all subsequent listing fetched recursively
    if direction == "before": # Intermediate listing of newer PMs
        # The new lastSeenId is the one from the most recent PM from the final subsequent listing 
        lo.info(f"Case 2a: Returning current listing of newer PMs + all subsequent listings of newer PMs", extra=x)
        lo.info(f"Returning {lastSeenId2} as lastSeenId: the ID of the most recent PM from the final listing", extra=x)
        return (lastSeenId2, messages2 + messages)
    elif depth == 0: # First listing of older PMs (e.g depth 0)
        # For older PMs at depth 0, we return the ID of the most recent PM as lastSeenId:
        # In future fetch cycles, we'll start from this PM again (but in the "before" direction)
        lo.info(f"Case 2b: Returning first listing of older PMs + all subsequent listings of older PMs", extra=x)
        lo.info(f"We're at depth 0, so returning the ID of the most recent PM ({messages[0]['name']})", extra=x)
        return (messages[0]["name"], messages+messages2) #<messages> will never be empty since we had to fetch more listings
    else: #Intermediate listing of older PMs, at depth>0
        # We return None as lastSeenId: we won't ever search at depth in that direction again...
        # The only ID we save from older PMs is the one of the most recent PM, received at depth 0 (case 2b)
        lo.info(f"Case 2c: Returning current listing of older PMs + all subsequent listings of older PMs", extra=x)
        lo.info(f"Returning None as lastSeenId: we don't care about the IDs of older PMs received at depth>0", extra=x)
        return (None, messages+messages2)

async def fetch(x:X, r:Connector, gamecode:str, location:str, lastSeenId:OptStr, redditTokens:RedditTokens, username:str) -> Tuple[OptStr, MsgLikes]:
    """Fetches all new messages from <user> from a certain <location> (<inbox> or <sent>).
    When <lastSeenId> is empty, we haven't fetched any PMs at all: we start at the most recent and go towards the oldest in <after> direction.
    But usually, we already know the <lastSeenId>, so we only fetch what has come <before> that ID.
    If no error occurred, returns (<True>,<messages>). Otherwise, returns (<False>,<error>)."""
    direction:Direction = "before" if lastSeenId is not None else "after"
    return await fetch_next_listing(x, r, gamecode, location, direction, lastSeenId, redditTokens, username)

def isMessage(game:Game, like:MsgLike, filtered_msgs:StrSet) -> bool:
    """non-PMs (e.g subreddit comments), system PMs (from Reddit/Automod), and PMs from non-players must be excluded immediately."""
    #TODO: make filtered_msgs something stored in DB, or remove it entirely
    name = like["name"]
    if like["kind"] != "t4": #Non-PMs
        filtered_msgs.add(name)
        return False
    elif like["author"] is None:
        filtered_msgs.add(name)
        return False
    elif like["author"] not in game.players:
        filtered_msgs.add(name)
        return False
    elif like["dest"] not in game.players:
        filtered_msgs.add(name)
        return False
    elif like["first_message_name"] in filtered_msgs:
        filtered_msgs.add(name)
        return False
    return True

def convertMsgLike(gamecode:str, like:MsgLike) -> MessageSDocumentTotal:
    author, dest = like["author"], like["dest"]
    if author is None:#TODO: Change isMessage into a TypeGuard when Python 3.10 or higher is used for this project
        raise ValueError(f"Impossible case, the author must be a string according to isMessage")
    user1, user2 = sorted((author, dest), key=str.lower)
    #root_id:OptStr = like["name"] if like["first_message_name"] is None else like["first_message_name"]
    return MessageSDocumentTotal(gamecode=gamecode, name=like["name"], root_id=like["first_message_name"], author=author, dest=dest,
            user1=user1, user2=user2, subject=like["subject"], body=like["body"], createdOn=datetime.utcfromtimestamp(like["created_utc"]))

def filterAndConvertRealMessages(game:Game, likes:MsgLikes) -> Msgs:
    filtered_msgs:StrSet = set()
    return [convertMsgLike(game.code, like) for like in likes if isMessage(game, like, filtered_msgs)]

async def player_routine(r:Connector, game:Game, username:str, x:X) -> Msgs:
    lo.info(f"Starting {username}'s routine", extra=x)
    lastSeenInbox, lastSeenSent = await r.get_last_seen_ids(game.code, username)
    redditTokens = RedditTokens[PlayerRole].player(None, username)
    inbox_task = asyncio.create_task(fetch(x, r, game.code, "inbox", lastSeenInbox, redditTokens, username))
    sent_task = asyncio.create_task(fetch(x, r, game.code, "sent", lastSeenSent, redditTokens, username))
    task_locations = {inbox_task.get_name():"inbox", sent_task.get_name():"sent"}
    done, pending = await asyncio.wait((inbox_task, sent_task), return_when=asyncio.FIRST_EXCEPTION)
    for task in done:
        exc = task.exception()
        if exc is None: # If both tasks ran successfully, we'll exit the loop with no exception
            continue
        exc_location = task_locations[task.get_name()]
        lo.critical(f"The player sub-routine for {username} ({game.code}) crashed in {exc_location}:", exc_info=exc, extra=x)
        for pending_task in pending:
            pending_location = task_locations[pending_task.get_name()]
            lo.error(f"Cancelling the task for the sub-routine of {username} ({game.code}) in {pending_location} too")
            pending_task.cancel()
        raise exc
    lastSeenInbox, inboxMsgLikes = inbox_task.result()
    lastSeenSent, sentMsgLikes = sent_task.result()
    countAll = len(inboxMsgLikes) + len(sentMsgLikes)
    lo.info(f"Received {countAll} messages for {username} in {game.code} before filtering", extra=x)
    msgs = filterAndConvertRealMessages(game, inboxMsgLikes + sentMsgLikes)
    count = len(msgs)
    lo.info(f"Saving the {count} messages obtained and filtered for {username} in {game.code}", extra=x)
    await r.insert_new_messages(game.code, *msgs)
    lo.info(f"Updating last seen params of {username} for {game.code} to {lastSeenInbox} in inbox and {lastSeenSent} in sent", extra=x)
    await r.update_last_seen_ids(game.code, username, lastSeenInbox, lastSeenSent)
    return msgs

def get_player_routine_tasks(r:Connector, game:Game, x:X) -> Tuple[Sequence["asyncio.tasks.Task[Msgs]"], StrMapping]:
    player_names_by_task_name:StrDict = {}
    tasks:List["asyncio.tasks.Task[Msgs]"] = []
    for player_name in game.players:
        if not game.players[player_name].connected:
            continue
        if not game.players[player_name].active:
            continue
        task = asyncio.create_task(player_routine(r, game, player_name, x))
        tasks.append(task)
        player_names_by_task_name[task.get_name()] = player_name
    return (tasks, player_names_by_task_name)

async def ensure_game_has_self_path_keys(r:Connector, game:Game, x:X) -> Game:
    """Quick and dirty fix for the fact that it is now possible for players to send PMs to themselves.
    There's a permanent fix for future games, where path keys with oneself will be added.
    But for existing games, existing players don't have path keys with themselves.
    I could deal with it in DB but it's tedious and error-prone.
    I'm lazy so I wrote a function that automatically does it for me:
    At the start of a game cycle, this function ensures that all active players do have a path keys with themselves."""
    for player_name in game.players:
        if not game.players[player_name].connected:
            continue
        if not game.players[player_name].active:
            continue

        path_key_index = PKTCouple(player_name, player_name)
        if path_key_index in game.path_keys:
            continue

        lo.info(f"Game <{game.code}> is missing a self-path key for {player_name}; we will add it now", extra=x)
        couple_key = r.generate_path_key_couple(player_name, player_name)
        key_str = couple_key["key"]
        lo.info(f"The key {key_str} was generated for {player_name} in {game.code}; we will now store it in DB", extra=x)
        await r.add_path_keys_for_game(game.code, couple_key)
        lo.info(f"The key {key_str} for {player_name} in {game.code} was successfully stored in DB!", extra=x)

    # I'm super lazy rn; to get an up-to-date Game object, I fetch it entirely from DB again
    # I could manually add the path key object to `game.path_keys`...
    # But fetching it fresh from DB is less error-prone
    game_updated = await get_game(game.code, r)
    return game_updated


async def gameCycle(r:Connector, gamecode:str, x:X, force_fetch:bool = False, wikiwrite:bool = False) -> None:
    """Routine launched when PMs for <game> must be fetched. Simultaneously launches a fetching routine for each player.
    Only new PMs will be fetched by the player subroutines. They will also automatically store the IDs of the last PMs in DB.
    If there are new PMs, a <Parlour> is constructed from all PMs in DB for <game>, and posted to the wiki."""
    lo.info(f"Starting the game routine for <{gamecode}>", extra=x)
    await r.increment_game_update_count(gamecode)
    game = await get_game(gamecode, r)
    game = await ensure_game_has_self_path_keys(r, game, x=x)
    tasks, player_names_by_task_name = get_player_routine_tasks(r, game, x)
    if not tasks:
        lo.info(f"There are no connected and active players, ending the game routine for {gamecode} now", extra=x)
        return
    done, pending = await asyncio.wait(tasks, return_when=asyncio.FIRST_EXCEPTION)
    for task in done:
        exc = task.exception()
        if exc is None:
            continue
        player_name = player_names_by_task_name[task.get_name()]
        lo.critical(f"The player routine of {player_name} in {gamecode} crashed", exc_info=exc, extra=x)
        for pending_task in pending:
            player_name_pending = player_names_by_task_name[pending_task.get_name()]
            lo.info(f"Cancelling the pending task of {player_name_pending} in {gamecode}")
            pending_task.cancel()
        raise exc
    messages = [ msg for task in done for msg in task.result() ]
    count = len(messages)
    lo.info(f"Got all {count} new messages for {game}", extra=x)
    if count <= 0:
        if force_fetch:
            lo.info("Force fetch option is active, so not stopping game routine despite 0 new messages.", extra=x)
        elif game.state == GameState.almostFinished:
            lo.info("Game is almost finished, let's make the final fetch (mostly for wiki public side) despite 0 new messages.", extra=x)
        else:
            lo.info(f"Exiting the game routine for {game} because there are 0 new messages.", extra=x)
            return
    message_docs = await r.get_messages(game.code)
    parlour = Parlour(message_docs, game, x)
    bot_tokens = RedditTokens[BotRole].bot()
    await bot_tokens.ensure(expiration_margin=config.api.tokensExpirationMarginWiki)
    publisher = WikiPublisher(bot_tokens, r, game, wikiwrite, x)
    for (url, contents) in parlour.generate_contents():
        await publisher.publish_page(url, contents)
    lo.info(f"All wiki updates for {game} are done", extra=x)
    if game.state == GameState.almostFinished:
        lo.info(f"{game} is ALMOST FINISHED: this was its final routine, updating its state to FINISHED now.", extra=x)
        await r.set_game_state(game.code, GameState.finished)
    lo.info(f"The game routine for {game} has successfully finished running", extra=x)


