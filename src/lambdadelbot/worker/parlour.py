# -*- coding: utf-8 -*-
from __future__ import annotations
import hashlib, re, sys, logging
from datetime import datetime
from enum import Enum
from typing import (Any, Awaitable, Callable, cast, ClassVar, Dict, Generator, Iterable, Mapping, List, Literal,
        NamedTuple, Optional, Protocol, Sequence, Tuple, TypeVar, Union)
from ..constants.configuration import config
from ..constants.logging_configuration import X
from ..constants.types import (BotRole, Direction, FFalse, GameRoutineParams, GameState,
        makeGameRoutineParams, OptStr, StrList, StrSet, TTrue)
from ..connector import Connector
from ..database.exceptions import DocumentNotFoundError
from ..database.singleton import handle_db, Missing
from ..database.messages_collection import Msgs, MessageSDocumentTotal
from ..database.games_collection import PathKeyDocument
from ..structs.game import Game
from ..structs.tokens import RedditTokens
from ..structs.wiki_url import WikiUrl, UrlPKType, UrlSubpage, UrlSubreddit, UrlPathKey, WikiUrlMenuSubreddit
from ..structs.path_keys import PKTSingle, PKTCouple, PKTDefault, PKTypeDefault, PKTypeSingle, PKTypeCouple

lo = logging.getLogger(config.loggerName)


A = TypeVar("A")

async def handleNetwork(awaitable:Awaitable[A]) -> Union[Tuple[TTrue, A], Tuple[FFalse, Exception]]:
    answerNegative:FFalse = False
    answerPositive:TTrue = True
    try:
        result = await awaitable
    except Exception as error:
        lo.exception(f"The following error occurred while executing a network request in the Parlour")
        return (answerNegative, error)
    else:
        return (answerPositive, result)


Prettifier = Callable[[str], str]
ConversationsDict = Dict[str, "ConversationMutable"]
ConversationsMapping = Mapping[str, "Conversation"]
ConversationsGlobalMapping = Mapping[str, Mapping[str, ConversationsMapping]]
ConversationsGlobalDict = Dict[str, Dict[str, ConversationsDict]]
AuthorsDict = Dict[str, "AuthorMutable"]
AuthorsMapping = Mapping[str, "Author"]
Roomsx2Dict = Dict[Tuple[str, str], "RoomMutable"]
Roomsx2Mapping = Mapping[Tuple[str, str], "Room"]
RoomsMapping = Mapping[str, "Room"]
RoomsDict = Dict[str, "RoomMutable"]
MessagesDict = Dict[str, "Message"]
MessagesMapping = Mapping[str, "Message"]
MessagesList = List["Message"]
MessagesSequence = Sequence["Message"]
TotalPagesDict = Dict[Union[str, Tuple[str, str]], int]
TotalPagesMapping = Mapping[Union[str, Tuple[str, str]], int]

class Message:
    DATE_LINE = "*{0}*. **{1}**:  \n"
    FULL_DATE_FORMAT = "%d %B %Y, %H:%M"
    HOUR_FORMAT = "%H:%M"
    SUBHEADING_REGEX = re.compile(r"(?:\n|\r|^) *(?P<heading>#+)") #Regex to to catch all Markdown headings

    def __init__(self, prettify:Prettifier, doc:MessageSDocumentTotal, previousMessage:Optional[Message]) -> None:
        self.doc, self.prettify, self.createdOn, self.name = doc, prettify, doc["createdOn"], doc["name"]
        self.previousMessage = previousMessage

    def __str__(self) -> str:
        if self.previousMessage is None or not Message.areOnSameDay(self, self.previousMessage): #Root message/changing date: we print the full date
            theStr = Message.DATE_LINE.format(self.createdOn.strftime(Message.FULL_DATE_FORMAT), self.prettify(self.doc["author"]))
        else: #Otherwise, we print the hour only
            theStr = Message.DATE_LINE.format(self.createdOn.strftime(Message.HOUR_FORMAT), self.prettify(self.doc["author"]))
        subheadingsBody = Message.SUBHEADING_REGEX.sub(r"##\g<heading>", self.doc["body"]) #Lowers all Markdown headings by 1 level
        theStr += "{0}  \n\n".format(subheadingsBody)
        return theStr

    @staticmethod
    def areOnSameDay(msg1:Message, msg2:Message) -> bool:
        return msg1.createdOn.day == msg2.createdOn.day and msg1.createdOn.month == msg2.createdOn.month and msg1.createdOn.year == msg2.createdOn.year


class Author(Protocol):
    username:str
    boldened_name:bool
    url:WikiUrl[PKTypeSingle, None, str, str]
    game_title:str
    conversations:ConversationsMapping
    rooms:RoomsMapping

    def header_line(self) -> str: ...
    def menu_line(self, total_pages:int) -> str: ...
    def intro(self) -> str: ...

class AuthorMutable(Author):
    def __init__(self, username:str, boldened_name:bool, prettify:Prettifier, url:WikiUrl[PKTypeSingle, None, str, str], game_title:str) -> None:
        self.username, self.boldened_name, self.prettify, self.url, self.game_title = username, boldened_name, prettify, url, game_title
        self.conversations:ConversationsDict  = {}
        self.rooms:RoomsDict = {}

    def header_line(self) -> str:
        pretty_username = self.prettify(self.username)
        return f"* **{pretty_username}**  \n"

    def menu_line(self, total_pages:int) -> str:
        name_format = "**{0}**" if self.boldened_name else "{0}"
        name_format = name_format.format(self.prettify(self.username))
        pagination_links = generate_pagination_links("menu", 0, total_pages, self.url)
        page1_url = self.url.paginate(1)
        return f"* [{name_format}]({page1_url}){pagination_links}  \n"

    def intro(self) -> str:
        pretty_username = self.prettify(self.username)
        return f"Here are all the conversations in which *{pretty_username}* was involved during **{self.game_title}**, in chronological order.  \n"


class Room(Protocol):
    user1:str
    user2:str
    url:WikiUrl[PKTypeCouple, None, str, str]
    game_title:str
    prettify:Prettifier
    conversations:ConversationsMapping
    def menu_line(self, username_other:str, total_pages:int) -> str: ...
    def intro(self) -> str: ...

class RoomMutable(Room):
    def __init__(self, user1:str, user2:str, prettify:Prettifier, url:WikiUrl[PKTypeCouple, None, str, str], game_title:str) -> None:
        self.user1, self.user2, self.prettify, self.url, self.game_title = user1, user2, prettify, url, game_title
        self.conversations:ConversationsDict = {}

    def menu_line(self, username_other:str, total_pages:int) -> str:
        if username_other != self.user1 and username_other != self.user2:
            raise ValueError(f"Unknown user {username_other} in {self} room")
        pagination_links = generate_pagination_links("menu", 0, total_pages, self.url)
        page1_url = self.url.paginate(1)
        pretty_other = self.prettify(username_other)
        link_desc = f"With {pretty_other}"
        return f" * [{link_desc}]({page1_url}){pagination_links}  \n"

    def intro(self) -> str:
        intro_fmt = "Here are all the conversations in which *{0}* and *{1}* were involved during **{2}**, in chronological order.  \n"
        return intro_fmt.format(self.prettify(self.user1), self.prettify(self.user2), self.game_title)


class Conversation(Protocol):
    title:str
    user1:str
    user2:str
    messagesByName:MessagesMapping
    messages:MessagesSequence
    def __str__(self) -> str: ...

class ConversationMutable(Conversation):
    def __init__(self, title:str, prettify:Prettifier, user1:str, user2:str) -> None:
        self.messagesByName:MessagesDict = {}
        self.messages:MessagesList = []
        self.title, self.prettify = title, prettify
        self.user1, self.user2 = user1, user2

    def addMessage(self, doc:MessageSDocumentTotal) -> None:
        previousMessage = self.messages[-1] if len(self.messages) > 0 else None
        msg = Message(self.prettify, doc, previousMessage)
        self.messagesByName[msg.name] = msg
        self.messages.append(msg)

    def __str__(self) -> str:
        theStr = f"## {self.title}\n"
        user1Str, user2Str = self.prettify(self.user1), self.prettify(self.user2)
        theStr += f"A conversation between {user1Str} and {user2Str}.  \n\n"
        for message in self.messages:
            theStr += str(message)
        theStr += "\n"
        return theStr


class Subpage:
    def __init__(self, intro:str, contents:str = "") -> None:
        self.intro, self.contents = intro, contents
        self.top_pagination, self.bottom_pagination = "\n", ""

    def __str__(self) -> str:
        return self.intro + self.top_pagination + self.contents + self.bottom_pagination

"""class PagePosition:
    top:ClassVar[PagePosition] = cast(PagePosition, "top")
    bottom:ClassVar[PagePosition] = cast(PagePosition, "bottom")
    menu:ClassVar[PagePosition] = cast(PagePosition, "menu")"""
PagePosition = TypeVar("PagePosition", Literal["top"], Literal["bottom"], Literal["menu"])

def generate_pagination_links(page_position:PagePosition, cur_page:int, total_pages:int, base_url:WikiUrl[Any, None, str, Any]) -> str:
        """Generates pagination links base on a <page_position> {top|bottom|menu}, a <cur_page> which will be boldened, a <baseUrl> and the <total_pages>.
        Will always includes the full wiki path to the <subreddit>."""
        if total_pages < 2:
            return ""
        base = "Pages: "
        if page_position == "top":
            prefix, suffix = "", "\n\n"
        elif page_position == "bottom":
            prefix, suffix = "\n\n", ""
        elif page_position == "menu":
            prefix, suffix, base = "(", ")", "more pages: "
        links = ""
        for i in range(1, total_pages+1):
            if cur_page == i: #Current page
                cur_link = f"**{i}** "
            elif i == 1 and page_position == "menu": #First page excluded in menus
                cur_link = ""
            else: #Other pages
                spacing = " " if i < total_pages else ""
                url = base_url.paginate(i)
                cur_link = f"[{i}]({url}){spacing}"
            links += cur_link
        return prefix + base + links + suffix


class Parlour:

    def __init__(self, message_docs:Msgs, game:Game,  x:X) -> None:
        self._conversations:ConversationsGlobalMapping
        self._authors:AuthorsMapping
        self._rooms:Roomsx2Mapping
        self._messages:MessagesMapping
        self._game = game
        self.prettify = self.getPrettifier(game.display_code)
        self.x = x
        self._conversations, self._authors, self._rooms, self._messages = self._structure_data(message_docs, self.prettify, game)

    @staticmethod
    def getPrettifier(display_code:str) -> Prettifier:
        "Closure which returns a functions which removes the game prefix from a username (used only for pretty printing)."
        pattern_string = config.app.prettifyUsernameRegex.format(re.escape(display_code))
        pattern = re.compile(pattern_string, re.IGNORECASE)
        def prettifier(username:str) -> str:
            return pattern.sub("", username)
        return prettifier

    def display_sort(self, usernames:Iterable[str], primary_sort:Union[Literal["custom"], Literal["name"]]) -> StrList:
        def sort_using_custom_value(username:str) -> str: #We could return an integer but using a string makes the type the same as str.lower, which is easier: https://github.com/python/typing/issues/760
            return str(-1 * self._game.players.get(username, self._game.default_player).sort_order)
        primary_sorter:Callable[[str], str]
        secondary_sorter:Callable[[str], str]
        if primary_sort == "custom":
            primary_sorter, secondary_sorter = sort_using_custom_value, str.lower
        else:
            primary_sorter, secondary_sorter = str.lower, sort_using_custom_value
        return sorted(sorted(usernames, key=secondary_sorter), key=primary_sorter)

    @staticmethod
    def _structure_data(docs:Msgs, prettify:Prettifier, game:Game) -> Tuple[ConversationsGlobalMapping, AuthorsMapping, Roomsx2Mapping, MessagesMapping]:
        """Structures PMs in conversations, both those from DB and from new PMs. PMs must already be filtered and sorted. 3 access mechanisms:
        Including existing PMs from DB allows for the insertion of skipped PMs (from temporarily inactive users), keeping the sorting in place.
        1. Universal storage of messages in <self._messages> by key (name), chronologically ordered thanks to <OrderedDict>
        2. Per 2-user storage of convos in <self._conversations[user1][user2]>, followed by the <root_id> of convo.
           Users are always in the same order. Within a 2-user couple, convos are stored chronologically.
        3. Per user storage of convos in <self._conversationsByAuthor>, in chronological order by <root_id>."""
        convs:ConversationsGlobalDict = {}
        authors:AuthorsDict = {}
        rooms:Roomsx2Dict = {}
        messages:MessagesDict = {}
        for doc in docs:
            root_id, name, user1, user2, subject = doc["root_id"], doc["name"], doc["user1"], doc["user2"], doc["subject"]
            if user1 not in convs:
                convs[user1] = {}
            if user2 not in convs[user1]:
                convs[user1][user2] = {}
            if root_id is not None and root_id in convs[user1][user2]: #Reply PM; main convo exists already
                convs[user1][user2][root_id].addMessage(doc)
                continue
            elif root_id is not None: #Conversation offshoot: reply PM that starts its own offshoot
                pass #We cheat and pretend there's no root_id; this will start its own convo
            elif root_id is None: #New main convo
                pass #Start its own convo
            convs[user1][user2][name] = ConversationMutable(subject, prettify, user1, user2)
            convs[user1][user2][name].addMessage(doc)
            messages[name] = convs[user1][user2][name].messagesByName[name]
            if (user1,user2) not in rooms:
                path_key = game.path_keys[PKTCouple(user1, user2)]
                couple_url = WikiUrl.base_url(game.display_code).couple_users(user1, user2, path_key).from_subreddit(game.subreddit_name)
                rooms[user1,user2] = RoomMutable(user1, user2, prettify, couple_url, game.title)
            rooms[user1,user2].conversations[name] = convs[user1][user2][name]
            for (username, username_other) in ((user1,user2), (user2,user1)):
                if username not in authors:
                    path_key = game.path_keys[PKTSingle(username)]
                    single_url = WikiUrl.base_url(game.display_code).single_user(username, path_key).from_subreddit(game.subreddit_name)
                    boldened_name = game.players.get(username, game.default_player).boldened_name
                    authors[username] = AuthorMutable(username, boldened_name, prettify, single_url, game.title)
                authors[username].conversations[name] = convs[user1][user2][name]
                if username_other not in authors[username].rooms:
                    authors[username].rooms[username_other] = rooms[user1, user2]
        return (convs, authors, rooms, messages)

    def _write_subpages(self, thing:Union[Author, Room]) -> Tuple[int, Mapping[WikiUrl[Any, int, str, str], str]]:
        """The procedure which divides a base page into subpages if needed, with pagination links and executes the required writing requests to Reddit.
        Receives a dict of <convos> by <root_id>, an <intro> corresponding the base page and the <urlParams> to construct the base URL.
        First, iters over convos, keeping track of the size of the <cur_page>. When max wiki size is exceeded, further convos are put on a new subpage.
        Each subpage is represented by a <dict> with its <intro>, <contents>, and originally empty pagination links at the <bottom> and <top>.
        Then, iters over subpages. For each one, generates pagination links if there are multiple subpages & launches the writing HTTP request.
        At the end, if there were subpages, we keep track of <total_pages> by the base URL, allowing the generation of pagination links from the menus."""
        intro = thing.intro()
        convos:ConversationsMapping = thing.conversations
        subpages:List[Subpage] = [Subpage(intro)]
        cur_page, totalSize = 0, 0
        for root_id in convos:
            curConvo = str(convos[root_id]) + "\n"
            curSize = sys.getsizeof(curConvo)
            if totalSize + curSize < config.api.approxMaxWikiSize:
                subpages[cur_page].contents += curConvo
                totalSize += curSize
                continue
            #We add a <nextSubpage> with only <intro> and <contents>: pagination links are generated at the end, when we know the <total_pages>.
            subpages.append(Subpage(intro, contents=curConvo))
            cur_page += 1
            totalSize = curSize
        subpages_by_url:Dict[WikiUrl[Any, int, str, str], str] = {}
        total_pages = len(subpages)
        for i,subpage in enumerate(subpages, start=1):
            subpage.top_pagination = generate_pagination_links("top", i, total_pages, thing.url)
            subpage.bottom_pagination = generate_pagination_links("bottom", i, total_pages, thing.url)
            url_subpage = thing.url.paginate(i)
            subpages_by_url[url_subpage] = str(subpage)
        return (total_pages, subpages_by_url)

    def _write_menu(self, total_pages:TotalPagesMapping) -> Tuple[WikiUrlMenuSubreddit, str]:
        """Writes the main menu of the game. The first part are the character routes, the second the couple conversations.
        Iters over <self._conversationsByAuthor> and <self._conversations>, respectively. For the first part, puts the Host first.
        2nd part: indexes convos not only by user1, but also mutually by each user2, so we can find them both ways. Both in a single index.
        Then iters other the index and produces for each user all the couple convos links.
        For each line, constructs the baseUrl, then the optional pagination links, through <_writeMenuPaginationLinksIfNeeded>."""
        contents = "## Character routes  \n"
        for username in self.display_sort(self._authors, primary_sort="custom"):
            contents += self._authors[username].menu_line(total_pages[username])
        contents += "\n\n## Conversations by participants  \n"
        linkDesc, coupleLine, headerLine = "With {}", " * [{0}]({1}){2}  \n", "* **{0}**  \n"
        for username in self.display_sort(self._authors, primary_sort="custom"):
            contents += self._authors[username].header_line()
            for username_other in self.display_sort(self._authors[username].rooms, primary_sort="custom"):
                room = self._authors[username].rooms[username_other]
                contents += room.menu_line(username_other, total_pages[room.user1, room.user2])
        menu_url_parent = WikiUrl.base_url(self._game.display_code).from_subreddit(self._game.subreddit_name)
        menu_url:WikiUrlMenuSubreddit
        if self._game.state >= GameState.almostFinished:
            menu_url = menu_url_parent.public_listing()
        else:
            path_key = self._game.path_keys[PKTDefault]
            menu_url = menu_url_parent.private_listing(path_key)
        lo.info(f"Menu url: {menu_url=}")
        return (menu_url, contents)

    def generate_contents(self) -> Generator[Tuple[WikiUrl[Any, Any, str, Any], str], None, None]:
        total_pages:TotalPagesDict = {}
        for (user1,user2) in self._rooms:
            total_pages[user1, user2], subpages_couple = self._write_subpages(self._rooms[user1,user2])
            for url in subpages_couple:
                yield (url, subpages_couple[url])
        for username in self._authors:
            total_pages[username], subpages_single = self._write_subpages(self._authors[username])
            for url in subpages_single:
                yield (url, subpages_single[url])
        yield self._write_menu(total_pages)
        return


class WikiPublisher:

    @staticmethod
    def _get_reason(gamecode:str, update_count:int) -> str:
        date_string = datetime.utcnow().strftime("%d %b %Y, %H:%M:%S")
        return f"{gamecode} update {update_count}, {date_string}"

    def __init__(self, tokens:RedditTokens[BotRole], r:Connector, game:Game, wikiwrite:bool, x:X) -> None:
        self._tokens, self._r, self._game, self._wikiwrite, self.x = tokens, r, game, wikiwrite, x
        self._reason = self._get_reason(self._game.code, self._game.update_count)
        lo.info(f"The reason for this update is: {self._reason}", extra=x)

    async def _hide_page_and_revision(self, prevID:OptStr, url:WikiUrl[Any, Any, str, Any]) -> None:
        lo.info(f"Setting page and revision to hidden for {url} in {self._game}: former revision no {prevID}", extra=self.x)
        permlevel, listed = await self._r.getWikiPagePermissions(self._tokens, url, x=self.x)
        lo.info(f"Here are the current permissions for {url}: permlevel {permlevel} and listed {listed}", extra=self.x)
        if listed:
            lo.info(f"The {url} page is listed, will unlist it (hide it)", extra=self.x)
            await self._r.setWikiPagePermissions(self._tokens, url, permlevel, False, x=self.x)
            lo.info(f"The {url} page is now unlisted!", extra=self.x)
        else:
            lo.info(f"No need to hide the {url} page, it is already unlisted", extra=self.x)
        lo.info(f"Getting the latest revision ID (ours) for {url}", extra=self.x)
        revision = await self._r.getLatestWikiPageRevision(self._tokens, url, x=self.x)
        if revision is None:
            lo.error(f"An error occurred while fetching the new revision of {url}, cannot ensure the revision is hidden", extra=self.x)
            return
        lo.debug(f"Here is the latest revision ID for {url}: ID {revision['revision_id']}, reason {revision['reason']} (current reason {self._reason})", extra=self.x)
        if revision["revision_hidden"]: #If the latest revision is hidden
            lo.info(f"The latest revision for {url} is already hidden, nothing to do here", extra=self.x)
            return
        lo.info(f"The latest revision for {url} is still visible, it must be hidden", extra=self.x)
        await self._r.toggleWikiRevisionVisibility(self._tokens, url, revision["revision_id"], x=self.x)
        lo.info(f"Latest revision of {url} hidden!", extra=self.x)

    async def _show_page(self, url:WikiUrl[Any, Any, str, Any]) -> None:
        lo.info(f"Game is finished, now showing page {url}.", extra=self.x)
        permlevel, listed = await self._r.getWikiPagePermissions(self._tokens, url, x=self.x)
        lo.info(f"Here are the current permissions for {url}: permlevel {permlevel} and listed {listed}", extra=self.x)
        await self._r.setWikiPagePermissions(self._tokens, url, permlevel, True, x=self.x)

    @staticmethod
    def hash_contents(contents:str) -> str:
        return hashlib.sha256(contents.encode("utf-8", errors="replace")).hexdigest()

    async def _is_update_required(self, url:WikiUrl[Any, Any, str, Any], contents:str) -> Optional[Tuple[OptStr, str]]:
        """"Checks if the <contents> of the page at <url> have changed on the basis of the <pathHashes> of the game, stored by <url>.
        Updates the <pathHashes> with the new one if they have changed. A new page (or on another subreddit, or with another code), would not be found,
        so the hashes would be updated."""
        new_hash = self.hash_contents(contents)
        prev_hash = await handle_db(self._r.get_path_hash(self._game.code, url), DocumentNotFoundError, None)
        if prev_hash is None:
            lo.info(f"[Hash Comp.] No previous page hash for {url}: it is a new page (hash: {new_hash}), an update will be published", extra=self.x)
            return (prev_hash, new_hash)
        elif prev_hash != new_hash:
            lo.info(f"[Hash Comp.] New page contents for {url} ({prev_hash} -> {new_hash}): an update will be published", extra=self.x)
            return (prev_hash, new_hash)
        elif self._wikiwrite:
            lo.info(f"[Hash Comp.] Unchanged page contents for {url} (hash: {prev_hash}), but WIKI_WRITE is ON: an update will be published", extra=self.x)
            return (prev_hash, new_hash)
        else:
            lo.info(f"[Hash Comp.] Unchanged page contents for {url} (hash: {prev_hash}): no update will be published, page up-to-date", extra=self.x)
            return None

    async def _write_update_if_needed(self, url:WikiUrl[Any, Any, str, Any], contents:str, prevID:OptStr) -> Optional[Tuple[OptStr, str]]:
        """Writes a single wiki page, only if needed, hiding the action (page listing and revision) if required."""
        lo.info(f"Writing an update of the {url} page?", extra=self.x)
        result = await self._is_update_required(url, contents)
        if result is None:
            lo.info(f"Will not write an update of {url}, the page hasn't changed.", extra=self.x)
            return None
        prev_hash, new_hash = result
        lo.info(f"YUP! Writing an update of {url}", extra=self.x)
        await self._r.writeWikiPage(self._tokens, url, contents, prevID, self._reason, self.x)
        lo.debug(f"Wrote an update of {url}!", extra=self.x)
        return (prev_hash, new_hash)

    async def publish_page(self, url:WikiUrl[Any, Any, str, Any], contents:str) -> None:
        prev_revision = await self._r.getLatestWikiPageRevision(self._tokens, url, x=self.x)
        prevID = None if prev_revision is None else prev_revision["revision_id"]
        result = await self._write_update_if_needed(url, contents, prevID)
        if self._game.state < GameState.almostFinished: #When game is not finished, need to hide the page
            await self._hide_page_and_revision(prevID, url)
        else: #When game is finished, need to finally show the page in listing
            await self._show_page(url)
        if result is None:
            return
        prev_hash, new_hash = result
        if prev_hash == new_hash:
            #TODO: commenting this part
            return
        elif prev_hash is None: #No previous hash: it's a new page, we perform a page hash push
            await self._r.add_path_hash(self._game.code, url, new_hash)
            lo.info(f"Added page hash of new page {url} in DB: {new_hash}", extra=self.x)
            return
        else: # Otherwise, it's an existing page with a previous hash, so we perform a page hash update
            await self._r.update_path_hash(self._game.code, url, new_hash)
            lo.info(f"Updated page hash of {url} in DB: {new_hash}", extra=self.x)
            return

