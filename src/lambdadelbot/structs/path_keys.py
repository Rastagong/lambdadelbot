# -*- coding: utf-8 -*-
import functools, logging
from collections import defaultdict
from typing import (cast, DefaultDict, Dict, Iterable, Iterator, List, Mapping, NamedTuple, Optional, overload, Tuple, Type,
        TypedDict, TypeVar, TYPE_CHECKING, Union)
from ..constants.configuration import config
from ..constants.types import *
from ..database.games_collection import (PathKeyDocument, PathKeyDefaultDocument, PathKeySingleDocument, PathKeyCoupleDocument,
        PKTypeSingle, PKTypeCouple, PKTypeDefault)
logger = logging.getLogger(config.loggerName)

def PKTSingle(user1:str) -> PKTypeSingle:
    return (user1, None, False)
def PKTCouple(user1:str, user2:str) -> PKTypeCouple:
    return (user1, user2, False)
PKTDefault:PKTypeDefault = (None, None, True)

PKType = TypeVar("PKType", PKTypeSingle, PKTypeCouple, PKTypeDefault)
PKTypeUnion = Union[PKTypeSingle, PKTypeCouple, PKTypeDefault]
PK = TypeVar("PK", PathKeyDefaultDocument, PathKeySingleDocument, PathKeyCoupleDocument)


class PathKeys(Mapping[PKTypeUnion, str]):
    """An instance representing a set of path keys for a game.
    Path keys are the random strings appended to the wiki URLs to keep the archive secret from players.

    There are 3 kinds of path keys:
        - The default key, which is appended to the URL of the archive's listing
        - The single user keys (<user1>), appended to the URL of a single player's page
        - The shared user keys (<user1><user2>), appended to the URLs of 2-player conversation pages

    To ensure consistency in the 2-player URLs, the names of the 2 players involved are sorted,
    with <user1> < <user2>, using str.lower as a base.
    When adding shared keys, <user1><user2> must be supplied with the CORRECT SORTING.

    In terms of storage, keys are stored in the <self._keys> list of docs, for easy MongoDB sorting.
    Each doc is a dict with:
        key: the actual key
        default: <False> or <True> if it's the default key
        user1: always filled with a specific name unless it's the <default> key
        user2: filled with name if it's a shared 2-player key with user1,
               otherwise <False> if it's a single key for user1 alone,
               and obviously still <False> if it's the default key

    For indexing keys, three mappings are available:
        - <self._default_key> points to the default key
        - <self._keysByUsers> is a 2D dict of all user keys by <user1><user2>,
                              with <user1><False> leading to single user keys
        - <self._keysByUsers2> is a 2D dict of 2-player shared keys by <user2><user1>;
                               it's redundant but used for easy user deletion
    These mappings allow direct O(1) access to all user keys.
    
    Usage routine:
        - pathKeys = PathKeys(game)
        - await pathKeys.loadFromDB()
        - pathKeys.addUser(username) // pathKeys.removeUser(username) // pathKeys.addKey(…)
        - pathKeys.saveToDB()
    Path keys of specific users can be retrieved with:
        - pathKeys(user1)
        - pathKeys(user1, user2)
        - pathKeys(default=True)"""

    def __init__(self, gamecode:str, *path_keys:PathKeyDocument) -> None:
        self.gamecode = gamecode
        self._keys:Dict[PKTypeUnion, str] = {}
        for path_key in path_keys:
            pkt, key = cast(PKTypeUnion, tuple(path_key["users"])), path_key["key"]
            if pkt in self._keys:
                existing_key = self._keys[pkt]
                raise ValueError(f"The current PathKeys already has a key ({existing_key}) for {pkt}, cannot add another one ({key})")
            self._keys[pkt] = key

    def __getitem__(self, pkt:PKTypeUnion) -> str:
        """Retrieves a path key corresponding to the specified key parameters."""
        return self._keys[pkt]

    def __len__(self) -> int:
        return len(self._keys)

    def __hash__(self) -> int:
        return hash(self._keys)
        
    def __contains__(self, pkt:object) -> bool:
        return pkt in self._keys

    def __iter__(self) -> Iterator[PKTypeUnion]:
        return iter(self._keys)
