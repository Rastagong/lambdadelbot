# coding:utf-8
from __future__ import annotations
from typing import Mapping, TYPE_CHECKING
from ..constants.configuration import config
from ..constants.types import *
if TYPE_CHECKING:
    from ..connector import Connector
from ..database.games_collection import GameSDocumentTotal, ScheduleDict, players_as_mapping, PlayerStatus
from .path_keys import PathKeys
from .week_time import WeekTime
from .wiki_url import WikiUrl
__all__ = ["Game", "get_game"]

class Game:
    def __init__(self, doc:GameSDocumentTotal) -> None:
        self.code = doc["gamecode"]
        self.display_code = doc["gamecode_display"]
        self.title = doc["title"]
        self.host_username = doc["host_username"]
        self.players = players_as_mapping(doc["players"])
        self.default_player = PlayerStatus(username="", active=False, sort_order=0, boldened_name=False, connected=False)
        self.connected_players = tuple(username for username in self.players if self.players[username].connected)
        self.active_players = tuple(username for username in self.players if self.players[username].active)
        self.subreddit_name = doc["subreddit_name"]
        self.state = GameState(doc["game_state"])
        self.path_keys = PathKeys(self.code, *doc["path_keys"])
        self.update_count = doc["update_count"]
        self.excluded_messages = doc["excluded_messages"]
        self.schedule_start = WeekTime(standardisedDatetime=doc["schedule"]["start"])
        self.schedule_end = WeekTime(standardisedDatetime=doc["schedule"]["end"])
        base_url = WikiUrl.base_url(self.display_code)
        self.public_url = base_url.public_listing().from_subreddit(self.subreddit_name)

    def __str__(self) -> str:
        return f"Game<{self.code}>"

    def __repr__(self) -> str:
        return f"Game('{self.code}')"


async def get_game(gamecode:str, r:Connector) -> Game:
    doc = await r.get_game_document(gamecode)
    game = Game(doc)
    if game.code != gamecode:
        raise ValueError(f"The gamecode used to fetch data from DB ({gamecode}) is different from the obtained gamecode ({game.code})")
    return game

