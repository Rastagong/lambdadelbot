# -*- coding: utf-8 -*-
from __future__ import annotations
import logging
from typing import (cast, Generic, Optional)
from datetime import datetime, timedelta
from typing import (Awaitable, Callable, Generic, Optional, overload, Tuple, Type, TypeVar,
        TYPE_CHECKING, Union)
from ..constants.configuration import config
from ..constants.logging_configuration import *
from ..constants.types import (AccessToken, Bot, BotRole, FFalse, Host, HostRole,
        OptFloat, OptStr, Player, PlayerRole, RefreshToken, TokenType, TokenTypeRefresh, TTrue)
from ..constants.connector_subscriber import ConnectorSubscriber
from ..constants.exceptions import LambdaError, WrongAccountError, NoConfigError
if TYPE_CHECKING:
    from ..database.results import UpdateResultPositive
    from ..database.types import TokensDocument
    from ..web.base_handler import HandlerInfo
from ..database.exceptions import NoMatchError
__all__ = ["OptRedditTokens", "RedditTokens", "UsernameRetriever"]

logger = logging.getLogger(config.loggerName)
RT = TypeVar("RT", str, OptStr)
TokensRequesterCoroutine = Awaitable[Tuple[str, RT, float]]
TokensRequester = Callable[[], TokensRequesterCoroutine[RT]]
UsernameRetrieverCoroutine = Awaitable[str]
UsernameRetriever = Callable[["RedditTokens"], UsernameRetrieverCoroutine]

class TokensNetworkError(LambdaError):
    def __init__(self, action:str, inner_tokens:"InnerRedditTokens", exc:Exception, x:X) -> None:
        self.exc = exc
        self.inner_tokens = inner_tokens
        super().__init__(f"An error occurred while {action} in the {inner_tokens} tokens: {self.exc}", exc_info=self.exc, x=x)


class PlayerTokensStore(ConnectorSubscriber):
    def __init__(self, gamecode:OptStr, username:str) -> None:
        self.username = username
        self.gamecode = gamecode
        self.role = Player

    def get_username_string(self) -> str:
        string = f"player u/{self.username}"
        if self.gamecode is not None:
            string += f" on game {self.gamecode}"
        return string

    async def save(self, access_token:str, refresh_token:str, expires_by:datetime) -> "UpdateResultPositive":
        return await self.r.store_reddit_tokens_for_player(self.gamecode, self.username, access_token, refresh_token, expires_by)

    async def load(self) -> "TokensDocument":
        return await self.r.get_reddit_tokens_for_player(self.gamecode, self.username)

    async def delete(self) -> "UpdateResultPositive":
        return await self.r.erase_reddit_tokens_for_player(self.gamecode, self.username)

class HostTokensStore(ConnectorSubscriber):
    def __init__(self, username:str) -> None:
        self.username = username
        self.role = Host

    def get_username_string(self) -> str:
        return f"host u/{self.username}"

    async def save(self, access_token:str, refresh_token:str, expires_by:datetime) -> "UpdateResultPositive":
        return await self.r.store_reddit_tokens_for_host(self.username, access_token, refresh_token, expires_by)

    async def load(self) -> "TokensDocument":
        return await self.r.get_reddit_tokens_for_host(self.username)

    async def delete(self) -> "UpdateResultPositive":
        return await self.r.erase_reddit_tokens_for_host(self.username)

class BotTokensStore(ConnectorSubscriber):
    def __init__(self) -> None:
        self.role = Bot

    def get_username_string(self) -> str:
        return f"bot u/{config.botUsername}"

    async def save(self, access_token:str, refresh_token:str, expires_by:datetime) -> "UpdateResultPositive":
        return await self.r.store_reddit_tokens_for_bot(access_token, refresh_token, expires_by)

    async def load(self) -> "TokensDocument":
        return await self.r.get_reddit_tokens_for_bot()

TokensStore = TypeVar("TokensStore", PlayerTokensStore, HostTokensStore, BotTokensStore)


class InnerRedditTokens(ConnectorSubscriber, Generic[TokensStore]):

    def __init__(self, store:TokensStore) -> None:
        super().__init__()
        self._store:TokensStore = store
        self.is_set:dict[TokenType, bool] = {"access_token":False, "refresh_token":False}
        self._expires_by:datetime
        self._refresh_token:str
        self.access_token:str
        self.title = self._store.get_username_string()
        self.expiration_margin:float = config.api.tokensExpirationMargin

    def __str__(self) -> str:
        return f"InnerRedditTokens<{self.title}>"

    def _ensure_is_set(self, token_type:Optional[TokenType] = None) -> None:
        """Checks that the tokens were initialised (either through API retrieval, or from DB)
        before using them (or even revoking them) in any critical function.
        Otherwise, raises an exception.
        The token_type allows filtering: only the specified token type should be set."""
        if token_type is None:
            if False in (self.is_set["access_token"], self.is_set["refresh_token"]):
                raise ValueError(f"The {self} tokens were not fully set: {self.is_set}")
            return None
        elif not self.is_set[token_type]:
            raise ValueError(f"The {self} {token_type} tokens was not set: {self.is_set}")
        return None

    def _ensure_is_set_handle(self, token_type:Optional[TokenType] = None) -> bool:
        try:
            self._ensure_is_set(token_type=token_type)
        except ValueError:
            return False
        else:
            return True

    def is_valid(self) -> bool:
        return self._ensure_is_set_handle() and not self._has_expired()
    
    def _has_expired(self) -> bool:
        """Returns <True> if the current <access_token> has not expired yet (nor is about to in less than the expiration margin)."""
        return datetime.utcnow() + timedelta(seconds=self.expiration_margin) >= self._expires_by

    def _save_tokens(self, access_token:str, refresh_token:str, expires_in:float) -> None:
        """Stores the new <access_token> internally and builds the <expires_by> datetime object using <expires_in> (provided in seconds)."""
        self.access_token, self._refresh_token = access_token, refresh_token
        self._expires_by = datetime.utcnow() + timedelta(seconds=expires_in)
        self.is_set["access_token"] = True
        self.is_set["refresh_token"] = True

    async def _request(self, 
            RTType:Type[RT], requester:TokensRequester[RT]) -> Tuple [str, RT, float]:
        """Executes an HTTP client request to retrieve or refresh tokens.
        The goal of this method is to ensure that:
            - The number of seconds till expiration is in the future, not in the past (takes into account the execution time of the request too)
        This method is used both by the retrieve and the refresh methods."""
        request_launch_time = datetime.utcnow()
        access_token, refresh_token, expires_in = await requester()
        request_duration = datetime.utcnow() - request_launch_time
        expires_in_adjusted = expires_in - request_duration.total_seconds()
        if expires_in_adjusted <= 0.0:
            raise ValueError(f"The expiration delay adjusted for the request duration ({expires_in_adjusted}) cannot be negative")
        return (access_token, refresh_token, expires_in_adjusted)

    async def retrieve(self, handler:"HandlerInfo", code:str, ox:OX = None) -> None:
        """Exchanges a <code> for some <access_token><refresh_token><expires_in>."""
        x = ensure_x(ox)
        try:
            def requester() -> Awaitable[Tuple[str, str, float]]:
                login_finish_url = handler.get_login_finish_url()
                return self.r.retrieve_reddit_tokens(login_finish_url, code, x=ox)
            access_token, refresh_token, expires_in = await self._request(str, requester)
        except Exception as error:
            raise TokensNetworkError(f"retrieving an access token in exchance for the {code} code",
                                     self, error, x=x) from error
        else:
            self._save_tokens(access_token, refresh_token, expires_in)

    async def refresh(self, ox:OX = None) -> None:
        """Refreshes an <access_token> and <refresh_token>. 
        Note: this method does not automatically store the new tokens in DB, <db_save> must manually be called afterwards."""
        x = ensure_x(ox)
        try:
            self._ensure_is_set()
            orig_refresh_token = self._refresh_token
            def requester() -> Awaitable[Tuple[str, OptStr, float]]:
                return self.r.refreshRedditTokens(orig_refresh_token, x=ox)
            access_token, refresh_token, expires_in = await self._request(type(None), requester)
        except Exception as error:
            raise TokensNetworkError("refreshing tokens", self, error, x=x) from error
        else:
            if refresh_token is None:
                refresh_token = orig_refresh_token
            self._save_tokens(access_token, refresh_token, expires_in)

    async def revoke(self, token_type:TokenType, ox:OX = None) -> None:
        x = ensure_x(ox)
        try:
            self._ensure_is_set(token_type)
            logger.info(f"Revoking the {self} tokens", extra=x)
            token = self._refresh_token if token_type == "refresh_token" else self.access_token
            await self.r.revokeRedditTokens(token, token_type, x=x)
        except Exception as error:
            self.is_set[token_type] = False
            raise TokensNetworkError("revoking the {token_type} token ({token)})",
                                     self, error, x=x) from error
        else:
            self.is_set[token_type] = False

    async def db_save(self, ox:OX = None) -> "UpdateResultPositive":
        x = ensure_x(ox)
        self._ensure_is_set()
        return await self._store.save(self.access_token, self._refresh_token, self._expires_by)

    async def db_load(self, ox:OX = None) -> None:
        x = ensure_x(ox)
        doc = await self._store.load()
        self.access_token, self._refresh_token, self._expires_by = doc["access_token"], doc["refresh_token"], doc["expires_by"]
        self.is_set["access_token"] = True
        self.is_set["refresh_token"] = True

    async def db_delete(self:InnerPlayerHost, ox:OX = None) -> "UpdateResultPositive":
        x = ensure_x(ox)
        return await self._store.delete()


InnerPlayerHost = Union[InnerRedditTokens[PlayerTokensStore], InnerRedditTokens[HostTokensStore]]
InnerTokens = Union[InnerRedditTokens[PlayerTokensStore], InnerRedditTokens[HostTokensStore], InnerRedditTokens[BotTokensStore]]
R = TypeVar("R", PlayerRole, HostRole, BotRole)


class RedditTokens(Generic[R]):

    def __init__(self, role:R, tokens:InnerTokens) -> None:
        self.role:R = role
        self.inner_tokens = tokens
        self._initializing = False

    def __str__(self) -> str:
        return f"RedditTokens<{self.inner_tokens.title}>"

    @classmethod
    def player(cls:Type[RedditTokens[PlayerRole]], gamecode:OptStr, username:str) -> RedditTokens[PlayerRole]:
        inner_tokens = InnerRedditTokens(PlayerTokensStore(gamecode, username))
        return cls(Player, inner_tokens)

    @classmethod
    def host(cls:Type[RedditTokens[HostRole]], username:str) -> RedditTokens[HostRole]:
        inner_tokens = InnerRedditTokens(HostTokensStore(username))
        return cls(Host, inner_tokens)

    @classmethod
    def bot(cls:Type[RedditTokens[BotRole]]) -> RedditTokens[BotRole]:
        inner_tokens = InnerRedditTokens(BotTokensStore())
        return cls(Bot, inner_tokens)

    def is_valid(self) -> bool:
        return self.inner_tokens.is_valid()

    @property
    def access_token(self) -> str:
        return self.inner_tokens.access_token

    async def ensure(self, ox:OX = None, expiration_margin:OptFloat = None) -> None:
        """Ensures the <access_token> is valid (even with the expiration margin). If it isn't, the token is refreshed and re-saved to DB.
        If <expiration_margin> is supplied, the expiration margin will be updated.
        If the token couldn't be refreshed, returns <False> (<True> otherwise). The error is stored in <connError>."""
        x = ensure_x(ox)
        logger.debug(f"Ensuring validity for the {self} tokens.", extra=x)
        if expiration_margin is not None:
            self.inner_tokens.expiration_margin = expiration_margin
        if not self.inner_tokens._ensure_is_set_handle():
            logger.debug(f"The {self} tokens must be loaded from DB", extra=x)
            await self.inner_tokens.db_load(x)
            logger.debug(f"The {self} tokens were loaded from DB", extra=x)
        if self.inner_tokens.is_valid():
            logger.info(f"The {self} tokens are already valid, nothing to do.", extra=x)
            return
        if self._initializing:
            raise LambdaError(f"The {self} tokens are still initializing but already invalid/still invalid so far: their validity cannot be ensured until initializing ends", x=x)
        logger.debug(f"The {self} tokens are no longer valid, they need to be refreshed.", extra=x)
        logger.info(f"Attempting to refresh the {self} tokens...", extra=x)
        await self.inner_tokens.refresh(x)
        logger.info(f"The {self} tokens were refreshed, they are now valid. Attempting to save them...", extra=x)
        await self.inner_tokens.db_save(x)
        logger.info(f"The refreshed {self} tokens were saved to DB", extra=x)

    async def initialize(self, handler:"HandlerInfo", code:str, expected_username:str, 
            username_retriever:UsernameRetriever, ox:OX = None) -> None:
        x = ensure_x(ox)
        try:
            self._initializing = True
            await self.inner_tokens.retrieve(handler, code)
        except Exception as error:
            raise error
        else:
            try:
                actual_username = await username_retriever(self)
                logger.debug(f"Actual username: {actual_username}")
                logger.debug(f"Expected username: {expected_username}")
                if actual_username.lower() != expected_username.lower():
                    raise WrongAccountError(actual_username, expected_username, x=x)
                logger.debug(f"Verified that the tokens belong to {actual_username} as expected")
            except Exception as error:
                await self.inner_tokens.revoke("access_token", ox=x)
                await self.inner_tokens.revoke("refresh_token", ox=x)
                raise error
            else:
                try:
                    await self.inner_tokens.db_save(x)
                except NoMatchError as err:
                    if self.role != Bot:
                        raise err
                    raise NoConfigError(f"The config has not been initialized yet in DB", x=x) from err
        finally:
            self._initializing = False

    async def void(self, ox:OX = None) -> None:
        x = ensure_x(ox)
        if not self.inner_tokens._ensure_is_set_handle():
            await self.inner_tokens.db_load(x)
        await self.inner_tokens.revoke("access_token", ox=x)
        await self.inner_tokens.revoke("refresh_token", ox=x)


OptRedditTokens = Optional[RedditTokens]

config.subscribe_to_connector(InnerRedditTokens)
config.subscribe_to_connector(PlayerTokensStore)
config.subscribe_to_connector(HostTokensStore)
config.subscribe_to_connector(BotTokensStore)
