# coding: utf-8 
from __future__ import annotations
from datetime import datetime, timedelta
from typing import overload, Optional, Tuple, Union
from ..constants.configuration import config
from ..constants.types import *
import logging
__all__ = ["WeekTime", "WTTupleHour", "WTTupleMinute"]
lo = logging.getLogger(config.loggerName)

WTTupleHour = Tuple[int, int]
WTTupleMinute = Tuple[int, int, int]
NEXT_WEEK_START = datetime.strptime("1 2 1970", "%w %U %Y").day #Day number (0-30) of the first day of 1970's next week (eg 12, for Mon Jan 12 '70)

class WeekTime(datetime):
    """Datetime sublass to represent a specific time within the week (From Monday 0:01 to Friday 23:59). All game times are represented as WeekTimes.
    The main purpose is to facilitate in-week comparisions, to check if the current WeekTime is during a game session bound by two start/end WeekTimes.
    This works by extracting <weekDay><hour><minute> from any datetime and standardising them to a datetime always set in the same exact week of 1970.
    Note: There are actually two weeks used in 1970, the 2nd and the 3rd one, to allow a game session to occur over a weekend."""
    _isNextWeek:bool

    @staticmethod
    def _createStandardisedDatetime(weekDay:int, hour:int, minute:int, nextWeek:bool) -> datetime:
        paddedHour, paddedMinute = str(hour).zfill(2), str(minute).zfill(2) #%H/%M must be padded because of 24-hour formatting
        adjustedWeekDay = weekDay if weekDay != 7 else 0 #With %w, Sunday is 0
        adjustedWeek = 2 if nextWeek is True else 1 #Week number (%U). Base week is 1, the 2nd week of 1970.
        if weekDay == 7:
            adjustedWeek += 1 #Week starts on Sunday with %U, so we need to offset it by 1 to get the Sunday at the end of the specified week
        return datetime.strptime(f"{adjustedWeekDay} {adjustedWeek} {paddedHour}:{paddedMinute} 1970", "%w %U %H:%M %Y")

    def __new__(cls, weekDay:int = 1, hour:int = 0, minute:int = 1, nextWeek:bool = False,
            standardisedDatetime:Optional[datetime] = None) -> WeekTime:
        """Initialises a <WeekTime> by standardising all weekly schedule datetimes to the same 1970 week, thus facilitating comparisons.
        By default, creates a standardised 1970 datetime from <weekDay> (0-7), <hour> (0-23) and <minute> (0-59).
        If <nextWeek>, the 1970 datetime will be set a week later, to allow for calculations to occur over the weekend and beyond. 
        But if a <standardisedDatetime> is provided, it is directly used to instantiate the WeekTime."""
        d = cls._createStandardisedDatetime(weekDay, hour, minute, nextWeek) if standardisedDatetime is None else standardisedDatetime
        self = datetime.__new__(cls, d.year, d.month, d.day, d.hour, d.minute, d.second)
        self._isNextWeek = self.day >= NEXT_WEEK_START
        return self

    def __str__(self) -> str:
        """Simple representation of the WeekTime. The day comparison (12 January 1970) is used to determine if we're on the current or next week."""
        return self.strftime("<WeekTime: on %A at %H:%M>" if self.day < NEXT_WEEK_START else "<WeekTime: on next %A at %H:%M>")

    def __add__(self, other:object) -> WeekTime:
        """Used when adding a <timedelta> to a <WeekTime>. We must initialise from the already-standardised result of the datetime addition."""
        if not isinstance(other, timedelta):
            return NotImplemented
        standardised_datetime = datetime(self.year, self.month, self.day, self.hour, self.minute, self.second) + other
        return WeekTime.__new__(WeekTime, standardisedDatetime=standardised_datetime)

    @classmethod
    def utcnow(cls, nextWeek:bool = False) -> WeekTime:
        """Like datetime.now(), returns the UTC+0 current time (but as a <WeekTime>).
        If <nextWeek> is True, sets the current time in the following week, to allow for comparisons over the weekend."""
        return cls.fromdatetime(datetime.utcnow(), nextWeek=nextWeek)

    @overload
    def asdatetuple(self, keepMinute:TTrue) -> WTTupleMinute: ...

    @overload
    def asdatetuple(self, keepMinute:FFalse = False) -> WTTupleHour: ...

    def asdatetuple(self, keepMinute:bool = False) -> Union[WTTupleMinute, WTTupleHour]:
        return WeekTime.datetuple(self, keepMinute=True) if keepMinute else WeekTime.datetuple(self)

    @overload
    @staticmethod
    def datetuple(datetimeObject:datetime, keepMinute:TTrue) -> WTTupleMinute: ...

    @overload
    @staticmethod
    def datetuple(datetimeObject:datetime, keepMinute:FFalse = False) -> WTTupleHour: ...

    @staticmethod
    def datetuple(datetimeObject:datetime, keepMinute:bool = False) -> Union[WTTupleMinute, WTTupleHour]:
        """Extracts a <weekDay><hour><minute> tuple from a regular <datetime>."""
        weekDay, hour, minute = datetimeObject.isoweekday(), datetimeObject.hour, datetimeObject.minute
        return (weekDay, hour, minute) if keepMinute else (weekDay, hour)

    @classmethod
    def fromdatetime(cls, datetimeObject:datetime, nextWeek:bool = False) -> WeekTime:
        """Returns a <WeekTime> from a non-standardised <datetime>, keeping its <weekDay><hour><minute>.
        If <nextWeek> is True, sets the <WeekTime> in the following week, to allow for comparisons over the weekend."""
        return cls(*cls.datetuple(datetimeObject, keepMinute=True), nextWeek=nextWeek)

    @staticmethod
    def pushByAWeek(date:WeekTime) -> WeekTime:
        return date + timedelta(weeks=1)

    @classmethod
    def orderRelativeDates(cls, start:WeekTime, end:WeekTime) -> Tuple[WeekTime, WeekTime]:
        """Orders the <start> and <end> WeekTimes correctly. In practice, a game session may play itself over a weekend.
        When standardising to the same 1970 week, this sets the end date earlier than the start date (e.g on MON 5 Jan, before FRI 9 Jan).
        So if <end> is before <start>, we need to push back <end> by a week, into the 3rd week of 1970, to restablish the right order."""
        if end < start:
            end = cls.pushByAWeek(end)
        return start, end

    @classmethod
    def generateSchedule(cls, startDay:int, startTime:int, endDay:int, endTime:int) -> Tuple[WeekTime, WeekTime]:
        """Generates two WeekTimes designating the <start> and <end> of a game session according to schedule, and orders them correctly."""
        start, end = cls(startDay, startTime), cls(endDay, endTime)
        return cls.orderRelativeDates(start, end)

    @staticmethod
    def weekDayNumberToLiteral(n:int) -> str:
        """Converts a week day number in int %w format (0->6) into a str literal of the week day thanks to %A format (Sunday->Saturday)"""
        adjustedN = n if n != 7 else 0
        return datetime.strptime(f"{adjustedN} 1970 1", "%w %Y %U").strftime("%A")

    @property
    def isNextWeek(self) -> bool:
        return self._isNextWeek
