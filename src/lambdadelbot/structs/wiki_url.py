#coding:utf-8
from __future__ import annotations
from typing import (Any, Generic, List, Literal, Union, Tuple, TypeVar)
from ..constants import *
from ..database.games_collection import (PathKeyDocument, PathKeyDocumentCondition, PathKeySingleDocument,
        PathKeyCoupleDocument, PathKeyDefaultDocument, PKTypeSingle, PKTypeCouple, PKTypeDefault)
from .path_keys import (PKTSingle, PKTCouple, PKTDefault)


UrlPKType = TypeVar("UrlPKType", PKTypeSingle, PKTypeCouple, PKTypeDefault, None)
UrlSubpage = TypeVar("UrlSubpage", int, None)
UrlSubreddit = TypeVar("UrlSubreddit", str, None)
UrlPathKey = TypeVar("UrlPathKey", str, None)


class WikiUrl(Generic[UrlPKType, UrlSubpage, UrlSubreddit, UrlPathKey]):

    def __init__(self, gamecode:str, pkt:UrlPKType, subpage:UrlSubpage, subreddit:UrlSubreddit,
            path_key:UrlPathKey) -> None:
        self.gamecode:str = gamecode
        self.pkt:UrlPKType = pkt 
        self.subpage:UrlSubpage = subpage
        self.subreddit:UrlSubreddit = subreddit
        self.path_key:UrlPathKey = path_key
        self.url:str = gamecode
        if self.pkt is None:
            return
        user1, user2, main_menu = self.pkt
        if main_menu is True and self.path_key is None:
            pass
        elif main_menu is True:
            self.url = self.transform_url_private_listing(self.url, self.path_key)
        elif user2 is not None and self.path_key is None:
            raise ValueError
        elif user2 is not None:
            self.url = self.transform_url_couple_users(self.url, user1, user2, self.path_key)
        elif self.path_key is None:
            raise ValueError
        else:
            self.url = self.transform_url_single_user(self.url, user1, self.path_key)
        if self.subpage is not None:
            self.url = self.transform_url_subpage(self.url, self.subpage)
        if self.subreddit is not None:
            self.url = self.transform_url_subreddit(self.url, self.subreddit)

    def __str__(self) -> str:
        return self.url

    def __repr__(self) -> str:
        return f"WikiUrl({repr(self.gamecode)}, {repr(self.pkt)}, {self.subpage}, {repr(self.subreddit)}, {repr(self.path_key)})"

    @staticmethod
    def transform_url_private_listing(url:str, path_key:str) -> str:
        return f"{url}/listing_{path_key}"

    @staticmethod
    def transform_url_couple_users(url:str, user1:str, user2:str, path_key:str) -> str:
        return f"{url}/{user1}_{user2}_{path_key}"

    @staticmethod
    def transform_url_single_user(url:str, user1:str, path_key:str) -> str:
        return f"{url}/{user1}_{path_key}"

    @staticmethod
    def transform_url_subpage(url:str, subpage:int) -> str:
        return f"{url}/{subpage}"

    @staticmethod
    def transform_url_subreddit(url:str, subreddit:str) -> str:
        return f"/r/{subreddit}/wiki/{url}"

    @classmethod
    def base_url(cls, gamecode:str) -> WikiUrl[None, None, None, None]:
        return WikiUrl[None, None, None, None](gamecode, None, None, None, None)

    def paginate(self, subpage:int) -> WikiUrl[UrlPKType, int, UrlSubreddit, UrlPathKey]:
        return WikiUrl[UrlPKType, int, UrlSubreddit, UrlPathKey](self.gamecode, self.pkt, subpage, self.subreddit, self.path_key)

    def from_subreddit(self, subreddit:str) -> WikiUrl[UrlPKType, UrlSubpage, str, UrlPathKey]:
        if self.subreddit is not None:
            raise ValueError(f"This wiki URL already has a subreddit")
        return WikiUrl[UrlPKType, UrlSubpage, str, UrlPathKey](self.gamecode, self.pkt, self.subpage, subreddit, self.path_key)

    def from_no_subreddit(self) -> WikiUrl[UrlPKType, UrlSubpage, None, UrlPathKey]:
        return WikiUrl[UrlPKType, UrlSubpage, None, UrlPathKey](self.gamecode, self.pkt, self.subpage, None, self.path_key)

    def single_user(self, username:str, path_key:str) -> WikiUrl[PKTypeSingle, UrlSubpage, UrlSubreddit, str]:
        return WikiUrl[PKTypeSingle, UrlSubpage, UrlSubreddit, str](self.gamecode, PKTSingle(username), self.subpage, self.subreddit, path_key)

    def couple_users(self, user1:str, user2:str, path_key:str) -> WikiUrl[PKTypeCouple, UrlSubpage, UrlSubreddit, str]:
        return WikiUrl[PKTypeCouple, UrlSubpage, UrlSubreddit, str](self.gamecode, PKTCouple(user1, user2), self.subpage, self.subreddit, path_key)

    def private_listing(self, path_key:str) -> WikiUrl[PKTypeDefault, UrlSubpage, UrlSubreddit, str]:
        if self.subpage is not None:
            raise ValueError(f"A listing url ({self}) cannot have a subpage")
        return WikiUrl[PKTypeDefault, UrlSubpage, UrlSubreddit, str](self.gamecode, PKTDefault, self.subpage, self.subreddit, path_key)

    def public_listing(self) -> WikiUrl[PKTypeDefault, UrlSubpage, UrlSubreddit, None]:
        if self.subpage is not None:
            raise ValueError(f"A listing url ({self}) cannot have a subpage")
        return WikiUrl[PKTypeDefault, UrlSubpage, UrlSubreddit, None](self.gamecode, PKTDefault, self.subpage, self.subreddit, None)


WikiUrlSubreddit = WikiUrl[Any, Any, str, Any]
WikiUrlMenuSubreddit = Union[ WikiUrl[PKTypeDefault, None, str, str], WikiUrl[PKTypeDefault, None, str, None] ]
WikiUrlMenu = Union[ WikiUrl[PKTypeDefault, None, None, str], WikiUrl[PKTypeDefault, None, None, None] ]
WikiUrlMenuPublic = WikiUrl[PKTypeDefault, None, UrlSubreddit, None]
WikiUrlMenuPrivate = WikiUrl[PKTypeDefault, None, UrlSubreddit, str]


