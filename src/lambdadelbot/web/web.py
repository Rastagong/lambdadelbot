# coding:utf-8
import asyncio, logging
from tornado.web import Application, StaticFileHandler
from ..constants.configuration import config
from ..constants.types import StrMapping
from .controller import Controller
from .handlers import BaseHandler

logger = logging.getLogger(config.loggerName)

def get_settings() -> StrMapping:
    return {"debug":not config.productionMode,
            "cookie_secret":config.webapp.cookieSecret,
            "xsrf_cookies": True,
            "static_path":"lambdadelbot/static",
            "template_path":"lambdadelbot"}

async def web_entrypoint() -> None:
    BaseHandler.controller = Controller()
    await BaseHandler.controller.on_app_start()
    settings = get_settings()
    logger.info(f"Settings: {settings}")
    app = Application(handlers=BaseHandler.routes, **settings)
    app.listen(config.network.containerPort)
    logger.info("Web application launched")
    await asyncio.gather(BaseHandler.controller.clear_expired_auth_requests(), config.mongoHandler.loop())
