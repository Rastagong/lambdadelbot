#coding:utf-8
from __future__ import annotations
import asyncio, json, logging
from abc import ABC, abstractmethod
from tornado.web import RequestHandler, StaticFileHandler
from tornado.routing import PathMatches, Rule, _RuleList
from typing import (Any, Awaitable, Callable, cast, ClassVar, Coroutine, Generic, List, Literal, Mapping,
        Optional, Protocol, Sequence, Tuple, Type, TypeVar, TYPE_CHECKING, Union)
from ..constants.configuration import config
from ..constants.exceptions import raise_logic_error
from ..constants.types import (GameState, GameStateValue, HTTPMethod, OptBool,
        OptGameState, OptInt, OptStr, StrDict, StrMapping)
from ..database.games_collection import GameSDocument
from ..structs.game import Game
from ..structs.path_keys import PKTypeDefault 
from ..structs.tokens import RedditTokens, WrongAccountError, UsernameRetriever
from ..structs.week_time import WeekTime
from ..structs.wiki_url import WikiUrl, UrlSubreddit, WikiUrlMenuPublic, WikiUrlMenuPrivate
if TYPE_CHECKING:
    from .controller import Controller
__all__ = ["Action",
        "BaseRender", "BaseHandler",
        "ContentPageABCHandler", "ContentPageName",
        "EmptyPage",
        "GameAdminPage", "GameHandler", "GameIntroductionPage",
        "HandlerInfo", "HomePage",
        "modal_success", "modal_error", "ModalError", "ModalSuccess", "ModalText",
        "NoArgsHandler",
        "Page",
        "PContent", "PEmpty", "PEmptyError",
        "PGAdmin", "PGAdmin_EmptyError", "PGAdminSuccess",
        "PGAdminError", "PGAdminSuccessError_EmptyError", "PGAdminSuccess_HomeError_EmptyError", "PGAdminSuccess_Home_EmptyError",
        "PGame", "PGame_EmptyError",
        "PGIntro", "PGIntro_EmptyError",
        "PHome", "PHomeNone", "PHomeError", "PHomeSuccess", "PHomeSuccessError", "PHomeError_Redir",
        "RedirectAction", "RenderAction",
        "UserHandler",
        ]

logger = logging.getLogger(config.loggerName)

EmptyPage = Tuple[Literal["empty_page"], Callable[[], StrDict]]
HomePage = Tuple[Literal["home"], Callable[[], StrDict]]
ContentPageName = Union[Literal["about"], Literal["changelog"]]
CONTENT_PAGE_NAMES:Tuple[ContentPageName, ...] = ("about", "changelog")
ContentPage = Tuple[ContentPageName, Callable[[], StrDict]]
GameIntroductionPage = Tuple[Literal["game_introduction"], Callable[[], StrDict]]
GameAdminPage = Tuple[Literal["game_admin"], Callable[[], StrDict]]
Page = TypeVar("Page", EmptyPage, HomePage, ContentPage, GameIntroductionPage, GameAdminPage)

ModalSuccess = Tuple[Literal["success"], str]
ModalError = Tuple[Literal["error"], str]
ModalText = TypeVar("ModalText", ModalSuccess, ModalError, None)
ModalTextUnion = Union[ModalSuccess, ModalError, None]

def modal_success(success_string:str) -> ModalSuccess:
    return ("success", success_string)

def modal_error(error_string:str) -> ModalError:
    return ("error", error_string)

class RenderAction(Generic[Page, ModalText]):
    tag:ClassVar[Literal["render"]] = "render"

    def __init__(self, page:Page, modal:ModalText) -> None:
        self.page:Page = page
        self.modal:ModalText = modal

    def get_values(self) -> StrMapping:
        page_name = self.page[0]
        namespace = self.page[1]()
        namespace["error_string"] = None
        namespace["success_string"] = None
        namespace["page"] = page_name
        if self.modal is None:
            return namespace
        elif self.modal[0] == "success":
            namespace["success_string"] = self.modal[1]
        elif self.modal[0] == "error":
            namespace["error_string"] = self.modal[1]
        return namespace

    @classmethod
    def template(cls) -> RenderAction[EmptyPage, None]:
        def render() -> StrDict: return {"title":"Lambdadelbot"}
        page:EmptyPage = ("empty_page", render)
        return RenderAction[EmptyPage, None](page, None)

    def homepage(self, home_text:str, games:Sequence[Game], host_usernames:Sequence[str],
            is_any_host:bool, is_admin:bool, is_config_ready:bool) -> RenderAction[HomePage, ModalText]:
        def render() -> StrDict:
            return {"home_text":home_text, "games":games, 
                    "host_usernames":host_usernames,
                    "title":"Homepage", "is_any_host":is_any_host,
                    "is_admin":is_admin, "is_config_ready":is_config_ready}
        page:HomePage = ("home", render)
        return RenderAction[HomePage, ModalText](page, self.modal)

    def content_page(self, page_name:ContentPageName, page_content:str) -> RenderAction[ContentPage, None]:
        def render() -> StrDict:
            return {"page_content":page_content,
                    "title":page_name[0].upper() + page_name[1:],
                    }
        page:ContentPage = (page_name, render)
        return RenderAction[ContentPage, None](page, None)

    def game_introduction_page(self, gamecode:str, host_username:str, game_title:str) -> RenderAction[GameIntroductionPage, ModalText]:
        def render() -> StrDict:
            return {"gamecode":gamecode, "game_title":game_title, "host_username":host_username, "title":game_title}
        page:GameIntroductionPage = ("game_introduction", render)
        return RenderAction[GameIntroductionPage, ModalText](page, self.modal)

    def game_admin_page(self, game:Game,
            public_listing:WikiUrlMenuPublic[str],
            private_listing:WikiUrlMenuPrivate[str]) -> RenderAction[GameAdminPage, ModalText]:
        def render() -> StrDict:
            return {"gamecode":game.code,
                    "game_instance":game,
                    "weekDayNumberToLiteral":WeekTime.weekDayNumberToLiteral,
                    "publicListing":public_listing, "privateListing":private_listing, "title":f"{game.title} Admin Panel"}
        page:GameAdminPage = ("game_admin", render)
        return RenderAction[GameAdminPage, ModalText](page, self.modal)

    def success(self, success_string:str) -> RenderAction[Page, ModalSuccess]:
        return RenderAction[Page, ModalSuccess](self.page, modal_success(success_string))

    def error(self, error_string:str) -> RenderAction[Page, ModalError]:
        return RenderAction[Page, ModalError](self.page, modal_error(error_string))

    def with_error_modal(self, modal:ModalError) -> RenderAction[Page, ModalError]:
        return RenderAction[Page, ModalError](self.page, modal)

    def with_success_modal(self, modal:ModalSuccess) -> RenderAction[Page, ModalSuccess]:
        return RenderAction[Page, ModalSuccess](self.page, modal)

    def with_no_modal(self) -> RenderAction[Page, None]:
        return RenderAction[Page, None](self.page, None)

    def with_modal_union(self, modal:ModalTextUnion) -> Union[RenderAction[Page, None], RenderAction[Page, ModalSuccess], RenderAction[Page, ModalError]]:
        if modal is None:
            return RenderAction[Page, None](self.page, None)
        elif modal[0] == "success":
            return RenderAction[Page, ModalSuccess](self.page, modal)
        elif modal[0] == "error":
            return RenderAction[Page, ModalError](self.page, modal)


class RedirectAction:
    tag:ClassVar[Literal["redirect"]] = "redirect"

    def __init__(self, url:str, permanent:bool = False) -> None:
        self.url, self.permanent = url, permanent

Action = Union[RenderAction[Page, ModalText], RedirectAction]


BaseRender = RenderAction.template()
PEmpty = RenderAction[EmptyPage, ModalText]
PGAdmin = RenderAction[GameAdminPage, ModalText]
PGIntro = RenderAction[GameIntroductionPage, ModalText]
PHome = RenderAction[HomePage, ModalText]
PContent = RenderAction[ContentPage, None]
PGame = Union[PGAdmin[ModalText], PGIntro[ModalText]]
PEmptyError = PEmpty[ModalError]
PGame_EmptyError = Union[PGame[ModalText], PEmptyError]
PGIntro_EmptyError = Union[PGIntro[ModalText], PEmptyError]
PGAdmin_EmptyError = Union[PGAdmin[ModalText], PEmptyError]
PGAdminSuccess = PGAdmin[ModalSuccess]
PGAdminError = PGAdmin[ModalError]
PGAdminSuccessError_EmptyError = Union[PGAdminSuccess, PGAdminError, PEmptyError]
PHomeNone = PHome[None]
PHomeError = PHome[ModalError]
PHomeSuccess = PHome[ModalSuccess]
PHomeSuccessError = Union[PHomeSuccess, PHomeError]
PHomeError_Redir = Union[PHomeError, RedirectAction]
PGAdminSuccess_HomeError_EmptyError = Union[PGAdminSuccess, PHomeError, PEmptyError]
PGAdminSuccess_Home_EmptyError = Union[PGAdminSuccess, PHome[ModalText], PEmptyError]


class HandlerInfo(Protocol):
    current_user:OptStr
    def clear_cookie(self, name:str, **kwargs:Any) -> None: ...
    def reverse_url(self, name: str, *args: Any) -> str: ...
    def get_login_finish_url(self) -> str: ...
    def set_signed_cookie(self, name:str, value:Union[str, bytes], expires_days:Optional[float] = 30, version:Optional[int] = None,
            **kwargs: Any) -> None: ...


class BaseHandler(RequestHandler, HandlerInfo, ABC):
    routes:ClassVar[_RuleList] = [(r"^/.well-known/acme-challenge/(.+)", StaticFileHandler, {"path":"lambdadelbot/.well-known/acme-challenge"})]
    get_method_allowed:ClassVar[bool] = False
    post_method_allowed:ClassVar[bool] = False
    controller:ClassVar[Controller]

    def __init_subclass__(cls, route:OptStr = None, handler_name:OptStr = None, **kwargs:Any) -> None:
        if route is not None and handler_name is not None:
            matcher = PathMatches(route)
            rule = Rule(matcher=matcher, target=cls, target_kwargs=kwargs, name=handler_name)
            BaseHandler.routes.append(rule)
        super().__init_subclass__()

    async def prepare(self) -> None:
        self.current_user:OptStr = await self.controller.validate_username_cookie(self.get_secure_cookie("username"), self)

    def get_login_finish_url(self) -> str:
        return config.webapp.url + self.reverse_url("login_finish")

    def get_template_namespace(self) -> StrDict:
        namespace = super().get_template_namespace()
        namespace["config"] = config
        namespace["GameState"] = GameState
        namespace["current_user"] = self.current_user
        namespace["CONTENT_PAGE_NAMES"] = CONTENT_PAGE_NAMES
        return namespace

    def _get_string(self, name:str) -> OptStr:
        return self.get_argument(name, None)

    def _get_integer(self, name:str) -> OptInt:
        if (value := self._get_string(name)) is None:
            return None
        try:
            int_value = int(value)
        except ValueError:
            return None
        else:
            return int_value

    def _get_checkbox(self, name:str) -> bool:
        return self._get_string(name) == "1"

    def _get_boolean(self, name:str) -> OptBool:
        if (value := self._get_integer(name)) is None:
            return None
        elif value == 0:
            return False
        elif value == 1:
            return True
        return None

    def _get_game_state(self, name:str) -> OptGameState:
        if (value := self._get_integer(name)) is None:
            return None
        try:
            game_state = GameState(cast(GameStateValue, value))
        except:
            return None
        else:
            return game_state

    def _get_mapping(self, name:str) -> Optional[StrMapping]:
        if (value := self._get_string(name)) is None:
            return None
        try:
            decoded_value = json.loads(value)
            if not isinstance(decoded_value, Mapping):
                raise ValueError
            mapping = cast(StrMapping, decoded_value)
        except (UnicodeDecodeError, json.JSONDecodeError, ValueError) as error:
            return None
        else:
            return mapping

    async def execute_action(self, action:Action[Page, ModalText]) -> None:
        if action.tag == "render":
            namespace = action.get_values()
            return await self.render("templates/base.html",  **namespace)
        elif action.tag == "redirect":
            return self.redirect(action.url, action.permanent)
        else:
            raise_logic_error(action.tag, "render", "redirect")

    async def get(self, *args:str) -> None:
        if not self.get_method_allowed:
            render_info_unallowed = await self.controller.on_unallowed_method_use(self)
            return await self.execute_action(render_info_unallowed)
        action = await self.control("GET", *args)
        return await self.execute_action(action)

    async def post(self, *args:str) -> None:
        if not self.post_method_allowed:
            render_info_unallowed = await self.controller.on_unallowed_method_use(self)
            return await self.execute_action(render_info_unallowed)
        action = await self.control("POST", *args)
        return await self.execute_action(action)

    @abstractmethod
    async def control(self, method:HTTPMethod, *args:str) -> Action:
        pass

class NoArgsHandler(BaseHandler):
    async def control(self, method:HTTPMethod, *args:str) -> Action:
        if len(args) != 0:
            return await self.controller.on_extra_args(self, *args)
        return await self.control_with_no_args(method)

    @abstractmethod
    async def control_with_no_args(self, method:HTTPMethod) -> Action:
        pass


class GameHandler(BaseHandler):
    async def control(self, method:HTTPMethod, *args:str) -> Action:
        if len(args) != 1:
            return await self.controller.on_missing_gamecode(self)
        gamecode = args[0]
        return await self.control_with_gamecode(method, gamecode)

    @abstractmethod
    async def control_with_gamecode(self, method:HTTPMethod, gamecode:str) -> Action:
        pass


class UserHandler(BaseHandler):
    async def control(self, method:HTTPMethod, *args:str) -> Action:
        if len(args) != 1:
            return await self.controller.on_missing_username(self)
        username = args[0]
        return await self.control_with_username(method, username)

    @abstractmethod
    async def control_with_username(self, method:HTTPMethod, gamecode:str) -> Action:
        pass

class ContentPageABCHandler(BaseHandler):


    def validate_content_page_name(self, page_name:str) -> Optional[ContentPageName]:
        about:ContentPageName = "about"
        changelog:ContentPageName = "changelog"
        if page_name == about:
            return about
        elif page_name == changelog:
            return changelog
        return None

    async def control(self, method:HTTPMethod, *args:str) -> Action:
        if len(args) != 1:
            return await self.controller.on_missing_content_page_name(self)
        page_name = self.validate_content_page_name(args[0])
        if page_name is None:
            return await self.controller.on_missing_content_page_name(self)
        return await self.control_with_page_name(method, page_name)

    @abstractmethod
    async def control_with_page_name(self, method:HTTPMethod, page_name:ContentPageName) -> Action:
        pass


