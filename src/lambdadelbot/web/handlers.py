# -*- coding: utf-8 -*-
import asyncio, functools, logging, secrets
from tornado.escape import json_decode
from tornado.web import RequestHandler
from tornado.websocket import WebSocketHandler
from typing import Any, Awaitable, Callable, cast, Coroutine, Dict, List, Optional, Tuple, TypeVar, TYPE_CHECKING, Union
from urllib.parse import urlencode
from ..constants.configuration import config
from ..constants.types import (HTTPMethod, OptStr)
from ..structs.game import Game
from ..structs.path_keys import PathKeys
from ..structs.tokens import RedditTokens, WrongAccountError, UsernameRetriever
from ..structs.week_time import WeekTime
from .base_handler import *

logger = logging.getLogger(config.loggerName)


class HomeHandler(NoArgsHandler, route=r"/", handler_name="home"):
    post_method_allowed = True
    get_method_allowed = True

    async def control_with_no_args(self, method:HTTPMethod) -> PGAdminSuccess_Home_EmptyError:
        if method == "GET":
            return await self.controller.get_home(self, None)
        form_name = self._get_string("f_form_name")
        if form_name is None:
            return await self.controller.get_home(self, modal_error("No form name was specified"))
        elif form_name == "create_game":
            game_title = self._get_string("f_game_title")
            host_username = self._get_string("f_host_username")
            gamecode_display = self._get_string("f_gamecode_display")
            subreddit_name = self._get_string("f_subreddit_name")
            return await self.controller.create_game(self, game_title, host_username, gamecode_display, subreddit_name)
        elif form_name == "delete_game":
            gamecode = self._get_string("f_gamecode")
            return await self.controller.delete_game(self, gamecode)
        elif form_name == "update_host":
            gamecode = self._get_string("f_gamecode")
            host_username = self._get_string("f_host_username")
            return await self.controller.update_host_username(self, gamecode, host_username)
        elif form_name == "add_host":
            host_username = self._get_string("f_host_username")
            return await self.controller.add_host(self, host_username)
        elif form_name == "remove_host":
            host_username = self._get_string("f_host_username")
            return await self.controller.remove_host(self, host_username)
        else:
            return await self.controller.get_home(self, modal_error(f"Unknown form name ({form_name})"))


class ContentPageHandler(ContentPageABCHandler, route=r"/(about|changelog)$", handler_name="content_page"):
    get_method_allowed = True
    post_method_allowed = False

    async def control_with_page_name(self, method:HTTPMethod, page_name:ContentPageName) -> PContent:
        return await self.controller.get_content_page(self, page_name)


class LogoutHandler(NoArgsHandler, route=r"/logout$", handler_name="logout"):
    post_method_allowed = True

    async def control_with_no_args(self:BaseHandler, method:HTTPMethod) -> RedirectAction:
        return await self.controller.logout(self)


class LoginStartHandler(UserHandler, route=r"/login/start/(.+)$", handler_name="login_start"):
    get_method_allowed = True
    post_method_allowed = True

    async def control_with_username(self, method:HTTPMethod, path_username:str) -> PHomeError_Redir:
        username:OptStr = None
        if method == "GET":
            username = path_username
        elif path_username == config.webapp.login_start_default_username:
            username = self._get_string("f_username_login")
        return await self.controller.login_start(self, username)


class LoginFinishHandler(NoArgsHandler, route=r"/login/finish$", handler_name="login_finish"):
    get_method_allowed = True

    async def control_with_no_args(self, method:HTTPMethod) -> PHomeSuccessError:
        error = self._get_string("error")
        state = self._get_string("state")
        code = self._get_string("code")
        return await self.controller.login_finish(self, error, state, code)


class GamePageHandler(GameHandler, route=r"/game/(.+)$", handler_name="game"):
    get_method_allowed = True
    post_method_allowed = True

    async def control_with_gamecode(self, method:HTTPMethod, gamecode:str) -> PGame_EmptyError:
        if method == "GET":
            return await self.controller.get_game_page(self, gamecode, None)
        form_name = self._get_string("f_form_name")
        if form_name is None:
            return await self.controller.get_game_admin_page(self, gamecode, modal_error("No form name was specified"))
        elif form_name == "launch_manual_game_cycle":
            force_fetch = self._get_checkbox("fforce")
            wikiwrite = self._get_checkbox("fwikiwrite")
            return await self.controller.request_manual_game_cycle(self, gamecode, force_fetch, wikiwrite)
        elif form_name == "update_game_title":
            game_title = self._get_string("f_game_title")
            return await self.controller.update_game_title(self, gamecode, game_title)
        elif form_name == "update_gamecode_display":
            gamecode_display = self._get_string("f_gamecode_display")
            return await self.controller.update_gamecode_display(self, gamecode, gamecode_display)
        elif form_name == "update_subreddit":
            subreddit_name = self._get_string("f_subreddit_name")
            return await self.controller.update_subreddit_name(self, gamecode, subreddit_name)
        elif form_name == "update_schedule":
            start_day = self._get_integer("f_start_day")
            start_time = self._get_integer("f_start_time")
            end_day = self._get_integer("f_end_day")
            end_time = self._get_integer("f_end_time")
            return await self.controller.update_schedule(self, gamecode, start_day, start_time, end_day, end_time)
        elif form_name == "add_player":
            username = self._get_string("f_new_player_username")
            return await self.controller.add_player(self, gamecode, username)
        elif form_name == "remove_player":
            username = self._get_string("f_username")
            return await self.controller.remove_player(self, gamecode, username)
        elif form_name == "disconnect_player":
            username = self._get_string("f_username")
            return await self.controller.disconnect_player(self, gamecode, username)
        elif form_name == "toggle_active_state_player":
            username = self._get_string("f_username")
            active_state = self._get_boolean("f_active_state")
            return await self.controller.set_player_active_state(self, gamecode, username, active_state)
        elif form_name == "toggle_boldened_name":
            username = self._get_string("f_username")
            boldened_name = self._get_boolean("f_boldened_name")
            return await self.controller.set_player_boldened_name(self, gamecode, username, boldened_name)
        elif form_name == "update_player_sort_order":
            username = self._get_string("f_username")
            sort_order = self._get_integer("f_sort_order")
            return await self.controller.set_player_sort_order(self, gamecode, username, sort_order)
        elif form_name == "update_game_state":
            game_state = self._get_game_state("state")
            return await self.controller.update_game_state(self, gamecode, game_state)
        else:
            return await self.controller.get_game_admin_page(self, gamecode, modal_error(f"No form named {form_name}"))
