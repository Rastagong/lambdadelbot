#coding:utf-8
from __future__ import annotations
import asyncio, logging, markdown, secrets, tornado.autoreload
from pathlib import Path
from pymongo.errors import DuplicateKeyError
from typing import (Any, Awaitable, Callable, cast, Coroutine, Literal, NewType,
        Optional, Protocol, Tuple, Type, TypeVar, Union)
from ..constants.configuration import config
from ..constants.types import (Bot, BotRole, FFalse, GameRoutineParams, Host, HostRole, GameState,
        GameStatesSequence,
        OptBool, OptBytes, OptGameState, OptInt, OptStr, Player, PlayerRole, Role, StrMapping, TTrue)
from ..constants.exceptions import (LogicError, NoConfigError, WrongAccountError, raise_logic_error)
from ..connector import Connector
from ..database.exceptions import (DocumentNotFoundError, NoDeletionError, NoMatchError)
from ..database.singleton import handle_db
from ..redditmessenger import ResponseError, NetworkError
from ..structs.game import Game, get_game
from ..structs.path_keys import PKTDefault
from ..structs.tokens import RedditTokens
from ..structs.week_time import WeekTime
from ..structs.wiki_url import WikiUrl, WikiUrlMenuPublic, WikiUrlMenuPrivate
from .base_handler import *
logger = logging.getLogger(config.loggerName)
__all__ = ["Controller"]


A = TypeVar("A")

async def handle_http(awaitable:Awaitable[A]) -> Union[Tuple[TTrue, A], Tuple[FFalse, Exception]]:
    try:
        result:A = await awaitable
    except Exception as error:
        logger.exception(f"The following error occurred while awaiting an awaitable:")
        answer_no:Tuple[FFalse, Exception] = (False, error)
        return answer_no
    else:
        answer_yes:Tuple[TTrue, A] = (True, result)
        return answer_yes

ControllerMethod = Callable[..., Coroutine[Any, Any, Action]]
CM = TypeVar("CM", bound=ControllerMethod)

def ensure_is_game_host(controller_method:CM) -> CM:
    async def wrapper(self:Controller, handler:HandlerInfo, gamecode:str, *args:Any, **kwargs:Any) -> Action:
        host_username = await handle_db(self.r.get_host_username(gamecode), DocumentNotFoundError, None)
        if host_username is None:
            return await self.get_empty_page(handler, modal_error("No such game exists! Please double-check the URL."))
        elif handler.current_user not in (host_username, config.adminUsername):
            return await self.get_game_introduction_page(handler, gamecode, None)
        return await controller_method(self, handler, gamecode, *args, **kwargs)
    return cast(CM, wrapper)

def ensure_is_host(controller_method:CM) -> CM:
    async def wrapper(self:Controller, handler:HandlerInfo, *args:Any, **kwargs:Any) -> Action:
        if handler.current_user is None or not await self.r.is_any_host(handler.current_user):
            return await self.get_home(handler, None)
        return await controller_method(self, handler, *args, **kwargs)
    return cast(CM, wrapper)

def ensure_is_admin(controller_method:CM) -> CM:
    async def wrapper(self:Controller, handler:HandlerInfo, *args:Any, **kwargs:Any) -> Action:
        if handler.current_user is None or handler.current_user != config.adminUsername:
            return await self.get_home(handler, None)
        return await controller_method(self, handler, *args, **kwargs)
    return cast(CM, wrapper)

def ensure_game_is_paused(controller_method:CM) -> CM:
    async def wrapper(self:Controller, handler:HandlerInfo, gamecode:str, *args:Any, **kwargs:Any) -> Action:
        logger.debug(f"Ensuring the <{gamecode}> game is paused before processing the admin form")
        gameState = await self.r.get_game_state(gamecode)
        if gameState == GameState.paused:
            return await controller_method(self, handler, gamecode, *args, **kwargs)
        else:
            logger.warning(f"The <{gamecode}> game is not paused but {gameState}: {controller_method} cannot be processed")
            return await self.get_game_admin_page(handler, gamecode, modal_error("You need to pause the game before changing this setting"))
    return cast(CM, wrapper)

def acquire_game_lock(op_name_prefix:str, *game_states:GameState) -> Callable[[CM], CM]:
    def decorator(controller_method:CM) -> CM:

        async def release_lock(self:Controller, gamecode:str) -> None:
            if not await self.r.release_game_lock(gamecode, "web"):
                logger.warning(f"Could not release the {gamecode} lock for the {controller_method} controller method")

        async def wrapper(self:Controller, handler:HandlerInfo, gamecode:str, *args:Any, **kwargs:Any) -> Action:
            logger.debug(f"Attempting to acquire the {gamecode} game lock for the {controller_method} controller method")
            game_states_sequence = game_states if game_states else None
            if not await self.r.acquire_game_lock(gamecode, "web", op_name_prefix + secrets.token_hex(), game_states=game_states_sequence):
                logger.warning(f"The {gamecode} lock could not be acquired for the {controller_method} controller method")
                modal = modal_error("The fetch request for this game is currently running in the worker process, please try again later.")
                return await self.get_game_admin_page(handler, gamecode, modal)
            try:
                action = await controller_method(self, handler, gamecode, *args, **kwargs)
            except Exception as error:
                await release_lock(self, gamecode)
                raise error
            else:
                await release_lock(self, gamecode)
                return action

        return cast(CM, wrapper)
    return decorator


class Controller:
    def __init__(self) -> None:
        self.r = Connector()
        self.content = self.load_content_sources()
        logger.info(f"Content loaded: {self.content}")

    def convert_markdown(self, markdown_text:str) -> str:
        html = markdown.markdown(markdown_text,
                extensions=config.webapp.markdown_extensions,
                extension_configs=config.webapp.markdown_extension_configs,
                output_format="html")
        return html

    def load_content_sources(self) -> StrMapping:
        sources_dir = Path(config.webapp.markdown_sources_dir)
        markdown_sources = tuple(source for source in sources_dir.iterdir() if source.suffix == ".md")
        if config.debugMode:
            for source in markdown_sources:
                tornado.autoreload.watch(str(source))
        return {source.name:self.convert_markdown(source.read_text())
                for source in markdown_sources}

    async def on_app_start(self) -> None:
        await self.r.release_all_game_locks("web")
        await self.r.clear_all_auth_requests()

    async def on_unallowed_method_use(self, handler:HandlerInfo) -> PHomeNone:
        return await self.get_home(handler, None)

    async def on_extra_args(self, handler:HandlerInfo, *args:str) -> PHomeNone:
        return await self.get_home(handler, None)

    async def on_missing_username(self, handler:HandlerInfo) -> PHomeError:
        return await self.get_home(handler, modal_error("Missing username"))

    async def on_missing_gamecode(self, handler:HandlerInfo) -> PHomeError:
        return await self.get_home(handler, modal_error("Missing gamecode"))

    async def on_missing_content_page_name(self, handler:HandlerInfo) -> PHomeError:
        return await self.get_home(handler, modal_error("Missing content page name"))

    async def validate_username_cookie(self, cookie_value:OptBytes, handler:HandlerInfo) -> OptStr:
        if cookie_value is None:
            return None
        username = cookie_value.decode("utf-8")
        role = await self.r.get_role_for_user(username)
        if role is None:
            handler.clear_cookie("username")
            return None
        return username

    async def get_empty_page(self, handler:HandlerInfo, modal:ModalText) -> PEmpty[ModalText]:
        action = BaseRender
        if modal is None:
            return action.with_no_modal()
        elif modal[0] == "success":
            return action.with_success_modal(modal)
        elif modal[0] == "error":
            return action.with_error_modal(modal)
        else:
            raise_logic_error(modal[0], "success", "error")

    async def get_content_page(self, handler:HandlerInfo, page_name:ContentPageName) -> PContent:
        page_content = self.content[f"{page_name}.md"]
        action = BaseRender.content_page(page_name, page_content)
        return action

    async def get_home(self, handler:HandlerInfo, modal:ModalText) -> PHome[ModalText]:
        home_text = self.content["home.md"]
        games = tuple(Game(game_doc) for game_doc in await self.r.get_game_documents())
        is_config_ready = await self.r.is_config_ready()
        host_usernames = await self.r.get_host_usernames()
        is_any_host, is_admin = False, False
        if handler.current_user is not None:
            is_any_host = await self.r.is_any_host(handler.current_user)
            is_admin = handler.current_user == config.adminUsername
        action = BaseRender.homepage(home_text, games, host_usernames, is_any_host, is_admin, is_config_ready)
        if modal is None:
            return action.with_no_modal()
        elif modal[0] == "success":
            return action.with_success_modal(modal)
        elif modal[0] == "error":
            return action.with_error_modal(modal)
        else:
            raise_logic_error(modal[0], "success", "error")

    async def logout(self, handler:HandlerInfo) -> RedirectAction:
        handler.clear_cookie("username")
        return RedirectAction(handler.reverse_url("home"))
    
    async def login_start(self, handler:HandlerInfo, username:OptStr) -> PHomeError_Redir:
        """Starts a registration request for <username> with their highest role, if that user exists."""
        if username is None:
            return await self.get_home(handler, modal_error("A username must be provided."))
        role = await self.r.get_role_for_user(username)
        if role is None:
            return await self.get_home(handler, modal_error("There is no player with such a name, you cannot register them."))
        url = await self.r.create_auth_request(handler, username, role)
        return RedirectAction(url)

    async def clear_expired_auth_requests(self) -> None:
        while True:
            await asyncio.sleep(60)
            await self.r.clear_expired_auth_requests()

    async def login_finish(self, handler:HandlerInfo, error:OptStr, state:OptStr,
            code:OptStr) -> PHomeSuccessError:
        """Finished the incoming OAuth registration via Reddit. Fails if there is any <error> or if the random <state> is invalid.
        Retrieved an access token and check that the logged in user is the one we expected according to our auth request."""
        #TODO: validate state and code when None?
        async def username_retriever(rtokens:RedditTokens) -> str:
            return await self.r.getAuthenticatedUsername(rtokens)
        if error is not None:
            modal = modal_error(f"The registration request has been denied ({error}). You must authorise Lambadelbot to access your account.")
            return await self.get_home(handler, modal)
        elif state is None:
            return await self.get_home(handler, modal_error("There was no state string to parse in the query parameters"))
        elif code is None:
            return await self.get_home(handler, modal_error("There was no code string to parse in the query parameters"))
        result = await handle_db(self.r.auth_request_authenticate_success(state), DocumentNotFoundError, None)
        if result is None:
            return await self.get_home(handler, modal_error("This registration request has an invalid state!"))
        expected_username, role = result
        rtokens:RedditTokens
        if role == Player:
            rtokens = RedditTokens[PlayerRole].player(None, expected_username)
        elif role == Host:
            rtokens = RedditTokens[HostRole].host(expected_username)
        elif role == Bot:
            rtokens = RedditTokens[BotRole].bot()
        else:
            raise LogicError(role, Role)
        try:
            await rtokens.initialize(handler, code, expected_username, username_retriever)
        except WrongAccountError as err:
            #Not the user we expected (tokens automatically revoked when this is raised)
            #We delete any tokens of the actual user in DB
            await self.r.erase_reddit_tokens_for_user_depending_on_role(err.actual_username) 
            error_string = f"Wrong account, you were expected to log in as {err.expected_username}, not as {err.actual_username}"
            return await self.get_home(handler, modal_error(error_string))
        except NoConfigError as config_db_error:
            if role != Bot:
                return await self.get_home(handler, modal_error(f"We could not retrieve the tokens for your account. Please retry later."))
            await self.r.insert_config_document(rtokens)
            admin_result = await handle_db(self.r.add_user(config.adminUsername), DuplicateKeyError, None)
            success_string = f"This application is now ready to be used! The admin user ({config.adminUsername}) "
            if admin_result is None:
                success_string += f"was already configured."
            else:
                success_string += f"has been added too."
            handler.set_signed_cookie("username", expected_username)
            handler.current_user = expected_username
            return await self.get_home(handler, modal_success(success_string))
        except Exception as error:
            return await self.get_home(handler, modal_error(f"We could not retrieve the tokens for your account. Please retry later."))
        else:
            handler.set_signed_cookie("username", expected_username)
            handler.current_user = expected_username
            success_string = "Registration complete!"
            if role == Host:
                success_string += " You can now access the admin panel for games you host."
            elif role == Player:
                success_string += " Lambdadelbot will automatically fetch the PMs of this game account"
            return await self.get_home(handler, modal_success(success_string))

    async def get_game_introduction_page(self, handler:HandlerInfo, gamecode:str,
            modal:ModalText) -> PGIntro_EmptyError[ModalText]:
        result = await handle_db(self.r.get_game_essential_info(gamecode), DocumentNotFoundError, None)
        if result is None:
            return await self.get_empty_page(handler, modal_error("No such game exists! Please double check the URL."))
        host_username, game_title = result
        action = BaseRender.game_introduction_page(gamecode, host_username, game_title)
        if modal is None:
            return action.with_no_modal()
        elif modal[0] == "success":
            return action.with_success_modal(modal)
        elif modal[0] == "error":
            return action.with_error_modal(modal)
        else:
            raise_logic_error(modal[0], "success", "error")

    async def get_game_page(self, handler:HandlerInfo, gamecode:str,
            modal:ModalText) -> PGame_EmptyError[ModalText]:
        host_username = await handle_db(self.r.get_host_username(gamecode), DocumentNotFoundError, None)
        if host_username is None:
            return await self.get_empty_page(handler, modal_error("No such game exists! Please double-check the URL."))
        elif handler.current_user not in (host_username, config.adminUsername):
            return await self.get_game_introduction_page(handler, gamecode, modal)
        return await self.get_game_admin_page(handler, gamecode, modal)

    @ensure_is_game_host
    async def get_game_admin_page(self, handler:HandlerInfo, gamecode:str,
            modal:ModalText) -> PGAdmin_EmptyError[ModalText]:
        game = await handle_db(get_game(gamecode, self.r), DocumentNotFoundError, None)
        if game is None:
            return await self.get_empty_page(handler, modal_error("No such game exists! Please double-check the URL."))
        base_url = WikiUrl.base_url(game.display_code)
        public_url = base_url.public_listing().from_subreddit(game.subreddit_name)
        private_url = base_url.private_listing(game.path_keys[PKTDefault]).from_subreddit(game.subreddit_name)
        action = BaseRender.game_admin_page(game, public_url, private_url)
        if modal is None:
            return action.with_no_modal()
        elif modal[0] == "success":
            return action.with_success_modal(modal)
        elif modal[0] == "error":
            return action.with_error_modal(modal)
        else:
            raise_logic_error(modal[0], "success", "error")

    @acquire_game_lock("handlers", GameState.paused)
    @ensure_is_game_host
    async def set_player_active_state(self, handler:HandlerInfo, gamecode:str,
            username:OptStr, active_state:OptBool) -> PGAdminSuccessError_EmptyError:
        if active_state is None:
            error_string = f"You must choose a new state for {username}"
            return await self.get_game_admin_page(handler, gamecode, modal_error(error_string))
        elif username is None:
            error_string = f"You must select a player"
            return await self.get_game_admin_page(handler, gamecode, modal_error(error_string))
        awaitable = self.r.set_player_active_state(gamecode, username, active_state)
        result = await handle_db(awaitable, DocumentNotFoundError, None)
        if result is None:
            error_string = f"{username} is not a player of the {gamecode} game" 
            return await self.get_game_admin_page(handler, gamecode, modal_error(error_string))
        if active_state:
            success_string = f"Lambda will collect the PMs of {username} again!"
        else:
            success_string = f"{username} has been put aside, Lambda will stop collecting their PMs!"
        return await self.get_game_admin_page(handler, gamecode, modal_success(success_string))

    @ensure_is_game_host
    async def set_player_boldened_name(self, handler:HandlerInfo, gamecode:str,
            username:OptStr, boldened_name:OptBool) -> PGAdminSuccessError_EmptyError:
        if boldened_name is None:
            error_string = f"You must choose a new boldened name option for {username}"
            return await self.get_game_admin_page(handler, gamecode, modal_error(error_string))
        elif username is None:
            error_string = f"You must select a player"
            return await self.get_game_admin_page(handler, gamecode, modal_error(error_string))
        awaitable = self.r.set_player_boldened_name(gamecode, username, boldened_name)
        result = await handle_db(awaitable, DocumentNotFoundError, None)
        if result is None:
            error_string = f"{username} is not a player of the {gamecode} game" 
            return await self.get_game_admin_page(handler, gamecode, modal_error(error_string))
        if boldened_name:
            success_string = f"The name of {username} will now be boldened on the wiki!"
        else:
            success_string = f"The name of {username} will no longer be boldened on the wiki!"
        return await self.get_game_admin_page(handler, gamecode, modal_success(success_string))

    @ensure_is_game_host
    async def set_player_sort_order(self, handler:HandlerInfo, gamecode:str,
            username:OptStr, sort_order:OptInt) -> PGAdminSuccessError_EmptyError:
        if sort_order is None:
            error_string = f"You must choose a new sort order for {username}"
            return await self.get_game_admin_page(handler, gamecode, modal_error(error_string))
        elif username is None:
            error_string = f"You must select a player"
            return await self.get_game_admin_page(handler, gamecode, modal_error(error_string))
        awaitable = self.r.set_player_sort_order(gamecode, username, sort_order)
        result = await handle_db(awaitable, DocumentNotFoundError, None)
        if result is None:
            error_string = f"{username} is not a player of the {gamecode} game" 
            return await self.get_game_admin_page(handler, gamecode, modal_error(error_string))
        success_string = f"The sort order of {username} has been set to {sort_order}!"
        return await self.get_game_admin_page(handler, gamecode, modal_success(success_string))

    @ensure_is_game_host
    async def request_manual_game_cycle(self, handler:HandlerInfo, gamecode:str,
            force_fetch:bool, wikiwrite:bool) -> PGAdminSuccessError_EmptyError:
        opts = GameRoutineParams(force_fetch=force_fetch, wikiwrite=wikiwrite)
        logger.info(f"Sending manual cycle request for {gamecode}...")
        await self.r.send_manual_cycle_request(gamecode, opts)
        logger.info(f"Awaiting manual cycle answer for {gamecode}")
        answer = await self.r.await_manual_cycle_answer(gamecode)
        logger.info(f"Manual cycle scheduled for {gamecode}? {answer}")
        if answer is None:
            error_string = f"The worker process did not reply in time, please retry later"
            return await self.get_game_admin_page(handler, gamecode, modal_error(error_string))
        elif answer is False:
            error_string = f"A manual game cycle could not be started, another game cycle for {gamecode} must already be running"
            return await self.get_game_admin_page(handler, gamecode, modal_error(error_string))
        success_string = "The game cycle was successfully scheduled!"
        return await self.get_game_admin_page(handler, gamecode, modal_success(success_string))

    @ensure_is_game_host
    async def update_game_title(self, handler:HandlerInfo, gamecode:str,
            game_title:OptStr) -> PGAdminSuccessError_EmptyError:
        if game_title is None:
            error = modal_error("You must enter a new title for the game.")
            return await self.get_game_admin_page(handler, gamecode, error)
        await self.r.update_game_title(gamecode, game_title)
        success = modal_success(f"The title of this game has been updated to {game_title}")
        return await self.get_game_admin_page(handler, gamecode, success)

    @ensure_is_game_host
    async def update_gamecode_display(self, handler:HandlerInfo, gamecode:str,
            gamecode_display_unsafe:OptStr) -> PGAdminSuccessError_EmptyError:
        if gamecode_display_unsafe is None:
            error = modal_error("You must enter a code for the game.")
            return await self.get_game_admin_page(handler, gamecode, error)
        elif (match := config.app.gamecode_sanitizer.fullmatch(gamecode_display_unsafe)) is None:
            error = modal_error("The code must not contain any spaces or special characters.")
            return await self.get_game_admin_page(handler, gamecode, error)
        gamecode_display = match.group(1)
        await self.r.update_display_code(gamecode, gamecode_display)
        success = modal_success(f"The shortened code of this code has been updated to {gamecode_display}")
        return await self.get_game_admin_page(handler, gamecode, success)

    @ensure_is_game_host
    async def update_subreddit_name(self, handler:HandlerInfo, gamecode:str,
            subreddit_name_initial:OptStr) -> PGAdminSuccessError_EmptyError:
        if subreddit_name_initial is None:
            error_string = "You must provide a subreddit name"
            return await self.get_game_admin_page(handler, gamecode, modal_error(error_string))
        elif handler.current_user is None:
            raise ValueError(f"The current user should be 100% set at this point")
        awaitable = self.r.ensure_game_subreddit_ready(subreddit_name_initial, handler.current_user)
        ok, subreddit_name_or_reason = await awaitable
        if not ok:
            error_string = subreddit_name_or_reason
            return await self.get_game_admin_page(handler, gamecode, modal_error(error_string))
        subreddit_name = subreddit_name_or_reason
        await self.r.update_subreddit_name(gamecode, subreddit_name)
        success_string = f"The subreddit of this game has been updated to r/{subreddit_name}!"
        return await self.get_game_admin_page(handler, gamecode, modal_success(success_string))

    @ensure_is_game_host
    async def update_schedule(self, handler:HandlerInfo, gamecode:str, start_day:OptInt,
            start_time:OptInt, end_day:OptInt, end_time:OptInt) -> PGAdminSuccessError_EmptyError:
        if start_day is None or start_time is None or end_day is None or end_time is None:
            error = modal_error("You must supply numbers for each field.")
            return await self.get_game_admin_page(handler, gamecode, error)
        elif start_day < 0 or start_day > 7 or end_day < 0 or end_day > 7:
            error = modal_error("The week day must be correctly formatted.")
            return await self.get_game_admin_page(handler, gamecode, error)
        elif start_time < 0 or start_time > 23 or end_time < 0 or end_time > 23:
            error = modal_error("The time must be correctly formatted.")
            return await self.get_game_admin_page(handler, gamecode, error)
        start, end = WeekTime.generateSchedule(start_day, start_time, end_day, end_time)
        if not self.r.is_valid_schedule(start, end):
            error = modal_error("The start and end cannot be exactly the same: the game cannot run week-long without interruption.")
            return await self.get_game_admin_page(handler, gamecode, error)
        await self.r.update_schedule(gamecode, start, end)
        logger.info(f"New schedule for game {gamecode} is {start} to {end}")
        return await self.get_game_admin_page(handler, gamecode, modal_success(f"The schedule has been successfully updated!"))

    @acquire_game_lock("handlers", GameState.paused)
    @ensure_is_game_host
    async def add_player(self, handler:HandlerInfo, gamecode:str, initial_username:OptStr) -> PGAdminSuccessError_EmptyError:
        if initial_username is None or not initial_username:
            return await self.get_game_admin_page(handler, gamecode, modal_error("You must enter the username of a player to add"))
        username = await self.r.getRedditUsername(initial_username, RedditTokens[BotRole].bot())
        if username is None:
            return await self.get_game_admin_page(handler, gamecode, modal_error("There is no Reddit user with this name"))
        elif username.lower() != initial_username.lower():
            raise ValueError(f"Reddit unexpectedly returned a different username ({username}) than the initial one ({initial_username})")
        path_keys = await self.r.generate_path_keys_for_user_in_game(gamecode, username)
        result = await handle_db(self.r.add_player(gamecode, username, path_keys), DuplicateKeyError, None)
        if result is None:
            error_string = f"{username} is already playing in another game"
            return await self.get_game_admin_page(handler, gamecode, modal_error(error_string))
        elif await self.r.is_any_host(username):
            del_result = await handle_db(self.r.remove_player(gamecode, username), NoDeletionError, None)
            error_string = f"{username} is already a host, they cannot be a player too"
            return await self.get_game_admin_page(handler, gamecode, modal_error(error_string))
        return await self.get_game_admin_page(handler, gamecode, modal_success(f"{username} was added to this game!"))

    @acquire_game_lock("handlers", GameState.paused)
    @ensure_is_game_host
    async def remove_player(self, handler:HandlerInfo, gamecode:str,
            username:OptStr) -> PGAdminSuccessError_EmptyError:
        if username is None:
            return await self.get_game_admin_page(handler, gamecode, modal_error("You must enter the username of a player to remove."))
        done = await self.r.disconnect_player(gamecode, username)
        result = await handle_db(self.r.remove_player(gamecode, username), NoMatchError, None)
        if result is None:
            return await self.get_game_admin_page(handler, gamecode, modal_error(f"There is no player named {username} playing in this game."))
        return await self.get_game_admin_page(handler, gamecode, modal_success(f"{username} was removed from this game!"))

    @acquire_game_lock("handlers", GameState.paused)
    @ensure_is_game_host
    async def disconnect_player(self, handler:HandlerInfo, gamecode:str,
            username:OptStr) -> PGAdminSuccessError_EmptyError:
        if username is None:
            return await self.get_game_admin_page(handler, gamecode, modal_error("You must enter the username of a player to disconnect"))
        done = await self.r.disconnect_player(gamecode, username)
        if not done:
            return await self.get_game_admin_page(handler, gamecode, modal_error(f"There is no player named {username} in this game"))
        return await self.get_game_admin_page(handler, gamecode, modal_success(f"{username} was successfully disconnected from this game!"))

    @acquire_game_lock("handlers")
    @ensure_is_game_host
    async def update_game_state(self, handler:HandlerInfo, gamecode:str,
            new_state:OptGameState) -> PGAdminSuccessError_EmptyError:
        if new_state is None:
            return await self.get_game_admin_page(handler, gamecode, modal_error("This isn't a valid game state."))
        cur_state = await self.r.get_game_state(gamecode)
        host_username = await self.r.get_host_username(gamecode)
        ok = False
        if cur_state == GameState.paused and new_state == GameState.running:#Paused->Running
            if handler.current_user is None:
                raise ValueError(f"The current user should be 100% set at this point")
            result = await self.r.is_game_ready_to_run(gamecode, host_username)
            if result[0] is False:
                return await self.get_game_admin_page(handler, gamecode, modal_error(result[1]))
            ok, success_string = True, "The game is now running!"
        elif cur_state == GameState.running and (new_state == GameState.paused): #Running -> Paused
            ok, success_string = True, "The game is now paused!" 
        elif cur_state == GameState.running and (new_state == GameState.almostFinished): #Running -> Almost finished
            ok, success_string = True, "The game was marked as almost finished! One more game cycle needs to be run before the end now." 
        elif cur_state == GameState.almostFinished and (new_state == GameState.running) : #Almost finished -> Paused
            ok, success_string = True, "The game is now running again!"
        elif cur_state == GameState.finished and new_state == GameState.almostFinished: #One more fetch
            ok, success_string = True, "The game is now (almost) finished. All conversations will be made available at the public URL very shortly."
        if not ok:
            return await self.get_game_admin_page(handler, gamecode, modal_error("You cannot change the state of this game in this way."))
        await self.r.set_game_state(gamecode, new_state)
        return await self.get_game_admin_page(handler, gamecode, modal_success(success_string))

    @ensure_is_host
    async def create_game(self, handler:HandlerInfo,
            game_title:OptStr, host_username:OptStr, gamecode_display_unsafe:OptStr,
            subreddit_name:OptStr) -> PGAdminSuccess_HomeError_EmptyError:
        if game_title is None:
            return await self.get_home(handler, modal_error("You must enter a title for the new game"))
        elif host_username is None:
            return await self.get_home(handler, modal_error("You must enter a username for the new host."))
        elif handler.current_user != config.adminUsername and host_username != handler.current_user:
            return await self.get_home(handler, modal_error("You must be an admin to create a game for someone else."))
        elif gamecode_display_unsafe is None:
            return await self.get_home(handler, modal_error("You must enter a code for the new game."))
        elif subreddit_name is None:
            return await self.get_home(handler, modal_error("You must enter a name for the subreddit of the new game."))
        elif handler.current_user is None:
            raise ValueError(f"The current user should be 100% set at this point")
        elif (match := config.app.gamecode_sanitizer.fullmatch(gamecode_display_unsafe)) is None:
            return await self.get_home(handler, modal_error("The code must not contain any spaces or special characters."))
        gamecode_display = match.group(1)
        logger.info(f"Params: title {game_title}, host_username {host_username}, gamecode_display {gamecode_display}, subreddit_name {subreddit_name}")
        gamecode = await self.r.create_game(game_title, host_username, gamecode_display, subreddit_name)
        if not await self.r.is_any_host(host_username):
            # NOTE: Sad state of affairs in the MongoDB world!
            # We added a game for a host who isn't actually a host.
            # In the relational world of SQL, we would have atomically specified...
            # ... during the insert request to execute it only if the <host_username> foreign key...
            # ... existed in the hosts table. We can't do that with MongoDB.
            # So we manually have to delete the created game if the host is actually no host.
            # In practice, this will never happen, since:
            #   * Only the admin can create games hosted by someone other than themselves
            #   * This form is accessible only to hosts to begin with
            # But race conditions are still a thing, so let's delete the created game anyway.
            # View this as a very, very dirty transaction rollback.
            await self.r.delete_game(gamecode)
            return await self.get_home(handler, modal_error(f"{host_username} is not an authorized host."))
        success_string = "The game was successfully created!"
        return await self.get_game_admin_page(handler, gamecode, modal_success(success_string))

    @ensure_is_admin
    async def delete_game(self, handler:HandlerInfo, gamecode:OptStr) -> PHomeSuccessError:
        if gamecode is None:
            return await self.get_home(handler, modal_error("You must select a game to delete."))
        result = await self.r.delete_game(gamecode)
        if result.deleted_count <= 0:
            return await self.get_home(handler, modal_error(f"There's no game with code '{gamecode}'."))
        return await self.get_home(handler, modal_success(f"The game of code '{gamecode}' was successfully deleted!"))

    @ensure_is_admin
    async def update_host_username(self, handler:HandlerInfo, gamecode:OptStr, host_username:OptStr) -> PHomeSuccessError:
        if gamecode is None:
            return await self.get_home(handler, modal_error("You must select a game whose host you want to change."))
        elif host_username is None:
            return await self.get_home(handler, modal_error("You must enter a username for the new host of the game."))
        elif not await self.r.is_any_host(host_username):
            return await self.get_home(handler, modal_error(f"{host_username} is not an authorized host."))
        found = await self.r.update_game_host_username(gamecode, host_username)
        if not found:
            return await self.get_home(handler, modal_error(f"There's no game with code '{gamecode}'."))
        elif not await self.r.is_any_host(host_username):
            # NOTE: In case the new host is actually not a host, we set the new host to be the admin
            # But this is not atomic, so hardly 100% correct.
            logger.error(f"Set the host of {gamecode} to {host_username}, who isn't a host anymore!")
            logger.error(f"So, in last recourse, updating the host of {gamecode} to the admin ({config.adminUsername})")
            found = await self.r.update_game_host_username(gamecode, config.adminUsername)
            logger.error(f"Was there any update? {found}")
            return await self.get_home(handler, modal_error(f"{host_username} is not an authorized host."))
        return await self.get_home(handler, modal_success(f"{host_username} was successfully set as the new host of game {gamecode}!"))

    @ensure_is_admin
    async def add_host(self, handler:HandlerInfo, host_username:OptStr) -> PHomeSuccessError:
        if host_username is None:
            return await self.get_home(handler, modal_error("You must enter a username for the host to add."))
        result = await handle_db(self.r.add_user(host_username), DuplicateKeyError, None)
        if result is None:
            return await self.get_home(handler, modal_error(f"{host_username} is already a host."))
        return await self.get_home(handler, modal_success(f"{host_username} was successfully added as host!"))

    @ensure_is_admin
    async def remove_host(self, handler:HandlerInfo, host_username:OptStr) -> PHomeSuccessError:
        if host_username is None:
            return await self.get_home(handler, modal_error("You must enter a username for the host to remove."))
        elif host_username == config.adminUsername:
            return await self.get_home(handler, modal_error(f"The admin cannot be removed from the hosts."))
        result = await self.r.delete_host(host_username)
        if result.deleted_count <= 0:
            return await self.get_home(handler, modal_error(f"{host_username} is not a host."))
        await self.r.update_games_host_username_by_old_host_username(host_username, config.adminUsername)
        return await self.get_home(handler, modal_success(f"{host_username} was successfully removed from the list of hosts!"))
