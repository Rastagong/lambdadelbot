
# Useful Links
**Lambdadelbot's main instance** [lambda.rastagong.eu][l_main_instance]  
**Git Repository** [bitbucket.org/Rastagong/lambdadelbot][l_git]  
**Discord Server** [discord.gg/htCqvenPq4][l_discord]

Page last updated on **March 3, 2022**.

# A short introduction
Hey there! Rastagong here. I thought I'd use this page to give a bit of background information on Lambda. This is still very much a work-in-progress, please check it back from time to time!

**Lambdadelbot**{.lambdadelbot} is a Reddit bot I originally designed in late 2016, to facilitate hosting role-playing games on [r/MyLittleHouseOfFun][l_hof], though other communites have relied on Lambda too.

You're currently viewing Lambda **version 2**, which went online in early July 2021. This version is hosted on my own server. Previously, Lambdadelbot was hosted for free on Heroku, but both by design and because of limits inherent to Heroku, it was very unstable.

The role-playing games we're speaking of are usually Battle Royale death games in the style of popular visual novel series such as _Zero Escape_, where players fight to death after being kidnapped. Players role-play as their characters using Reddit accounts created for this purpose: they can interact publicly in Reddit threads ([example][l_oof_thread]), and have private conversations with each other through private messages.

**Lambdadelbot**{.lambdadelbot} is designed to regularly fetch the private messages exchanged by players while a game is running, and to archive them on a wiki at a secret URL. The host of the game can use that secret URL to discover the conversations their players are having in private, without having to manually log into the accounts of each player. (Of course, players know the host has this ability.)  

Non-participating members of the community ("audience members") usually have access to that URL too, to be able to follow the proceedings of the game from behind the scenes.

When the game ends, Lambda publishes everything at a cleaner, public URL, that can be shared with everyone. [Here's an example of private messages archive written by Lambda][l_oof_wiki], for the Ordeal of Fun game which ran during Summer 2017.

The website you're on is used by game hosts to control Lambdadelbot when they run their games: they can add the accounts of their players here and set a few settings to control her execution.

# FAQ for Players

## What does Lambda do with my account?
**Lambda**{.lambdadelbot} uses Reddit's API to gain access to the private messages you send and receive, and to archive them on a wiki.

## Do I need to do anything special?
No, not much! Just play the game and enjoy.

Although you should remember to use your game account **only for the purpose of playing this game**, since Lambda will read the messages you receive and write them to a wiki where other community members can see them. 

For your own sake, please do not use your game account for personal purposes!

# FAQ for Hosts

## Can I use Lambdadelbot for my own game?
Yes! Just ask me so on the Discord server, and I'll make you a host on my instance on Lambda!

## How often does Lambda fetch PMs?
On my instance, she's currently configured to launch a global routine to fetch PMs **every 10 minutes**.

## What does Lambda mean by _host_?
Lambda considers your regular main Reddit account to be your host account.  

Lambda will never fetch any PMs from your host account (the only permission she asks from hosts is knowing your username to authenticate you).  

With your host account, you can create & manage as many games as you want.

## What does Lambda mean by _player_?
Lambda considers any Reddit account she must fetch PMs from to be a player.

In other words, players include both playing characters and any NPCs that you may play yourself (or with somebody's help). If there are such characters in your game, and if they send or receive PMs from playing characters, you should add them as players.

## Can I host my own version of Lambda?
Feel free to! The Git repository has instructions to run your own instance of Lambda.


# Behind the scenes

## What's with the new URL of Lambda?
Lambda was previously hosted on Heroku, a service which makes it relatively easy to host web apps. I used the free tier, but it had a lot of limitations.

By moving Lambda on my own server, I was able to escape a lot of these limitations. In particular, I can have:

* a worker process that's running all the time and fetching PMs very frequently (on Heroku's free tier, this was super complicated to set up)
* full, persistent access to Lambda's logs, to debug the application more easily. Immediately after the launch of version 2, I fixed a somewhat insidious super easily just because I could finally read logs in their entirety.
* I also rewrote a significant version of Lambda's codebase for the new version, which is Lambda version 2

## What's new in Lambda version 2?
Hard to summarise it all!

There a a few additional minor features, but they're not the most important part.

Primarily, Lambda's much more stable now. The "lock" mechanism to prevent two game cycles for the same game to be run simultaneously has been simplified. I'll never have to manually unlock Lambda again.

The database schema is much clearer and better-suited to MongoDB.

I also added static typing (using mypy) to the entire codebase, which means that I can be much more confident in what I write. Whenever I add in a new feature or fix a bug, it's much easier to tell right away if I accidentally broke a part of Lambda and to fix that! Yeah for maintainability!

More functional programming patterns also made my code easier to reason about.
This was not a complete rewrite though, there are still lots and lots of ugly corners in the codebase, but at least now it's overall manageable!

## What are your plans for Lambda's future?
At the moment, documenting Lambda more & ensuring it becomes more stable.

I'm not planning to keep developing Lambda intensively & to add new features all the time (though I do have plans), I'd mostly like Lambda to become more reliable so I can do maintenance work more easily.

If anybody wants to pitch in & develop more features themselves, they're welcome to! I'm confident enough in version 2 to let it live its life now. Thanks to static typing & hopefully to more documentation in the future, it should become easier to pitch in.


[l_main_instance]: https://lambda.rastagong.eu "You're on it right now"
[l_git]: https://bitbucket.org/Rastagong/lambdadelbot/ ""
[l_discord]: https://discord.gg/htCqvenPq4 "Click to join the server"
[l_hof]: https://www.reddit.com/r/MyLittleHouseOfFun/ "Visit the subreddit"
[l_oof_wiki]: https://www.reddit.com/r/mylittlehouseoffun/wiki/oof "I played in this one! It was brutal, but my character survived!"
[l_oof_thread]: https://www.reddit.com/r/MyLittleHouseOfFun/comments/6j7pfo/oof_day_1_aint_no_rest_for_the_wicked/ "Day 1 thread of the Ordeal of Fun game (Summer 2017)"

# Privacy
This website uses a cookie to remember you when you log in, and another one for security reasons when you validate a form. No cookies are used beyond this!

I'll try to write a super short but transparent summary of Lambda's data processing here when I have the time to. But for the most part it's all low stakes since the only data processed are private messages related to the game, that Lambda accesses game accounts created for this purpose, and wholly distinct from players' regular Reddit accounts.

Lambda uses a database to store PMs and API keys to these accounts, but doesn't process them beyond the reposting on the wiki.

# Legal Mentions
This website is hosted by OVH, a company headquartered at 2 rue Kellermann, 59100 Roubaix, France.

