
# [v2.3.3](https://bitbucket.org/Rastagong/lambdadelbot/commits/tag/v2.3.3) [26 August 2024]
A patch release that fixes a new oddity from the reddit API. It's seemingly possible for:

* Players to send PMs to themselves; I had not planned for this, so Lambda isn't prepared to write conversation pages for a player with themselves  
* Even more strangely, it seems possible for conversation offshoots to start from a parent conversation: in response to a PM from the Game Master, a player may reply to someone else (the someone else being _themselves_, hence the self-PMs thing!)

Suffice to say, this does not make much sense, but we confirmed it could happen in terms data.  

This patch release fixes that by:  

1. Preparing in advance path keys for self-PMs so that self-conversation may have defined URLs like any other conversation page.  
2. Since path keys are added only when adding new players, this will not work for existing games. So this release also prepares a function which fills missing self-path keys for existing games at the start of their game cycle.  
3. As for conversation offshoots, the fix is simple: when structuring PMs in a conversation/tree structure, it is no longer assumed that reply PMs must belong to the same parent conversation. Conversation offshoots may be initialised from parent convos, all the while being their own independent convos.

I haven't tested, I'm pushing in production live!  
(I did perform static type checking. And the fix is confirmed via the debugging and manual fix I performed a few weeks ago.)

# [v2.3.2](https://bitbucket.org/Rastagong/lambdadelbot/commits/tag/v2.3.2) [27 August 2023]
A patch release which adds 2 Bash scripts to either create a database backup, or restore the database from an existing backup. 

The scripts are to be run on the host server, but send the appropriate commands to the database container.

I've had to perform some backups over this weekend:

* The first time, I performed a manual backup & restore, for the database upgrade from MongoDB 4.4 to 6.0 (v2.3.0)
* Then, I finally reinstalled this server to perform a clean upgrade from Debian 10 to Debian 12

For this second operation, I realised having an automated procedure would make it easier.


# [v2.3.1](https://bitbucket.org/Rastagong/lambdadelbot/commits/tag/v2.3.1) [26 August 2023]
Lambdadelbot v2.3.1 is a patch release fixing single-sourcing from the `pyproject.toml` when the package isn't installed through Poetry.

# [v2.3.0](https://bitbucket.org/Rastagong/lambdadelbot/commits/tag/v2.3.0) [25-26 August 2023]

Lambdadelbot v2.3.0 is a minor maintenance release.

## Dependencies Updates
The major updates are:  

* Python 3.9 to 3.11, which brings a lot of static type checking and new language features, that I could use in the future  
* MongoDB 4.4 (long outdated) to 6.0  
* Tornado 6.1.0 to 6.3.3, with a few important fixes

The Docker image used for Python has been changed from `-alpine` to `-slim` (similar size but more stable).

Other minor updates: Motor 2.5.1 to 3.2.0, python-dotenv 0.19.2 to 1.0.0, Markdown 3.3.6 to 3.4.4, mypy 0.931 to 1.5.1.   
The mypy update brings a lot of static type checking goodness too.  

pip-tools has been added as a development requirements to be able to generate a `requirements.txt` more easily from the `pyproject.toml`.

## Various type checking fixes
All due to mypy's upgrade.

## The version number and release date are now single-sourced from `pyproject.toml`  
Instead of being duplicated in the `config_web.env` config file.  

# [v2.2.1](https://bitbucket.org/Rastagong/lambdadelbot/commits/tag/v2.2.1) [18 March 2023]
Lambdadelbot v2.2.1 is a patch release regarding the deletion of players (added in the previous version, v2.2.0).

Previously, the deletion of a player also resulted in the deletion of the players's path keys (the randomly-generated strings appended in their conversation URLs, to make them secret and difficult to find).   
When the player had already exchanged PMs before being removed, this prevented the worker from writing these conversations to the wiki: it couldn't find the path keys required to build the conversation URLs, and so, failed to write these conversation pages.

This is fixed by ensuring that the path keys of a given player are never deleted even when that player is removed.  
When adding a player, Lambda also checks for existing path keys (because the player may have been previously removed before being added again), instead of assuming that all path keys have to be generated from scratch.

A short Python script was also added to restore path keys mistakenly deleted in the previous version for a game, by inferring them from the game's list of path hashes.

Finally, the game cycle was also tweaked to handle removed players whose status (boldened name, sort order) we may not know anymore since their deletion.

# [v2.2.0](https://bitbucket.org/Rastagong/lambdadelbot/commits/tag/v2.2.0) [8 February 2023]
Lambdadelbot v2.2.0 is a minor release featuring a number of bug fixes and a few minor new features:

## Improved choices for hosts to remove players
There are now 3 distinct options for hosts to remove or disable a player:

* They can choose to _stop collecting PMs_ for a given player for some time. This option used to be known as _temporarily put aside_ a player.
* They may now also revoke Reddit API tokens, which entirely disconnects the player from Lambda, while keeping them in the player list. This may be required for privacy reasons (ensuring Lambda can _never_ enter an account), or to connect them again if anything malfunctions.
* The option to remove a player entirely remains and has been fixed: since a regression from v2.1.3 regarding player removal, only unconnected players could be removed. Now connected and unconnected players alike can be removed. Removing a player now also revokes their API tokens, to leaved unused accounts in a cleaner state & to minimise data collection.

To make the previous changes work, functions related to Reddit tokens handling have been largely refactored to behave more consistently and more predictably.

## Finished games can be run and paused again
Previously, when a game was marked as finished, it was not possible to return to the "running" or "paused" state. This is now fixed: from the "almost finished" state, it is possible to return to "running" a game (and from there, to "pausing" a game again).

This is important because most game settings (removing a player, changing the subreddit, etc) can only be edited while a game is paused. The previous behaviour prevented any change once the game was marked as finished.

The game admin panel now also makes it more explicit that pausing a game is required to edit most settings, and some game state change success messages have been improved.

Choosing to stop collecting PMs for a given player now also requires the game to be paused.

## Bug fixes & other minor evolutions
* As long as at least one player is connected and active (not excluded from PMs collection), it is now possible to start running a game. Previously, Lambda mistakenly checked that _all_ players were connected and active, which defeated the point of having inactive or unconnected players temporarily put on the side.
* The admin can now start running someone else's game: previously, Lambda checked whether the admin was a mod on the game's subreddit, instead of checking that the host was.
* A few clarifications were made in the game admin panel UI.
* In DEBUG mode, when the optional `config.webapp.allow_paused_games_manual_game_cycles` setting is enabled, manual game cycles can now also be requested for running and almost finished games, and not just for paused games.
* The Dockerfile now includes the full micro version number of the Python release to use (e.g 3.9.16) instead of the minor version number alone (3.9). As soon as this version number is updated in the Dockerfile, this in turn forces my deployment system to rebuild the Docker container image with that more recent version, instead of waiting for a manual pull of an updated Python Docker image.

# [v2.1.5](https://bitbucket.org/Rastagong/lambdadelbot/commits/tag/v2.1.5) [5 March 2022]
* Revamps the display of games handled by Lambda on the homepage, by making it clear that all games are listed, whether they have already finished running or not. The homepage now lists the state of each game, and gives out the public archive link for finished games, to incite visitors to have a look at the end result. Many thanks to Tauros for suggesting a change to the homepage in that regard!
* Fixes the display of a game page when the visitor isn't logged in. The new admin forms added in v2.1.4 resulted in the reuse of a variable name both as a for loop variable on the homepage, and as a regular variable on the game page. This triggered a `referenced before assignment` error on the game page once the variable from the homepage was erased from scope after the execution of the loop (this may be a Tornado bug). This is fixed by taking care to use distinct variable names on both templates.

# [v2.1.4](https://bitbucket.org/Rastagong/lambdadelbot/commits/tag/v2.1.4) [3 March 2022]
* The admin can now change the host of an existing game, delete a game, remove a host and add a new host, all from the homepage.
* Internally, the handling of potential edge cases during game creation or host update have been improved, guaranteeing more accuracy to the database. These edge cases are due to MongoDB's lack of atomic operations across distinct tables.
* All dependencies have been updated, and Python updated from version 3.8 to version 3.9.
* Dependency files have been updated (using [pip-tools](https://pypi.org/project/pip-tools/)) to make it easier for people who do not use [Poetry](https://python-poetry.org) to install the project's dependencies just with pip.
* The README file has been updated to make it easier for people who do not use Docker to install the project using the Python version on their system.
* Fixes a typo in the `about` page.
* Web Design: Makes it clearer that the date displayed on the left is a release date, not today's date.


# [v2.1.3](https://bitbucket.org/Rastagong/lambdadelbot/commits/tag/v2.1.3) [4 November 2021]
When 0 newer PMs are received, Lambda correctly remembers the ID of the last PM she saw. Prior to this bug fix, when she received 0 newer PMs, she would incorrectly return None for this `last_seen` field, which made her fetch all PMs from scratch again in the next cycle. That was obviously very wasteful.  
Also documented the return of the `last_seen` field in the recursive method which fetches PMs, so that it is easier to understand in the future.

Lambda no longer launches a global routine for games which are already finished.

Unconnected players can now be removed from the game admin panel, just like connected players.

# [v2.1.2](https://bitbucket.org/Rastagong/lambdadelbot/commits/tag/v2.1.2) [28 September 2021]
Fixes the structure of the directory used by Certbot during the [webroot challenge](https://certbot.eff.org/docs/using.html#webroot), to grant the server an SSL certificate automatically.

# [v2.1.1](https://bitbucket.org/Rastagong/lambdadelbot/commits/tag/v2.1.1) [3 August 2021]
A few clarifications and documentation improvements: in the content pages, in `README.md`, in the file hierarchy of the project, on the game admin panel.

A few changes related to the `requirements.txt` files used to install the Python dependencies of the project inside Docker:  
The `requirements_frozen.txt` file is used as a sort of lockfile to install the project's dependencies when running `docker-compose build`.  
The development dependencies (e.g mypy) are in `requirements_dev.txt`, which can be easily used inside a Docker container when we want to type check the codebase.


# [v2.1.0](https://bitbucket.org/Rastagong/lambdadelbot/commits/tag/v2.1.0) [24 July 2021]
Adds content pages such as the changelog you're currently reading and the `about` page. Also adds a small introduction text on the homepage. All these texts are loaded from Markdown files.

Displays the version number and its release date on the left side.


# v2.0.1 [9 July 2021] 
The first working version of Lambda v2.

Fixes the parsing of "system" private messages sent by Reddit, which do not have an `author` key ([48e7fe3](https://bitbucket.org/Rastagong/lambdadelbot/commits/48e7fe336cce7808672e946e4e1c377b7ae17127)).

Ensures that game cycles crash when a subcrash occurs ([61b937c](https://bitbucket.org/Rastagong/lambdadelbot/commits/61b937c8ca8c2c3c4f1ee6713e3389a0cc0c974e)).  
Game cycles spawn asynchronous coroutines for each player, and each player subroutine itself spawns 2 subroutines (1 to fetch PMs in the inbox, to fetch sent PMs). When a spawned subroutine crashed, the whole game cycle crashed as expected, but a bug kept the spawned subroutines running, which meant there were "zombie" remains of previous game cycles which kept running.

# v2.0.0 [7 July 2021]
Lambda version 2 went up for the first time at [lambda.rastagong.eu][l_rastagong_eu]!  

She was not really functional yet, though.

[l_rastagong_eu]: https://lambda.rastagong.eu "You're there right now."
