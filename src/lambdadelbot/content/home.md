**Lambdadelbot**{.lambdadelbot} is a bot designed by Rastagong to facilitate running role-playing games on Reddit. 

Lambda was originally designed for use in the [r/MyLittleHouseOfFun][2] community, but has also been used on [r/KowzHOF][3].

Lambda collects the private messages that players exchange between themselves and [archives them on a wiki][5], so that the game host and audience members of the community can follow the private conversations of all players while a game is running.

If you would like to use Lambda in a game you run, or if you are interested in this project, [feel free to join our Discord server][6]!

**Lambdadelbot**{.lambdadelbot} is a free and open source project!  
Anyone can [have a look at the source code][1], to clone it, run their own version or to just improve it.  
Please note that this project is licensed under the terms of the [AGPL license][4], which contains a copyleft clause: if you run, host or create your own version, you'll have to license it under the same license.


[1]: https://bitbucket.org/Rastagong/lambdadelbot/ "Git repository on Bitbucket"
[2]: https://www.reddit.com/r/MyLittleHouseOfFun/ "Visit the subreddit"
[3]: https://www.reddit.com/r/KowzHOF/ "Visit the subreddit"
[4]: https://www.gnu.org/licenses/agpl-3.0.en.html "Read the full legal text"
[5]: https://www.reddit.com/r/mylittlehouseoffun/wiki/oof "Archive of the Ordeal of Fun game (Summer 2017)"
[6]: https://discord.gg/htCqvenPq4 "Click to join the server"
