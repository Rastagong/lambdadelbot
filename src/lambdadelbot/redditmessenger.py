# -*- coding: utf-8 -*-
import asyncio, functools, logging, ssl, concurrent.futures
from collections import defaultdict, deque
from datetime import datetime
from json.decoder import JSONDecodeError
from tornado.httpclient import AsyncHTTPClient, HTTPRequest, HTTPResponse, HTTPClientError
from tornado.escape import json_encode, json_decode
from typing import (Any, Awaitable, Callable, cast, Coroutine, Deque, Dict, List, Mapping, NoReturn, Optional,
        overload, Tuple, Type, TypedDict, TypeVar, Union)
from urllib.parse import urlencode, urlparse, quote
from .constants.configuration import config
from .constants.logging_configuration import (X, OX)
from .constants.types import (AnswerType, AnswerTypeJSON, AnswerTypeOK, BotRole, Direction,
        FFalse, HTTPMethod, OptDict, OptInt, OptStr, StrFrozenSet, StrMapping, StrSet, TokenType, TTrue)
from .constants.exceptions import LambdaError
from .structs.wiki_url import WikiUrl
from .structs.tokens import OptRedditTokens, RedditTokens
logger = logging.getLogger(config.loggerName)

NetworkResponse = Union[HTTPResponse, HTTPClientError]
Response = Union[NetworkResponse, Exception]

def ensureNetworkX(ox:OX) -> X:
    if ox is None:
        return X(network=True)
    if "network" not in ox:
        return ox(network=True)
    return ox

def decodeBody(body:bytes) -> Union[str, UnicodeDecodeError]:
    try:
        decodedBody = body.decode("utf-8")
    except UnicodeDecodeError as error:
        return error
    else:
        return decodedBody

def getBodyStr(source:Union[HTTPClientError, HTTPResponse]) -> str:
        if isinstance(source, HTTPClientError):
            if source.response is None:
                return "no body in the response"
            body = source.response.body
        else:
            body = source.body
        decodedBody = decodeBody(body)
        if isinstance(decodedBody, UnicodeDecodeError):
            return f"failed to decode the body from bytes: {decodedBody}"
        return f"with the following body: {decodedBody}"


class NetworkError(LambdaError):

    def __init__(self, msg:str, response:Response, x:X) -> None:
        if isinstance(response, HTTPClientError):
            bodyStr = getBodyStr(response)
            super().__init__(f"{msg}. HTTP code {response.code}, {bodyStr}", exc_info=response, x=x)
        elif isinstance(response, HTTPResponse):
            bodyStr = getBodyStr(response)
            super().__init__(f"{msg}. HTTP error code {response.code} {response.reason}, {bodyStr}", x=x)
        elif isinstance(response, Exception):
            super().__init__(msg, exc_info=response, x=x)


class ResponseError(NetworkError):

    def __init__(self, msg:str, response:NetworkResponse, x:X) -> None:
        super().__init__(f"The request succeeded, but the response was incorrect: {msg}", response, x=x)

class RequestWrapper:

    def __init__(self, originalUrl:str, method:HTTPMethod, answerType:AnswerType,
            sslCtx:ssl.SSLContext, data:OptDict,
            tokens:OptRedditTokens, x:X) -> None:
        self._params:Dict =  {"headers":{"User-Agent":config.reddit.ua}, "url":originalUrl, "ssl_options":sslCtx, "method":method}
        if data is None and answerType == "json":
            data = {}
        if data is not None:
            self._params["url"], self._params["body"] = self._encodeData(data, method, answerType, originalUrl, tokens is None)
        if tokens is None:
            self._params["auth_mode"] = "basic"
            self._params["auth_username"], self._params["auth_password"] = config.reddit.clientID, config.reddit.clientSecret
        else:
            self._params["headers"]["Authorization"] = f"bearer {tokens.access_token}"
        self._requestObj = HTTPRequest(**self._params)
        self._selfStr = self._makeSelfRepr(method, originalUrl, tokens is None, data)
        self.rebuildWithNewTokens = self._rebuildWithNewTokensWrapper(originalUrl, method, answerType, sslCtx, data)

    def _rebuildWithNewTokensWrapper(self, originalUrl:str, method:HTTPMethod, answerType:AnswerType,
            sslCtx:ssl.SSLContext, data:Optional[Dict]) -> Callable[[RedditTokens, X], "RequestWrapper"]:
        cls = self.__class__
        def decoratedFunction(tokens:RedditTokens, x:X) -> "RequestWrapper":
            return cls(originalUrl=originalUrl, method=method, answerType=answerType,
                    sslCtx=sslCtx, data=data, tokens=tokens, x=x)
        return decoratedFunction

    def _makeSelfRepr(self, method:HTTPMethod, originalUrl:str, tokensExist:bool, data:Optional[StrMapping]) -> str:
        clientCredentials = "Set" if not tokensExist else "None"
        authHeader = "Set" if tokensExist else "None"
        dataStr = str(data) if data is not None else "None"
        return f"Method: {method}\nThis request is bound for: {originalUrl}\nClient credentials: {clientCredentials}\nAuth header: {authHeader}\nData: {data}"

    def _encodeData(self, data:Dict, method:HTTPMethod, answerType:AnswerType, url:str, tokens_request:bool) -> Tuple[str, OptStr]:
        if answerType == "json" and not tokens_request: #Additional condition (implicit): only for requests to Reddit
            data["raw_json"] = 1
        encodedData = urlencode(data)
        if method == "GET":
            return (f"{url}?{encodedData}", None)
        elif method == "POST":
            return (url, encodedData)
        else:
            raise ValueError(f"Impossible HTTP method: {method}")

    def __str__(self) -> str:
        return self._selfStr

    @property
    def requestObj(self) -> HTTPRequest:
        return self._requestObj


RequestExecutor = Callable[ ["RedditMessenger", RequestWrapper, OptRedditTokens, OptInt, int, X],
        Coroutine[Any, Any, Tuple[bool, RequestWrapper, Response]] ]
RE = RequestExecutor

def rateLimitRequest(requestExecutor:RE) -> RE:
    async def wrapper(messenger:"RedditMessenger", request:RequestWrapper, tokens:OptRedditTokens, okErrorCode:OptInt, attemptsCount:int,
            parentX:X) -> Tuple[bool, RequestWrapper, Response]:
        x = parentX(network=False, lock=True)
        logger.debug(f"Entry into the rate limit function", extra=x)
        if messenger.locksQueue:
            logger.debug("There are some locks in queue, attempting to retrieve the lock at the head of the queue...", extra=x)
            lock = messenger.locksQueue.pop()
            logger.debug("Lock retrieved from the queue; now waiting for it to open before proceeding further", extra=x)
            await lock.wait()
            logger.debug("Our lock has opened, we can proceed", extra=x)
        else:
            logger.debug("No locks in queue, we do not have to wait", extra=x)
        logger.debug(f"Appending a new lock to the tail of the queue and schedulings its opening in {config.api.rateLimit} seconds", extra=x)
        newLock = asyncio.Event()
        messenger.locksQueue.appendleft(newLock)
        asyncio.get_event_loop().call_later(config.api.rateLimit, messenger.unlock, newLock, x)
        logger.debug("Executing the request now", extra=x)
        responseTuple = await requestExecutor(messenger, request, tokens, okErrorCode, attemptsCount, parentX)
        logger.debug("Request executed", extra=x)
        return responseTuple
    return wrapper


RetryExecutor = Callable[ ["RedditMessenger", RequestWrapper, OptRedditTokens, OptInt, X], Awaitable[Response] ]

def retryRequest(requestExecutor:RE) -> RetryExecutor:
    async def wrapper(messenger:"RedditMessenger", request:RequestWrapper, tokens:OptRedditTokens, okErrorCode:OptInt, x:X) -> Response:
        attemptsCount = 0
        while attemptsCount < config.api.maxRequestAttempts:
            finished, request, response = await requestExecutor(messenger, request, tokens, okErrorCode, attemptsCount, x)
            attemptsCount += 1
            if finished:
                return response
            elif attemptsCount < config.api.maxRequestAttempts:
                break
                #logger.debug(f"Now waiting {config.api.waitBeforeRequestRetry} seconds before retrying.", extra=x)
                #await asyncio.sleep(config.api.waitBeforeRequestRetry)
        logger.debug(f"Too many attempts, cancelling the request, returning the last retrieved error", extra=x)
        return response
    return wrapper

V = TypeVar("V")
ModdedSubreddit = TypedDict("ModdedSubreddit", {"name":str, "permissions":StrFrozenSet})
Revision = TypedDict("Revision", {"page":str, "reason":str, "revision_id":str, "timestamp":datetime, "revision_hidden":bool})
MsgLike = TypedDict("MsgLike", {"kind":str, "name":str, "author":OptStr, "body":str, "created_utc":float, "subject":str, "dest":str, "first_message_name":OptStr})
MsgLikes = List[MsgLike]

class RedditMessenger:

    def __init__(self) -> None:
        super().__init__()
        self._rclient = AsyncHTTPClient()
        self.locksQueue:Deque[asyncio.Event]  = deque()
        self._sslCtx = ssl.create_default_context()

    @staticmethod
    def unlock(lock:asyncio.Event, x:X) -> None:
        logger.debug("About to OPEN the lock to allow the next request to be executed", extra=x)
        lock.set()
        logger.debug("The lock has been OPENED, the next request can now be executed", extra=x)

    @retryRequest
    @rateLimitRequest
    async def _executeRequest(self, request:RequestWrapper, tokens:OptRedditTokens, okErrorCode:OptInt, attemptsCount:int,
            x:X) -> Tuple[bool, RequestWrapper, Response]:
        try:
            logger.debug("Launching the request itself, awaiting its response...", extra=x)
            response = await self._rclient.fetch(request.requestObj)
            logger.debug("Response received!", extra=x)
        except HTTPClientError as error:
            logger.debug(f"HTTPClientError with code {error.code} occurred at attempt no {attemptsCount}: {error}", extra=x)
            if okErrorCode is not None and error.code == okErrorCode:
                logger.debug(f"Error code {error.code} is accepted for this request: returning the response", extra=x)
                return (True, request, error)
            elif tokens is not None and error.code == config.api.unauthorizedCode and attemptsCount == 0: #Access token may have expired: it must be refreshed if it's the case
                logger.debug(f"Since the error code is {error.code}, trying to ensure tokens are valid for the next attempt.", extra=x)
                try:
                    await tokens.ensure(ox=x)
                except Exception as tokens_error:
                    logger.debug(f"Failed to ensure the tokens for the next attempt, returning the tokens error", extra=x)
                    return (True, request, tokens_error)
                else:
                    logger.debug("Tokens were ensured for the next attempt.", extra=x)
                    request = request.rebuildWithNewTokens(tokens, x)
                    return (False, request, error)
            else:
                logger.debug(f"HTTP error occurred at attempt no {attemptsCount}: {error}", extra=x)
                return (False, request, error)
        except Exception as error:
            logger.debug(f"Non-HTTP error occurred at attempt no {attemptsCount}: {error}", extra=x)
            return (False, request, error)
        else:
            logger.debug(f"Request successfully finished executing at attempt no {attemptsCount} with code {response.code}", extra=x)
            return (True, request, response)

    def _logHeaders(self, source:NetworkResponse, x:X) -> None:
        if isinstance(source, HTTPClientError):
            if source.response is None:
                return
            response = source.response
        else:
            response = source
        if "X-Ratelimit-Remaining" in response.headers:
            logger.debug(f"X-Ratelimit-Remaining: {response.headers['X-Ratelimit-Remaining']}", extra=x)


    @overload
    async def _request(self, x:X, url:str, method:HTTPMethod, answerType:AnswerTypeJSON, tokens:OptRedditTokens,
            okErrorCode:int, data:OptDict = None) -> Union[HTTPClientError, Tuple[StrMapping, HTTPResponse]]: ...

    @overload
    async def _request(self, x:X, url:str, method:HTTPMethod, answerType:AnswerTypeJSON, tokens:OptRedditTokens,
            okErrorCode:None = None, data:OptDict = None) -> Tuple[StrMapping, HTTPResponse]: ...

    @overload
    async def _request(self, x:X, url:str, method:HTTPMethod, answerType:AnswerTypeOK, tokens:OptRedditTokens,
            okErrorCode:int, data:OptDict = None) -> NetworkResponse: ...

    @overload
    async def _request(self, x:X,url:str, method:HTTPMethod, answerType:AnswerTypeOK, tokens:OptRedditTokens,
            okErrorCode:None = None, data:OptDict = None) -> HTTPResponse: ...

    async def _request(self, x:X,url:str, method:HTTPMethod, answerType:AnswerType, tokens:OptRedditTokens,
            okErrorCode:OptInt = None, data:OptDict = None) -> Union[NetworkResponse, HTTPResponse, HTTPClientError, Tuple[StrMapping, HTTPResponse]]:
        if x.get("network", False) is not True:
            x = x(network=True)
        if tokens is not None:
            await tokens.ensure(ox=x)
        request = RequestWrapper(url, method, answerType, self._sslCtx, data, tokens, x)
        logger.debug(f"REQUEST INFO:\n{request}", extra=x)
        response = await self._executeRequest(request, tokens, okErrorCode, x)
        if not isinstance(response, HTTPClientError) and not isinstance(response, HTTPResponse):
            raise NetworkError(f"Request failure with a non-HTTP error", response, x=x)
        elif isinstance(response, HTTPClientError) and (okErrorCode is None or okErrorCode != response.code):
            self._logHeaders(response, x)
            raise NetworkError(f"Request failure with a HTTP error", response, x=x)
        elif isinstance(response, HTTPClientError):
            bodyStr = getBodyStr(response)
            self._logHeaders(response, x)
            logger.info(f"Got error {response.code} (body: {bodyStr}), which is accepted, so returning it", exc_info=response, extra=x)
            return response
        elif answerType == "ok": 
            self._logHeaders(response, x)
            logger.info(f"Request succeeded with code {response.code} {response.reason}, returning the HTTP response", extra=x)
            return response
        elif answerType == "json":
            self._logHeaders(response, x)
            try:
                responseBody = json_decode(response.body.decode("utf-8"))
            except Exception as error:
                raise NetworkError(f"Failed to decode the JSON body of the response", response, x) from error
            else:
                if not isinstance(responseBody, Mapping):
                    raise ResponseError(f"the decoded response was not a Mapping as expected", response, x=x)
                if "error" in responseBody:
                    redditReason = responseBody["error"]
                    if redditReason == "invalid_grant":
                        raise ResponseError(f"the supplied code for the OAuth process is wrong or has expired (reason: {redditReason})", response, x)
                    else:
                        raise ResponseError(f"there was an error field in the decoded JSON body ({redditReason})", response, x)
                mapping = cast(StrMapping, responseBody)
                logger.info(f"Request succeeded with code {response.code} {response.reason}, returning the decoded mapping", extra=x)
                logger.debug(f"Decoded mapping: {mapping}", extra=x)
                return (mapping, response)
        else:
            raise NetworkError(f"Impossible case: answerType {answerType}, response type {type(response)}", response, x)

    def _extractValue(self, key:str, expectedType:Type[V], data:StrMapping, response:HTTPResponse, x:X) -> V:
        if key not in data:
            raise ResponseError(f"the <{key}> key was not in the obtained mapping ({data})", response, x)
        value = data[key]
        if not isinstance(value, expectedType):
            raise ResponseError(f"the value of the <{key}> key was not a {expectedType} as expected but a {type(value)}", response, x)
        return value

    async def retrieve_reddit_tokens(self, login_finish_url:str, code:str, x:OX = None) -> Tuple[str, str, float]:
        x = ensureNetworkX(x)
        params = {"grant_type": "authorization_code", "code":code, "redirect_uri":login_finish_url}
        data, response = await self._request(x, config.api.urls.tokensRetrieve, "POST", "json", tokens=None, data=params)
        accessToken = self._extractValue("access_token", str, data, response, x)
        refreshToken = self._extractValue("refresh_token", str, data, response, x)
        expiresIn = self._extractValue("expires_in", int, data, response, x)
        return (accessToken, refreshToken, expiresIn)

    async def refreshRedditTokens(self, refreshToken:str, x:OX = None) -> Tuple[str, OptStr, float]:
        x = ensureNetworkX(x)
        params = {"grant_type":"refresh_token", "refresh_token":refreshToken}
        data, response = await self._request(x, config.api.urls.tokensRefresh, "POST", "json", tokens=None, data=params)
        accessToken = self._extractValue("access_token", str, data, response, x)
        expiresIn = self._extractValue("expires_in", int, data, response, x)
        refreshTokenOpt:OptStr = None
        if "refresh_token" in data:
            refreshTokenOpt = self._extractValue("refresh_token", str, data, response, x)
        return (accessToken, refreshTokenOpt, expiresIn)

    async def revokeRedditTokens(self, token:str, tokenType:TokenType, x:OX = None) -> None:
        x = ensureNetworkX(x)
        params = {"token":token, "token_type_hint":tokenType}
        await self._request(x, config.api.urls.tokensRevoke, "POST", "ok", tokens=None, data=params)

    async def getAuthenticatedUsername(self, redditTokens:RedditTokens, x:OX = None) -> str:
        x = ensureNetworkX(x)
        data, response = await self._request(x, config.api.urls.getIdentity, "GET", "json", redditTokens)
        name = self._extractValue("name", str, data, response, x)
        return name

    async def getRedditUsername(self, username:str, redditTokens:RedditTokens[BotRole], x:OX = None) -> OptStr:
        x = ensureNetworkX(x)
        completeUrl = config.api.urls.getUserAbout.format(quote(username))
        networkResponse = await self._request(x, completeUrl, "GET", "json", tokens=redditTokens, okErrorCode=404)
        if isinstance(networkResponse, HTTPClientError):
            return None
        data, response  = networkResponse
        innerData = self._extractValue("data", dict, data, response, x)
        name = self._extractValue("name", str, innerData, response, x)
        return name

    async def get_real_subreddit_name(self, subreddit_name:str, redditTokens:RedditTokens, x:OX = None) -> str:
        x = ensureNetworkX(x)
        url = config.api.urls.getSubredditAbout.format(quote(subreddit_name))
        data, response = await self._request(x, url, "GET", "json", tokens=redditTokens)
        if data.get("kind", None) != "t5":
            raise ResponseError(f"The decoded mapping did not have the 't5' subreddit kind 't5' ({data})",response, x)
        innerData = self._extractValue("data", dict, data, response, x)
        name = self._extractValue("display_name", str, innerData, response, x)
        return name

    async def get_subreddit_mods(self, subreddit_name:str, redditTokens:RedditTokens, x:OX = None) -> Tuple[str, ...]:
        x = ensureNetworkX(x)
        data, response = await self._request(x, config.api.urls.get_subreddit_mods.format(quote(subreddit_name)), "GET", "json", tokens=redditTokens)
        innerData = self._extractValue("data", dict, data, response, x)
        mods = self._extractValue("children", list, innerData, response, x)
        #return tuple(validateMod(i, mod, response, x) for i,mod in enumerate(mods))
        return tuple(self._extractValue("name", str, mod, response, x) for i,mod in enumerate(mods))

    async def get_modded_subreddits(self, redditTokens:RedditTokens, x:OX = None) -> Mapping[str, ModdedSubreddit]:
        def invalidPerm(permission:object, response:HTTPResponse, x:X) -> NoReturn:
            raise ResponseError(f"the {permission} was not a string", response, x)

        def validateSubreddit(item:object, response:HTTPResponse, x:X) -> ModdedSubreddit:
            if not isinstance(item, Mapping):
                raise ResponseError(f"the {item} subreddit object was not a mapping", response, x)
            _data = self._extractValue("data", dict, cast(StrMapping, item), response, x)
            subreddit_name = self._extractValue("display_name", str, _data, response, x)
            permissionsUntyped = self._extractValue("mod_permissions", list, _data, response, x)
            permissions = frozenset( permission if isinstance(permission, str) else invalidPerm(permission, response, x)
                    for permission in permissionsUntyped )
            return ModdedSubreddit(name=subreddit_name, permissions=permissions)

        x = ensureNetworkX(x)
        data, response = await self._request(x, config.api.urls.get_modded_subreddits, "GET", "json", tokens=redditTokens, data={"limit":100})
        innerData = self._extractValue("data", dict, data, response, x)
        subreddits = self._extractValue("children", list, innerData, response, x)
        moddedSubreddits = {moddedSubreddit["name"]:moddedSubreddit for moddedSubreddit
                in (validateSubreddit(subreddit, response, x) for subreddit in subreddits)}
        return moddedSubreddits

    async def accept_wiki_mod_invite(self, subreddit_name:str, redditTokens:RedditTokens, x:OX = None) -> bool:
        x = ensureNetworkX(x)
        urlFull = config.api.urls.acceptModInvite.format(quote(subreddit_name))
        data, response = await self._request(x, urlFull, "POST", "json", tokens=redditTokens, data={"api_type":"json"})
        innerData = self._extractValue("json", dict, data, response, x)
        errors = self._extractValue("errors", list, innerData, response, x)
        if errors:
            logger.error(f"Failed to accept a mod invite to {subreddit_name} for the following reason: {errors}", extra=x)
            return False
        logger.info(f"Successfully accepted a mod invite to {subreddit_name}", extra=x)
        return True

    async def writeWikiPage(self, redditTokens:RedditTokens, page_url:WikiUrl[Any, Any, str, Any], 
            pageContents:str, prevID:OptStr, reason:str, x:OX = None) -> None:
        x = ensureNetworkX(x)
        params = {"page":str(page_url.from_no_subreddit()), "content":pageContents, "reason":reason}
        if prevID is not None:
            params["previous"] = prevID
        urlFull = config.api.urls.wikiWrite.format(quote(page_url.subreddit))
        response = await self._request(x, urlFull, "POST", "ok", data=params, tokens=redditTokens)

    async def getLatestWikiPageRevision(self, redditTokens:RedditTokens,
            page_url:WikiUrl[Any, Any, str, Any], x:OX = None) -> Optional[Revision]:
        def validateRevision(data:object, response:HTTPResponse, x:X) -> Revision:
            if not isinstance(data, Mapping):
                raise ResponseError(f"the obtained revision ({data}) was not a mapping", response, x)
            page = self._extractValue("page", str, data, response, x)
            reason = self._extractValue("reason", str, data, response, x)
            revision_id = self._extractValue("id", str, data, response, x)
            timestamp = self._extractValue("timestamp", int, data, response, x)
            revision_hidden = self._extractValue("revision_hidden", bool, data, response, x)
            return Revision(page=page, reason=reason, revision_id=revision_id, timestamp=datetime.fromtimestamp(timestamp), revision_hidden=revision_hidden)
        x = ensureNetworkX(x)
        params = {"limit":1}
        requestUri = config.api.urls.wikiGetRevisions.format(quote(page_url.subreddit), quote(str(page_url.from_no_subreddit())))
        result = await self._request(x, requestUri, "GET", "json", data=params, tokens=redditTokens, okErrorCode=404)
        if isinstance(result, HTTPClientError):
            return None
        data, response = result
        innerData = self._extractValue("data", dict, data, response, x)
        children = self._extractValue("children", list, innerData, response, x)
        if not children:
            return None
        revision = validateRevision(children[0], response, x)
        return revision

    async def getWikiPagePermissions(self, redditTokens:RedditTokens, page_url:WikiUrl[Any, Any, str, Any],
            x:OX = None) -> Tuple[int, bool]:
        x = ensureNetworkX(x)
        requestUri = config.api.urls.wikiPermissions.format(quote(page_url.subreddit), quote(str(page_url.from_no_subreddit())))
        data, response = await self._request(x, requestUri, "GET", "json", tokens=redditTokens)
        innerData = self._extractValue("data", dict, data, response, x)
        permlevel = self._extractValue("permlevel", int, innerData, response, x)
        listed = self._extractValue("listed", bool, innerData, response, x)
        return (permlevel, listed)

    async def setWikiPagePermissions(self, redditTokens:RedditTokens,
            page_url:WikiUrl[Any, Any, str, Any], permlevel:int, listed:bool, x:OX = None) -> None:
        x = ensureNetworkX(x)
        requestUri = config.api.urls.wikiPermissions.format(quote(page_url.subreddit), quote(str(page_url.from_no_subreddit())))
        params = {"listed":listed, "permlevel":permlevel}
        response = await self._request(x, requestUri, "POST", "ok", data=params, tokens=redditTokens)

    async def toggleWikiRevisionVisibility(self, redditTokens:RedditTokens, page_url:WikiUrl[Any, Any, str, Any],
            revID:str, x:OX = None) -> None:
        x = ensureNetworkX(x)
        requestUri = config.api.urls.wikiToggleVisibility.format(quote(page_url.subreddit))
        params = {"page":str(page_url.from_no_subreddit()), "revision":revID}
        response = await self._request(x, requestUri, "POST", "ok", data=params, tokens=redditTokens)

    async def fetchPMs(self, redditTokens:RedditTokens, location:str, direction:Direction, count:int, lastSeenId:OptStr = None, x:OX = None) -> Tuple[OptStr, MsgLikes]:
        def validateMsg(child:StrMapping, response:HTTPResponse, x:X) -> MsgLike:
            #TODO: test that these args are all present on non-PMs as well
            kind = self._extractValue("kind", str, child, response, x)
            coreData:StrMapping = self._extractValue("data", dict, child, response, x)
            author:OptStr = None
            if coreData.get("author", None) is not None:
                author = self._extractValue("author", str, coreData, response, x)
            name = self._extractValue("name", str, coreData, response, x)
            body = self._extractValue("body", str, coreData, response, x)
            subject = self._extractValue("subject", str, coreData, response, x)
            dest = self._extractValue("dest", str, coreData, response, x)
            created_utc = self._extractValue("created_utc", float, coreData, response, x)
            first_message_name:OptStr = None
            if "first_message_name" in coreData and coreData["first_message_name"] is not None:
                first_message_name = self._extractValue("first_message_name", str, coreData, response, x)
            return MsgLike(kind=kind, name=name, author=author, body=body, created_utc=created_utc, subject=subject, first_message_name=first_message_name, dest=dest)

        x = ensureNetworkX(x)
        params = {"show":"all", "mark":False, "count":count,  "limit":config.api.requestFetchLimit}
        if lastSeenId is not None:
            params[direction] = lastSeenId
        data, response = await self._request(x, config.api.urls.getMessages[location], "GET", "json", data=params, tokens=redditTokens)
        innerData = self._extractValue("data", dict, data, response, x)
        lastSeenIdNew:OptStr = innerData[direction]#self._extractValue(direction, AnyStr, innerData, response, x) 
        children = self._extractValue("children", list, innerData, response, x)
        # NO FILTERING OF NO PMs here! Otherwise, this will disturb our pagination/fetching logic, which relies on checking whether there are 0 retrieved PMs or not
        msgs = [validateMsg(child, response, x) for child in children]
        return (lastSeenIdNew, msgs)


