# coding:utf-8
import asyncio, itertools, logging, math, secrets
from datetime import datetime
from pymongo.errors import DuplicateKeyError
from typing import (List, Literal, Optional, Sequence, Tuple, Union, TYPE_CHECKING)
from urllib.parse import urlencode
from .constants.configuration import config
from .constants.exceptions import LogicError
from .constants.types import (Bot, BotRole, FFalse, FFFalse, Host, OptStr,
                              Player, PlayerRole, Role, StrSet, TTrue, TTTrue)
from .database.exceptions import DocumentNotFoundError, NoMatchError
from .database.singleton import DBSingleton, handle_db
from .database.games_collection import (PathKeyDocument, PathKeyDocumentCondition, PathKeySingleDocument,
        PathKeyCoupleDocument, PathKeyDefaultDocument)
from .database.results import InsertOneResult, UpdateResultPositive
from .redditmessenger import NetworkError, RedditMessenger
from .structs.path_keys import (PKType, PKTypeSingle, PKTypeCouple, 
                                PKTypeDefault, PKTSingle, PKTCouple, PKTDefault)
from .structs.tokens import RedditTokens
from .structs.week_time import WeekTime
from .structs.game import Game
if TYPE_CHECKING:
    from .web.base_handler import HandlerInfo

lo = logging.getLogger(config.loggerName)


class Connector(RedditMessenger, DBSingleton):
    def __init__(self) -> None:
        super().__init__()
        config.publish_connector(self)

    async def get_role_for_user(self, username:str) -> Optional[Role]:
        if username == config.botUsername:
            return Bot
        role = await handle_db(self.get_user_role_in_game(username), DocumentNotFoundError, None)
        if role is not None:
            return role
        elif await self.is_any_host(username):
            return Host
        return None

    async def disconnect_player(self, gamecode:OptStr, username:str) -> bool:
        """Disconnects a player: revokes the Reddit tokens if possible,
        then erases them in DB as well."""
        tokens = RedditTokens[PlayerRole].player(gamecode, username)
        try:
            await tokens.void()
        except DocumentNotFoundError:
            lo.warning(f"Could not find any tokens for {username} [filtering on game: {gamecode}]")
            return False
        except Exception as error:
            raise error
        else:
            lo.info(f"Tokens for player {username} successfully revoked on the Reddit side!")
            anything_erased = await handle_db(self.erase_reddit_tokens_for_player(gamecode, username), NoMatchError, None)
            if anything_erased:
                lo.info(f"Tokens for player {username} in game {gamecode} successfully erased in DB too!")
            else:
                lo.warning(f"But after erasing, found no tokens to erase in DB [player {username}-game {gamecode}]")
            return True


    def generateRandomString(self, length:int = 10, url:bool = True) -> str:
        if url:
            return secrets.token_urlsafe(length)
        else:
            return secrets.token_hex(math.ceil(length/2))

    def get_reddit_authorization_url(self, handler:"HandlerInfo", state:str, scope:StrSet) -> str:
        params = {"client_id":config.reddit.clientID,
                "response_type": "code",
                "state": state,
                "redirect_uri": handler.get_login_finish_url(),
                "duration": "permanent",
                "scope": " ".join(scope)}
        return config.api.urls.authorize + "?" + urlencode(params)

    async def create_auth_request(self, handler:"HandlerInfo", username:str, role:Role) -> str:
        """Creates an auth request in DB with a random <state> for <username>. Any exiting request is deleted.
        Then redirects the user towards the Reddit authorization URL with the correct scope/state according to their role.
        A user can be both HOST in a game and PLAYER in another, so we only take into account the highest <redditRole>:
        in terms of scope, PLAYER is higher than HOST (because of PMs) so that's what the app must ask Reddit for a dual HOST/PLAYER."""
        result:Optional[InsertOneResult[TTrue]] = None
        while result is None:
            state = self.generateRandomString(url=True)
            result = await handle_db(self.add_auth_request(state, username, role), DuplicateKeyError, None)
            if result is None:
                lo.error(f"Failed to add an authentication request because of a duplicate state ({state})")
        url = self.get_reddit_authorization_url(handler, state, config.api.scopes[role])
        lo.info(f"Generated the following authorization URL for {username}: {url}")
        return url

    async def erase_reddit_tokens_for_user_depending_on_role(self, username:str) -> Optional[UpdateResultPositive]:
        role = await self.get_role_for_user(username)
        if role == Player:
            return await handle_db(self.erase_reddit_tokens_for_player(None, username), NoMatchError, None)
        elif role == Host:
            return await handle_db(self.erase_reddit_tokens_for_host(username), NoMatchError, None)
        elif role == Bot:
            return None #TODO: something
        else:
            raise LogicError(role, Role)

    def is_valid_schedule(self, start:datetime, end:datetime) -> bool:
        if start.day == end.day and start.hour == end.hour and start.minute == end.minute:
            return False
        else:
            return True
  
    async def is_lambda_subreddit_mod(self, subreddit_name_initial:str,
            redditTokens:RedditTokens[BotRole]) -> OptStr:
        modded_subreddits = await self.get_modded_subreddits(redditTokens)
        lo.debug(f"Subreddits modded by u/{config.botUsername}: {modded_subreddits}")
        for subreddit_name in modded_subreddits:
            if subreddit_name_initial.lower() != subreddit_name.lower():
                continue
            elif "wiki" in modded_subreddits[subreddit_name]["permissions"]:
                return subreddit_name
            elif "all" in modded_subreddits[subreddit_name]["permissions"]:
                return subreddit_name
        return None

    async def is_game_ready_to_run(self, gamecode:str, host_username:str) -> Union[Tuple[TTrue, None], Tuple[FFalse, str]]:
        subreddit_name = await self.get_subreddit_name(gamecode)
        if await self.count_connected_active_players(gamecode) < 1:
            return (FFFalse, "You need to have at least one connected and active player to start running the game.")
        ok, error_string = await self.ensure_game_subreddit_ready(subreddit_name, host_username)
        if not ok:
            return (FFFalse, error_string)
        schedule = await self.get_schedule(gamecode)
        if not self.is_valid_schedule(**schedule):
            return (FFFalse, "You must define a weekly schedule for your game")
        return (TTTrue, None)

    async def ensure_bot_is_mod(self, subreddit_name_initial:str, tokens:Optional[RedditTokens[BotRole]] = None) -> OptStr:
        if tokens is None:
            tokens = RedditTokens[BotRole].bot()
        for attempt_number in (0,1):
            subreddit_name = await self.is_lambda_subreddit_mod(subreddit_name_initial, tokens)
            if subreddit_name is not None and subreddit_name.lower() == subreddit_name_initial.lower():
                return subreddit_name
            elif subreddit_name is not None:
                raise ValueError(f"Unexpectedly different subreddit name: {subreddit_name} is not {subreddit_name_initial}")
            elif attempt_number == 0 and not await self.accept_wiki_mod_invite(subreddit_name_initial, tokens):
                return None
        return None

    async def ensure_game_subreddit_ready(self, subreddit_name_initial:str, host_username:str) -> Tuple[bool, str]:
        tokens = RedditTokens[BotRole].bot()
        awaitable = self.ensure_bot_is_mod(subreddit_name_initial, tokens)
        subreddit_name = await handle_db(awaitable, NetworkError, None)
        if subreddit_name is None:
            return (False, f"You must invite u/{config.botUsername} to be a wiki moderator of r/{subreddit_name_initial}.")
        lo.info(f"Real subreddit name: {subreddit_name}")
        subreddit_mods = await self.get_subreddit_mods(subreddit_name, tokens)
        if host_username not in subreddit_mods:
            return (False, f"u/{host_username} must be a mod of r/{subreddit_name}.")
        return (True, subreddit_name)

    async def create_game(self, game_title:str, host_username:str, gamecode_display:str, subreddit_name:str) -> str:
        default_key = self._generate_path_key_default()
        default_week_time = WeekTime(1,0,1)
        for attempt_number in itertools.count(start=1):
            gamecode = gamecode_display if attempt_number == 1 else f"{gamecode_display}{attempt_number}"
            lo.debug(f"Attempt number {attempt_number} to insert a game. Gamecode: {gamecode}. Display code: {gamecode_display}")
            awaitable = self.insert_game_document(gamecode, gamecode_display, game_title,
                        host_username, subreddit_name, default_week_time, default_key)
            result = await handle_db(awaitable, DuplicateKeyError, None)
            if result is not None:
                break
        return gamecode

    def sort_usernames(self, first_username:str, second_username:str) -> Tuple[str, str]:
        user1, user2 = sorted((first_username, second_username), key=str.lower)
        return (user1, user2)

    def _generate_path_key_default(self) -> PathKeyDefaultDocument:
        return {"key":self.generateRandomString(), "users":PKTDefault}

    def _generate_path_key_single(self, username:str) -> PathKeySingleDocument:
        return {"key":self.generateRandomString(), "users":PKTSingle(username)}

    def generate_path_key_couple(self, username1:str, username2:str) -> PathKeyCoupleDocument:
        user1, user2 = self.sort_usernames(username1, username2)
        return {"key":self.generateRandomString(), "users":PKTCouple(user1, user2)}

    async def generate_path_keys_for_user_in_game(self, gamecode:str, username:str) -> Sequence[PathKeyDocument]:
        game_docs = await self.get_game_documents(gamecode)
        game = Game(game_docs[0])
        path_keys:List[PathKeyDocument] = []
        player_names = await self.get_player_names(gamecode)
        for player_name in player_names:
            user1, user2 = self.sort_usernames(username, player_name)
            pkt_couple = PKTCouple(user1, user2)
            if pkt_couple not in game.path_keys:
                couple_key = self.generate_path_key_couple(username, player_name)
                path_keys.append(couple_key)
        if PKTSingle(username) not in game.path_keys:
            single_key = self._generate_path_key_single(username)
            path_keys.append(single_key)
        return path_keys

    
