document.addEventListener("DOMContentLoaded", function(){
  const sanitizer = /[\s\-\:]/g;
  const main = document.getElementById("logsArea");
  var cycles = new Map();

  var streamUrl = document.getElementById("stream-script").getAttribute("data-stream-url");
  var ws = new WebSocket(streamUrl);

  ws.onopen = function() {
    var token = document.getElementById("stream-script").getAttribute("data-token");
    var game = document.getElementById("stream-script").getAttribute("data-game");
    console.log("test", token, game);
    ws.send(JSON.stringify({"token":token, "game":game}));
  };

  function onClick(cycleName){
    var div = document.getElementById(getId(cycleName));
    if(div.classList.contains("hidden")){
      div.className = "logs visible";
    }else{
      div.className = "logs hidden";
    }
  }

  function getId(cycleName){
    return "logs-" + cycleName;
  }

  ws.onmessage = function (evt) {
    var data = JSON.parse(evt.data)
    var messageEl = document.createElement("div");
    messageEl.className = "logMessage";
    var textNode = document.createTextNode(data.message);
    messageEl.appendChild(textNode);
    if(data.event == "close")
      messageEl.className += " endLogMessage";
    if(!cycles.has(data.cn)){
      var cycleName = data.cn.replace(sanitizer, "");
      cycles.set(data.cn, cycleName);
      var div = document.createElement("div");
      div.id = getId(cycleName);
      div.className = "logs hidden";
      var title = document.createElement("h3");
      title.appendChild(document.createTextNode(data.cn));
      div.appendChild(title);
      main.prepend(div);
      document.getElementById(getId(cycleName)).addEventListener("click", (event) => {
        onClick(cycleName);
      });
    }
    cycleName = cycles.get(data.cn);
    var curDiv = document.getElementById(getId(cycleName));
    curDiv.appendChild(messageEl);
  };
});
