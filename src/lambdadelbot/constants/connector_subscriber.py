#coding: utf-8
from __future__ import annotations
from typing import Type, TYPE_CHECKING, Union
from abc import ABC
if TYPE_CHECKING:
    from ..connector import Connector
else:
    Connector = ""
__all__ = ["ConnectorSubscriber", "Subscriber"]

class ConnectorSubscriber(ABC):
    r:Connector

    @classmethod
    def set_connector_on_class(cls, r:Connector) -> None:
        cls.r = r

    def set_connector_on_instance(self, r:Connector) -> None:
        self.r = r


Subscriber = Union[ConnectorSubscriber, Type[ConnectorSubscriber]]
