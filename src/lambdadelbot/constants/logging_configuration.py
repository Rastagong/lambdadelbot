# -*- coding:utf-8 -*-
from __future__ import annotations
import logging, traceback, re, asyncio
from collections import deque
from datetime import datetime
from typing import Any, Deque, Dict, Hashable, Literal, Mapping, Optional, Set, Tuple, TYPE_CHECKING, Union
if TYPE_CHECKING:
    from ..database.logs_collection import LogSDocumentTotal
else:
    LogSDocumentTotal = ""
from .connector_subscriber import ConnectorSubscriber
from .types import *
__all__ = ["ensure_x", "LoggingExtra", "X", "OX", "configure_logging", "MongoHandler"]


class LoggingExtra(Dict[str, object]):

    def __init__(self, gamecode:Optional[str] = None, globalCycleName:Optional[str] = None, gameCycleName:Optional[str] = None,
            lock:Optional[bool] = None, network:Optional[bool] = None) -> None:
        super().__init__()
        if gamecode is not None:
            self["gamecode"] = gamecode
        if globalCycleName is not None:
            self["globalCycleName"] = globalCycleName
        if gameCycleName is not None:
            self["gameCycleName"] = gameCycleName
        if lock is not None:
            self[LockEvent] = lock
        if network is not None:
            self[NetworkEvent] = network

    def __call__(self, gamecode:Optional[str] = None, globalCycleName:Optional[str] = None, gameCycleName:Optional[str] = None,
            lock:Optional[bool] = None, network:Optional[bool] = None) -> "LoggingExtra":
        if gamecode is None and "gamecode" in self:
            gamecode = self["gamecode"] #type: ignore
        if globalCycleName is None and "globalCycleName" in self:
            globalCycleName = self["globalCycleName"]#type: ignore
        if gameCycleName is None and "gameCycleName" in self:
            gameCycleName = self["gameCycleName"]#type: ignore
        if lock is None and LockEvent in self:
            lock = self[LockEvent]#type: ignore
        if network is None and NetworkEvent in self:
            network = self[NetworkEvent]#type: ignore
        return self.__class__(gamecode=gamecode, globalCycleName=globalCycleName, gameCycleName=gameCycleName, lock=lock, network=network)

X = LoggingExtra
OX = Optional[LoggingExtra]

def ensure_x(ox:OX) -> X:
    if ox is None:
        return X()
    return ox


class MongoHandler(logging.Handler, ConnectorSubscriber):
    """Logging queue handler which writes records to MongoDB.
    Sending is done asynchronously via the Connector."""

    def __init__(self) -> None:
        super().__init__()
        self.queue:Deque[LogSDocumentTotal] = deque()
        self.open = True

    def flush(self) -> None:
        loop = asyncio.new_event_loop()
        task = loop.create_task(self.send())
        loop.run_until_complete(task)

    def close(self) -> None:
        """Marks the current handler as closed and asynchronously waits until all records in queue have been sent."""
        super().close()
        self.open = False
        self.flush()

    def emit(self, logRecord:logging.LogRecord) -> None:
        try:
            self.format(logRecord)
            self.enqueue(self.prepare(logRecord))
        except:
            self.handleError(logRecord)

    def prepare(self, record:logging.LogRecord) -> LogSDocumentTotal:
        """Prepares a <record> to be sent to MongoDB.
        We can convienently use the underlying <__dict__> attribute provided by the LogRecord class.
        However, we also need to stringify the event flags if there are any, so Motor can insert them without any error."""
        logDocument:LogSDocumentTotal = {
                "asctime":datetime.strptime(record.asctime, FORMATTER_DATEFMT),
                "exc_text":record.__dict__.get("exc_text", None),
                "filename":record.filename,
                "funcName":record.funcName,
                "levelname":record.levelname,
                "levelno":record.levelno,
                "lineno":record.lineno,
                "message":record.message,
                "module":record.module,
                "pathname":record.pathname,
                "process":record.process,
                "processName":record.processName,
                "gamecode":record.__dict__.get("gamecode", None),
                "globalCycleName":record.__dict__.get("globalCycleName", None),
                "gameCycleName":record.__dict__.get("gameCycleName", None),
                "lock":record.__dict__.get(LockEvent, None),
                "network":record.__dict__.get(NetworkEvent, None),
                } #TODO: keep only the fields we are actually interested in, remove the bloat
        """if record["exc_info"] is not None:
            tracebackList = traceback.format_exception(*record["exc_info"])
            record["exc_info"] += "\n".join(tracebackList)"""
        return logDocument

    def enqueue(self, document:LogSDocumentTotal) -> None:
        """Attempts to append a <record> to the queue
        The record is appended only if the handler is still <open>
        If it is appended, an attempt to send records is performed via <attemptSend>"""
        if not self.open:
            return
        self.queue.append(document)

    async def loop(self) -> None:
        while self.open:
            while self.queue:
                await self.send()
            await asyncio.sleep(1)

    async def send(self) -> None:
        records = tuple(self.queue)
        self.queue.clear()
        try:
            await self.r.write_logs(*records)
        except Exception as error:
            print(f"Logging error while sending to MongoDB: {error}")


class EventsFilter(logging.Filter):
    def __init__(self, minLevel:int, eventIncludes:LogEventFrozenSet, eventExcludes:LogEventFrozenSet) -> None:
        super().__init__()
        self.minLevel, self.eventIncludes, self.eventExcludes = minLevel, eventIncludes, eventExcludes

    def filter(self, record:logging.LogRecord) -> bool:
        """Filters a <record>.
        Returns <True> if the record is kept, <False> if it is filtered out.
        The master rule: the inclusion/exclusion of certain events trumps the examination of the record level.
        We check the presence of event flags first to determine inclusion.
        When there are no events at all or the events do not determine anything, we check the record level."""

        # First of all, the case where the <record> has some event flags
        # Maybe some of these events will trigger an inclusion, or on the contrary an exclusion
        # To perform a presence check, we check if the flag is also in our list of events to include or exclude
        if record.__dict__.get(LockEvent, False) is True:
            if LockEvent in self.eventExcludes:
                return False
            elif LockEvent in self.eventIncludes:
                return True
        if record.__dict__.get(NetworkEvent, False) is True:
            if NetworkEvent in self.eventExcludes:
                return False
            elif NetworkEvent in self.eventIncludes:
                return True

        # The secondary condition of inclusion
        # When the record cannot be excluded/included because of its events, we look at its level
        # The level at which the record was logged must be superior or equal to the <minLevel> allowed in this session
        recordLevel:int = record.__dict__["levelno"] 
        return recordLevel >= self.minLevel


FORMATTER_DATEFMT = "%Y-%m-%d %H:%M:%S"
FORMATTER_STYLE:Literal["{"] = "{"

def getBasicFormatter() -> logging.Formatter:
    fmt = "{asctime} - {levelname} - {message}"
    return logging.Formatter(fmt, FORMATTER_DATEFMT, FORMATTER_STYLE)

def getDetailsFormatter() -> logging.Formatter:
    #fmt = "{asctime} - {levelname} - {operation} - {game} - {funcName}@{module}.py (line {lineno})\n{message}\n"
    fmt = "{asctime} - {levelname} - {funcName}@{module}.py (line {lineno})\n{message}\n"
    return logging.Formatter(fmt, FORMATTER_DATEFMT, FORMATTER_STYLE)

def displaySettings(loggerName:str, productionMode:bool, minLevelStr:str, consoleFormatStr:str,
        eventIncludes:LogEventFrozenSet, eventExcludes:LogEventFrozenSet) -> None:
    """This function displays constants at the start of the program."""
    def boolStr(setting:bool) -> str: return "ON" if setting else "OFF"
    def displayEvents(events:LogEventFrozenSet) -> str: return ", ".join(sorted(tuple(events))) if events else "NONE"
    logger = logging.getLogger(loggerName)
    display = f"[PRODUCTION MODE: {boolStr(productionMode)}] "
    display += f"[LOGGING LEVEL: {minLevelStr}] [CONSOLE LOGS FORMAT: {consoleFormatStr}] "
    display += f"[INCLUDED EVENTS: {displayEvents(eventIncludes)}] [EXCLUDED EVENTS: {displayEvents(eventExcludes)}]"
    logger.info(display)


FORMATTER_BASIC_NAME = "basic"
FORMATTER_DETAILS_NAME = "details"
GLOBAL_MIN_LEVEL = logging.NOTSET + 1 #The minimum logging level that is possible at all

def configure_logging(productionMode:bool, loggerName:str, minLevelStr:str, consoleFormatStr:str,
        eventIncludes:LogEventFrozenSet, eventExcludes:LogEventFrozenSet) -> MongoHandler:
    logger = logging.getLogger(loggerName)
    logger.setLevel(GLOBAL_MIN_LEVEL) #Minimal possible level: filtering is performed at the handler level

    # Events filter creation
    minLevel = int(minLevelStr) if re.fullmatch(r"\d+", minLevelStr) else  getattr(logging, minLevelStr)
    if (incompatibleEvents := eventIncludes.intersection(eventExcludes)):
        raise ValueError(f"Events cannot be both included and excluded ({incompatibleEvents})")
    eventsFilter = EventsFilter(minLevel, eventIncludes, eventExcludes)

    # Determines the formatter 
    formatter:logging.Formatter
    if consoleFormatStr == FORMATTER_BASIC_NAME:
        formatter = getBasicFormatter()
    elif consoleFormatStr == FORMATTER_DETAILS_NAME:
        formatter = getDetailsFormatter()
    else:
        raise ValueError(f"Unexpected string for the console logs format ({consoleFormatStr}): expected either '{FORMATTER_BASIC_NAME}' or '{FORMATTER_DETAILS_NAME}'")

    # Adds the console handler
    consoleHandler  = logging.StreamHandler()
    consoleHandler.setFormatter(formatter)
    consoleHandler.addFilter(eventsFilter)
    logger.addHandler(consoleHandler)

    # Adds the MongoDB Handler
    mongoHandler = MongoHandler()
    mongoHandler.setFormatter(formatter)
    logger.addHandler(mongoHandler)

    # Returns the name of the logger to use and the mongoHandler
    displaySettings(loggerName, productionMode, minLevelStr, consoleFormatStr, eventIncludes, eventExcludes)
    return mongoHandler


