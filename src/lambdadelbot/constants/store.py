# -*- coding: utf-8 -*-
import os, json
from abc import ABC, abstractmethod
from pathlib import Path
from typing import Any, cast, Mapping, Union
from .types import (LogEventFrozenSet, StrMapping, StrTuple, LockEvent, NetworkEvent, LogEventFrozenSet, LogEvent)


__all__ = ["EnvironmentStore", "JSONFileStore"]

class _Store(ABC):
    """Abstract base class for all stores
    An store instance provides methods facilitating the access, presence check and type check of environment variables."""

    @abstractmethod
    def __init__(self) -> None:
        pass

    @abstractmethod
    def __contains__(self, key:str) -> bool:
        pass

    @abstractmethod
    def _get(self, key:str) -> Any:
        pass

    @abstractmethod
    def string(self, key:str) -> str:
        pass

    @abstractmethod
    def boolean(self, key:str) -> bool:
        pass

    @abstractmethod
    def integer(self, key:str) -> int:
        pass

    @abstractmethod
    def float(self, key:str) -> float:
        pass


class EnvironmentStore(_Store):
    """ EnvironmentStore
    A Store fetching values from the environment (as environment variables)."""

    def __init__(self, prefix:str = "") -> None:
        self.prefix = prefix
        super().__init__()

    def _get(self, key:str) -> str:
        """Return the value of the <key> environment variable.
        Raises a KeyError if the key cannot be found."""
        try:
            value = os.environ[self.prefix + key]
        except KeyError as error:
            raise KeyError(f"The <{self.prefix + key}> environment variable does not exist") from error
        else:
            return value

    def __contains__(self, key:str) -> bool:
        return key in os.environ

    def string(self, key:str) -> str:
        """Returns the value of the <key> variable as a string.
        Environment variables are stored as strings, therefore this method returns the value directly."""
        return self._get(key)

    def boolean(self, key:str) -> bool:
        """Returns the value of the <key> env variable as a <bool>.
        If the value of the environment variable in storage is neither 'True' nor 'False', raises a ValueError."""
        value = self._get(key)
        if value == "True":
            return True
        elif value == "False":
            return False
        else:
            raise ValueError(f"The {self.prefix+key} environment variable is not a valid boolean string: expected 'True' or 'False', got '{value}'")

    def integer(self, key:str) -> int:
        """Returns the value of the <key> env variable as an <int>.
        If the value cannot be converted to an <int>, raises a ValueError."""
        value = self._get(key)
        try:
            int_value = int(value)
            if str(int_value) != value:
                raise ValueError
        except ValueError:
            raise ValueError(f"The {self.prefix+key} environment variable is not a valid integer: got '{value}'")
        else:
            return int_value

    def float(self, key:str) -> float:
        """Returns the value of the <key> env variable as a <float>.
        If the value cannot be converted to a <float>, raises a ValueError."""
        value = self._get(key)
        try:
            float_value = float(value)
        except ValueError:
            raise ValueError(f"The {self.prefix+key} environment variable is not a valid floating number: got '{value}'")
        else:
            return float_value

    @staticmethod
    def validate_log_event(key:str, i:int, item:Any) -> LogEvent:
        if item == LockEvent:
            return LockEvent
        elif item == NetworkEvent:
            return NetworkEvent
        raise ValueError(f"Expected item no {i} of the <{key}> config variable to be either a lock or a network event, but it was not: got {item}")
    
    def log_events_frozen_set(self, key:str) -> LogEventFrozenSet:
        value = self._get(key)
        try:
            list_value = json.loads(value)
            if not isinstance(list_value, list):
                raise ValueError
        except:
            raise ValueError(f"Expected the value of the <{self.prefix+key}> config variable to be a list of strings, but it was not: got {value}")
        else:
            return frozenset(self.validate_log_event(self.prefix+key, i, item) for (i,item) in enumerate(list_value))


class JSONFileStore(_Store):
    """A store fetching values from a JSON config file"""

    def __init__(self, filepath:Union[Path, str]) -> None:
        self.path = filepath if isinstance(filepath, Path) else Path(filepath)
        try:
            jsonTxt = self.path.read_text("utf-8")
            mapping = json.loads(jsonTxt)
            if not isinstance(mapping, Mapping):
                raise ValueError("it is not a mapping")
            self.mapping = cast(StrMapping, mapping)
        except Exception as error:
            raise ValueError(f"Could not decode the <{self.path}> JSON config file: {error}") from error

    def _get(self, key:str) -> Any:
        """Return the value of the <key> environment variable.
        Raises a KeyError if the key cannot be found."""
        try:
            value = self.mapping[key]
        except KeyError:
            raise KeyError(f"The <{key}> config variable does not exist in the <{self.path}> JSON file")
        else:
            return value

    def __contains__(self, key:str) -> bool:
        return key in self.mapping

    def string(self, key:str) -> str:
        value = self._get(key)
        if not isinstance(value, str):
            raise ValueError(f"Expected the value of the <{key}> config variable to be a string, but it was not: got {value}")
        return value

    def boolean(self, key:str) -> bool:
        value = self._get(key)
        if not isinstance(value, bool):
            raise ValueError(f"Expected the value of the <{key}> config variable to be a boolean, but it was not: got {value}")
        return value

    def integer(self, key:str) -> int:
        value = self._get(key)
        if not isinstance(value, int):
            raise ValueError(f"Expected the value of the <{key}> config variable to be an integer, but it was not: got {value}")
        return value

    def float(self, key:str) -> float:
        value = self._get(key)
        if isinstance(value, float):
            return value
        elif isinstance(value, int):
            return float(value)
        raise ValueError(f"Expected the value of the <{key}> config variable to be a float (or at least an integer), but it was not: got {value}")

    @staticmethod
    def validate_log_event(key:str, i:int, item:Any) -> LogEvent:
        if item == LockEvent:
            return LockEvent
        elif item == NetworkEvent:
            return NetworkEvent
        raise ValueError(f"Expected item no {i} of the <{key}> config variable to be either a lock or a network event, but it was not: got {item}")
    
    def log_events_frozen_set(self, key:str) -> LogEventFrozenSet:
        value = self._get(key)
        if not isinstance(value, list):
            raise ValueError(f"Expected the value of the <{key}> config variable to be a list of strings, but it was not: got {value}")
        return frozenset(self.validate_log_event(key, i, item) for (i,item) in enumerate(value))


