# -*- coding:utf-8 -*-
from __future__ import annotations
import re
from datetime import datetime
from typing import (Any, ClassVar, Dict, FrozenSet, List, Literal, Mapping,
        NewType, Optional, Pattern, Sequence, Set, Tuple, Type, TypedDict, Union)
__all__ = ["AccessToken", "AnswerType", "AnswerTypeOK", "AnswerTypeJSON",
        "Bot", "BotRole",
        "DirectionBefore", "DirectionAfter", "Direction",
        "FFalse",  "FFFalse",
        "GameRoutineParams", "GAMECODE_NOCASE_CHECKER", "GameState", "GameStateName", "GameStateValue",
        "GameStatesSequence", "GameStatesTuple",
        "Host", "HostRole", "HTTPMethod",
        "LockEvent", "LockEventType","LogEvent", "LogEventFrozenSet",
        "makeGameRoutineParams",
        "NetworkEvent", "NetworkEventType",
        "OptBytes", "OptDict", "OptInt", "OptFloat", "OptStr", "OptDatetime", "OptException", "OptBool", "OptGameState",
        "Player", "PlayerRole",
        "RefreshToken", "Role",
        "StrDict", "StrFrozenSet", "StrList", "StrMapping", "StrSeq", "StrSet", "StrStrMapping", "StrTuple",
        "TokenType", "TokenTypeAccess", "TokenTypeRefresh", "TTrue", "TTTrue"]

StrMapping = Mapping[str, Any]
StrStrMapping = Mapping[str, str]
StrFrozenSet = FrozenSet[str]
StrSet = Set[str]
StrSeq = Sequence[str]
StrDict = Dict[str, Any]
StrTuple = Tuple[str, ...]
StrList = List[str]

OptDict = Optional[Dict]
OptInt = Optional[int]
OptFloat = Optional[float]
OptStr = Optional[str]
OptBool = Optional[bool]
OptDatetime = Optional[datetime]
OptException = Optional[Exception]
OptBytes = Optional[bytes]
OptGameState = Optional["GameState"]
GameStatesSequence = Sequence["GameState"]
GameStatesTuple = tuple["GameState", ...]

LockEventType = Literal["lock"]
LockEvent:LockEventType = "lock"
NetworkEventType = Literal["network"]
NetworkEvent:NetworkEventType = "network"
LogEvent = Union[LockEventType, NetworkEventType]
LogEventFrozenSet = FrozenSet[LogEvent]

HTTPMethod = Union[Literal["GET"], Literal["POST"]]

TokenTypeAccess = Literal["access_token"]
TokenTypeRefresh = Literal["refresh_token"]
AccessToken:TokenTypeAccess = "access_token"
RefreshToken:TokenTypeRefresh = "refresh_token"
TokenType = Union[TokenTypeAccess, TokenTypeRefresh]

AnswerTypeOK = Literal["ok"]
AnswerTypeJSON = Literal["json"]
AnswerType = Union[AnswerTypeOK, AnswerTypeJSON]

FFalse = Literal[False]
FFFalse:FFalse = False
TTrue = Literal[True]
TTTrue:TTrue = True

DirectionBefore = Literal["before"]
DirectionAfter = Literal["after"]
Direction = Union[DirectionBefore, DirectionAfter]

PlayerRole = Literal["player"]
Player:PlayerRole = "player"
HostRole = Literal["host"]
Host:HostRole = "host"
BotRole = Literal["bot"]
Bot:BotRole = "bot"
Role = Union[PlayerRole, HostRole, BotRole]

Paused = Literal["paused"]
Running = Literal["running"]
AlmostFinished = Literal["almost finished"]
Finished = Literal["finished"]

GameStateName = Union[Paused, Running, AlmostFinished, Finished]
GameStateValue = Union[Literal[10], Literal[20], Literal[30], Literal[40]]


class GameState:
    values_by_names:ClassVar[Mapping[GameStateName, GameStateValue]] = {"paused":10, "running":20, "almost finished":30, "finished":40}
    names_by_values:ClassVar[Mapping[GameStateValue, GameStateName]]
    paused:ClassVar[GameState]
    running:ClassVar[GameState]
    almostFinished:ClassVar[GameState]
    finished:ClassVar[GameState]

    def __init__(self, name_or_value:Union[GameStateName,GameStateValue]) -> None:
        try:
            if isinstance(name_or_value, str):
                self.name = name_or_value
                self.value = self.values_by_names[self.name]
            elif isinstance(name_or_value, int):
                self.value = name_or_value
                self.name = self.names_by_values[self.value]
            else:
                raise KeyError
        except KeyError:
            raise KeyError(f"Unexpected name or value to initialize a game state: {name_or_value}, vs {self.values_by_names}/{self.names_by_values}")
        else:
            pass

    @classmethod
    def initialize(cls) -> None:
        cls.names_by_values = {cls.values_by_names[name]:name for name in cls.values_by_names}
        cls.paused = cls("paused") 
        cls.running = cls("running")
        cls.almostFinished = cls("almost finished")
        cls.finished = cls("finished")

    def __str__(self) -> str:
        return f"GameState<{self.name} = {self.value}>"

    def __repr__(self) -> str:
        return f"GameState('{self.name}')"

    def __ge__(self, other:object) -> bool:
        if isinstance(other, self.__class__):
            return self.value >= other.value
        return NotImplemented

    def __gt__(self, other:object) -> bool:
        if isinstance(other, self.__class__):
            return self.value > other.value
        return NotImplemented

    def __le__(self, other:object) -> bool:
        if isinstance(other, self.__class__):
            return self.value <= other.value
        return NotImplemented

    def __lt__(self, other:object) -> bool:
        if isinstance(other, self.__class__):
            return self.value < other.value
        return NotImplemented

    def __eq__(self, other:object) -> bool:
        if isinstance(other, self.__class__):
            return self.value == other.value
        return NotImplemented

    def __hash__(self) -> int:
        return hash(self.name)
GameState.initialize()


"""EVENT_DESCRIPTIONS = {
    Events.LOCK:    "Lock events are generated when the HTTP client attempts to acquire/free the lock which enforces a request rate limit",#"This application performs HTTP requests asynchronously: instead of waiting for an answer from a remote server to resume work, the application uses the waiting time to work on _other_ tasks in the meantime. For instance, another request could be concurrently started."
    Events.NETWORK: "Network events are generated when HTTP requests are sent or received (including during retries)"
}"""
#DEFAULT_LEVELNAMES =  ['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL']


def GAMECODE_NOCASE_CHECKER(code:str) -> Pattern: #Allows case-insensitive search for game codes in the <games> table
    return re.compile(r"^{0}$".format(re.escape(code)), re.IGNORECASE)


class GameRoutineParams(TypedDict):
    force_fetch:bool
    wikiwrite:bool

def makeGameRoutineParams(force_fetch:bool = False, wikiwrite:bool = False) -> GameRoutineParams:
    return {"force_fetch":force_fetch, "wikiwrite":wikiwrite}

