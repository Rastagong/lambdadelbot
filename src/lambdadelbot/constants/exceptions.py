#coding:utf-8
from __future__ import annotations
import logging
from typing import (cast, Generic, NoReturn, Optional, Type, TypeVar, TYPE_CHECKING, Union)
from .configuration import config
from .logging_configuration import OX
from .types import OptException, TTrue, FFalse
__all__ = ["LambdaError", "LogicError", "WrongAccountError", "NoConfigError", "raise_logic_error"]
logger = logging.getLogger(config.loggerName)


class LambdaError(Exception):
    def __init__(self, msg:str, exc_info:OptException = None, x:OX = None) -> None:
        if exc_info is None and x is None:
            logger.error(msg)
        elif exc_info is None:
            logger.error(msg, extra=x)
        elif x is None:
            logger.error(msg, exc_info=exc_info)
        else:
            logger.error(msg, exc_info=exc_info, extra=x)


class NoConfigError(LambdaError):
    pass


class LogicError(LambdaError): 
    def __init__(self, actual_value:object, *expected:Union[Type|object], x:OX = None) -> None:
        self.actual_value, self.expected = actual_value, expected
        self.actual_type = type(actual_value)
        msg  = f"Expected a value to be among the following types/values ({self.expected}), "
        msg += f"got {self.actual_value} of type {self.actual_type} instead. "
        msg += f"This error would never have been raised if the pattern matching/if-else chain "
        msg +=  "had been exhaustive, as guaranteed by mypy: "
        msg +=  "this may signal that at least one case is not correctly handled by mypy in that chain"
        super().__init__(msg, x=x)

def raise_logic_error(actual_value:NoReturn, *expected:Union[Type|object]) -> NoReturn:
    raise LogicError(cast(object, actual_value), *expected)


class WrongAccountError(LambdaError):
    def __init__(self, actual_username:str, expected_username:str, x:OX = None) -> None:
        self.actual_username, self.expected_username = actual_username, expected_username
        super().__init__(f"User logged into the wrong account, expected {expected_username}, got {actual_username}", x=x)



