# coding: utf-8
from __future__ import annotations
import logging, re, tomllib
from pathlib import Path
from datetime import datetime
from importlib import metadata
from dotenv import load_dotenv
from typing import List, Mapping, Optional, Tuple, TYPE_CHECKING
from .store import EnvironmentStore
from .connector_subscriber import ConnectorSubscriber, Subscriber
from .logging_configuration import configure_logging, LoggingExtra, MongoHandler
from .types import Bot, Host, Role, Player
if TYPE_CHECKING:
    from ..connector import Connector
else:
    Connector = ""
__all__ = ["Config", "config"]


class Stores:
    def __init__(self) -> None:
        self.general = EnvironmentStore()
        if "SECRETS_WEB_FILEPATH" in self.general:
            load_dotenv(self.general.string("SECRETS_WEB_FILEPATH"))
        if "SECRETS_DB_FILEPATH" in self.general:
            load_dotenv(self.general.string("SECRETS_DB_FILEPATH"))
        if "CONFIG_WEB_FILEPATH" in self.general:
            load_dotenv(self.general.string("CONFIG_WEB_FILEPATH"))
        self.env = EnvironmentStore("LAMBDADELBOT_")
        self.web, self.db, self.config = self.env, self.env, self.env

class NetworkConfig:
    def __init__(self, stores:Stores) -> None:
        self.containerPort = stores.config.integer("CONTAINER_PORT")
        self.publicPort = stores.config.integer("PUBLIC_PORT")

class DatabaseConfig:
    def __init__(self, stores:Stores) -> None:
        self.dbName = stores.db.string("DATABASE_NAME")
        self.username = stores.db.string("DATABASE_USERNAME")
        self.password = stores.db.string("DATABASE_PASSWORD")
        self.dbHostname = stores.config.string("DATABASE_HOSTNAME")
        self.authDBName = self.dbName 
        self.connectionURI = f"mongodb://{self.username}:{self.password}@{self.dbHostname}:27017/{self.authDBName}"

class RedditConfig:
    def __init__(self, stores:Stores, productionMode:bool, version:str, adminUsername:str) -> None:
        self.uaSecret = stores.web.string("USER_AGENT_SECRET")
        self.ua = f"web:lambdadelbot-{self.uaSecret}:v{version} (by /u/{adminUsername})"
        self.clientID = stores.web.string("REDDIT_CLIENT_ID")
        self.clientSecret = stores.web.string("REDDIT_CLIENT_SECRET")

class AppConfig:
    def __init__(self, stores:Stores) -> None:
        self.loggerName = "lambdadelbot"
        self.maxUsers = 100
        self.maxMessages = 1000000
        self.maxGames = 100
        self.gamecode_sanitizer = re.compile(r"([A-Za-z0-9\_]{0,12})")
        self.subredditNameSanitizer = re.compile(r"^(/?r?/?)")
        self.defaultPathKey = "reddit"
        self.dbLockTimeout = 3.0
        self.globalRoutineIntervalMinutes = stores.config.integer("GLOBAL_ROUTINE_INTERVAL_IN_MINUTES")
        self.globalRoutineIntervalSeconds:int = self.globalRoutineIntervalMinutes * 60
        self.cycleNameFormat = "%Y-%m-%d %H:%M"
        self.prettifyUsernameRegex = r"^{}[_|-]"

class WebAppConfig:
    def __init__(self, stores:Stores, publicPort:int) -> None:
        self.url = stores.config.string("PUBLIC_URL")
        if stores.config.boolean("PUBLIC_URL_INCLUDE_PORT"):
            self.url += f":{publicPort}"
        self.cookieSecret = stores.web.string("COOKIE_SECRET")
        self.auth_requests_expiration_delay = 60.0
        self.login_start_default_username = "reddit"
        self.allow_paused_games_manual_game_cycles = stores.config.boolean("ALLOW_PAUSED_GAMES_MANUAL_GAME_CYCLES")
        self.markdown_sources_dir = "lambdadelbot/content"
        self.markdown_extensions = ("attr_list", "toc", "footnotes", "smarty")
        self.markdown_extension_configs = {"toc": {"baselevel":2, "title":"Table of contents"}}
        self.menu_pages = {"home":"Homepage", "about":"About", "changelog":"Changelog"}
        self.version_date_format = "%d %B %Y"

class APIUrlsConfig:
    def __init__(self, stores:Stores) -> None:
        self.authorize = "https://www.reddit.com/api/v1/authorize"
        self.tokensRetrieve = "https://www.reddit.com/api/v1/access_token"
        self.tokensRefresh = "https://www.reddit.com/api/v1/access_token"
        self.tokensRevoke = "https://www.reddit.com/api/v1/revoke_token"
        self.getIdentity = "https://oauth.reddit.com/api/v1/me"
        self.getUserAbout  = "https://oauth.reddit.com/user/{0}/about"
        self.getSubredditAbout = "https://oauth.reddit.com/r/{0}/about"
        self.get_subreddit_mods = "https://oauth.reddit.com/r/{0}/about/moderators"
        self.get_modded_subreddits = "https://oauth.reddit.com/subreddits/mine/moderator"
        self.acceptModInvite = "https://oauth.reddit.com/r/{0}/api/accept_moderator_invite"
        self.wikiWrite = "https://oauth.reddit.com/r/{0}/api/wiki/edit"
        self.wikiGetRevisions = "https://oauth.reddit.com/r/{0}/wiki/revisions/{1}"
        self.wikiPermissions = "https://oauth.reddit.com/r/{0}/wiki/settings/{1}"
        self.wikiToggleVisibility = "https://oauth.reddit.com/r/{0}/api/wiki/hide"
        self.getMessages = {"inbox": "https://oauth.reddit.com/message/inbox", "sent":"https://oauth.reddit.com/message/sent"}

class APIConfig:
    def __init__(self, stores:Stores) -> None:
        self.urls = APIUrlsConfig(stores)
        self.rateLimit = 2.5 #In seconds
        self.maxRequestAttempts = 3
        self.waitBeforeRequestRetry = 5
        self.unauthorizedCode = 401
        self.requestFetchLimit = 100
        self.approxMaxWikiSize = 508000
        self.sanitizeUrlComponent = re.compile(r"[/\\. ]")
        self.scopes = {Bot: set(["identity", "wikiedit", "wikiread", "read", "modself", "modwiki", "mysubreddits"]),
                Host:set(["identity"]), Player:set(["identity", "privatemessages"])}
        self.tokensExpirationMargin = 5 * 60
        self.tokensExpirationMarginWiki = 20 * 60


class Config:

    def __init__(self) -> None:
        stores = Stores()
        #
        self.botUsername = stores.config.string("BOT_USERNAME")
        self.adminUsername = stores.config.string("ADMIN_USERNAME")
        self.version, self.version_date = self.get_pkg_metadata()
        self.debugMode = "PYTHONDEVMODE" in stores.general and stores.general.integer("PYTHONDEVMODE") == 1
        self.productionMode = not self.debugMode
        #
        self.network = NetworkConfig(stores)
        self.database = DatabaseConfig(stores)
        self.reddit = RedditConfig(stores, self.productionMode, self.version, self.adminUsername)
        self.app = AppConfig(stores)
        self.webapp = WebAppConfig(stores, self.network.publicPort)
        self.api = APIConfig(stores)
        #
        self.mongoHandler = self.configure_logging(stores)
        self.loggerName = self.app.loggerName #TODO: put it in a specific location
        #
        self.connector_subscribers:List[Subscriber] = []
        self.subscribe_to_connector(self.mongoHandler)
        #

    def get_pkg_metadata(self) -> tuple[str, datetime]:
        try:
            data_from_importlib = metadata.metadata("lambdadelbot")
            version_str:str = data_from_importlib["Version"]
            description:str = data_from_importlib["Summary"]
        except:
            data_from_pyproject = tomllib.loads(Path("pyproject.toml").read_text())["tool"]["poetry"]
            version_str = data_from_pyproject["version"]
            description = data_from_pyproject["description"]
        finally:
            release_date_extractor = re.compile(r"Released (\d+)-(\d+)-(\d+)")
            match = release_date_extractor.search(description)
            if not match:
                raise ValueError(f"Could not extract release date from the following package description: {description}")
            year, month, day = match.groups()
            release_date = datetime(year=int(year), month=int(month), day=int(day))
            return (version_str, release_date)

    def configure_logging(self, stores:Stores) -> MongoHandler:
        min_level = stores.config.string("LOGGING_MIN_LEVEL")
        console_format = stores.config.string("LOGGING_CONSOLE_FORMAT")
        included_ev = stores.config.log_events_frozen_set("LOGGING_EVENT_INCLUDES")
        excluded_ev = stores.config.log_events_frozen_set("LOGGING_EVENT_EXCLUDES")
        return configure_logging(self.productionMode, self.app.loggerName,
                min_level, console_format, included_ev, excluded_ev)

    def subscribe_to_connector(self, subscriber:Subscriber) -> None:
        self.connector_subscribers.append(subscriber)

    def publish_connector(self, r:Connector) -> None:
        for subscriber in self.connector_subscribers:
            if isinstance(subscriber, ConnectorSubscriber):
                subscriber.set_connector_on_instance(r)
            elif issubclass(subscriber, ConnectorSubscriber):
                subscriber.set_connector_on_class(r)
            else:
                raise ValueError(f"Unidentified connector subscriber: {subscriber}")
        self.connector_subscribers.clear()

config = Config()

