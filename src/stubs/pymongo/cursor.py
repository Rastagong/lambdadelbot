#coding:utf-8
from typing import ClassVar, NewType
__all__ = ["CursorType", "CT"]

CT = NewType("CT", int)

class CursorType:
    NON_TAILABLE:ClassVar[CT] = CT(0)
    TAILABLE:ClassVar[CT] = CT(2)
    TAILABLE_AWAIT:ClassVar[CT] = CT(NON_TAILABLE | 32)
    EXHAUST:ClassVar[CT] = CT(64)
