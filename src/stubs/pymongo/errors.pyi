#coding:utf-8
from typing import Any, Mapping, Optional

class PyMongoError(Exception):
    def has_error_label(self, label:str) -> bool: ...

class OperationFailure(PyMongoError):
    code:Optional[int]
    details:Mapping[str, Any]

class DuplicateKeyError(OperationFailure):
    pass

class BulkWriteError(OperationFailure):
    pass
