from typing import NewType
from . import cursor
from . import errors
__all__ = ["ASCENDING", "DESCENDING", "SortDirection", "cursor", "errors"]

SortDirection = NewType("SortDirection", int)
ASCENDING = SortDirection(1)
DESCENDING = SortDirection(-1)
