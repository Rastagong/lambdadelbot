#coding: utf-8
from typing import Generic, Literal, Mapping, TypedDict, TypeVar, Union

I = Union[Literal[0], Literal[1]]
K = TypeVar("K", bound=str)
V = TypeVar("V", bound=Mapping[str, object])
Map = Mapping[K, I]
UpdateKeys = Union[Literal["$set"], Literal["$unset"], Literal["$where"]]
Update = Mapping[UpdateKeys, V]

class Collection(Generic[K,V]):
    def do(self, mymap:Map[K]) -> None: pass
    def foo(self, update:Update[V]) -> None: pass



GameKeys = Union[Literal["gamecode"], Literal["title"], Literal["update_count"]]
GameDict = TypedDict("GameDict", {"gamecode":str, "title":str, "update_count":int})
class Games(Collection[GameKeys, GameDict]): pass

games = Games()
games.do({"gamecode":1})
games.foo({"$set":{"gamecode":"ToF", "title":"Tower of Fun", "update_count":3}})


