#coding: utf-8
from typing import Any, Dict, Generic, List, Literal, Mapping, Optional, overload, Type, TypeVar, Union

OptStr = Optional[str]


class CollectionKeys:
    def getType(self) -> Type[Any]:
        pass

C = TypeVar("C", bound=CollectionKeys)#, covariant=True) 


K = TypeVar("K", Literal["gamecode"], Literal["update_count"], Literal["subreddit"])#, covariant=True)
class GameKeys(Generic[K], CollectionKeys):
    key:K
    m:Mapping[K, Type[Any]] = {"gamecode":str, "update_count":int, "subreddit":str}
    
    @overload
    def getType(self:GameKeys[Literal["gamecode"]]) -> Type[str]: ...

    @overload
    def getType(self:GameKeys[Literal["update_count"]]) -> Type[int]: ...

    @overload
    def getType(self:GameKeys[Literal["subreddit"]]) -> Type[OptStr]: ...

    def getType(self) -> Type[Any]: pass


g = GameKeys[Literal["gamecode"]]()
reveal_type(g.getType())


N = TypeVar("N", bound=str)
class Document(Generic[C]):
    collection:C
    
    def getType(self:Document[GameKeys[K]]) -> None:
        t = self.collection.getType()
        if self.collection.key == "gamecode":
            reveal_type(t)
        return None

def typecheck(collection:GameKeys[K], dico:Dict[str, Any]) -> None:
    for key in dico:
        if key == "gamecode" or key == "update_count" or key == "subreddit":
            t = collection[key].getType()
