#coding:utf-8
from typing import *
V = TypeVar("V")
N = TypeVar("N")
C = TypeVar("C", bound=Callable)
C1 = TypeVar("C1", bound=Callable[..., V])
C2 = TypeVar("C2", bound=Callable[..., Type[V]])

class Decorate(Generic[V,C]):
    def __init__(self, func:C) -> None:
        self._f:C = func

    def __call__(self, *args:Any, null:N, **kwargs:Any) -> Type[V]:
        value:V = self._f(*args, null, **kwargs)
        return type(value)

D = Decorate[int, Callable[[int,str,N], int]]
@D
def do(arg1:int, arg2:str, null:N) -> int:
    return 5

S = NewType("S", str)
T = TypeVar("T", Optional[str], str)
def dof(t:T = None) -> T:
    if t is None:
        reveal_type(t)
        return "a"
    else:
        return "a"

v = dof("a")
reveal_type(v)
