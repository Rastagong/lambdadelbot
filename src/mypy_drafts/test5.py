#coding:utf-8
from typing import Any

class Foo:
    def __init__(self, **kwargs:Any) -> None:
        self.do(**kwargs)

    def do(self, lock:bool, name:str) -> None:
        return

dico = {"lock":True, "name":"Alison"} 
d = Foo(**dico)
