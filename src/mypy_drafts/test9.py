#coding:utf-8
from typing import *

TTrue = Literal[True]
FFalse = Literal[False]

A = TypeVar("A", TTrue, FFalse)


class Result(Generic[A]):

    @overload
    @property
    def modified_count(self:Result[TTrue]) -> int: ...
    @overload
    @property
    def modified_count(self:Result[FFalse]) -> NoReturn: ...
    @property
    def modified_count(self) -> Union[int, NoReturn]: ...


r1 = Result[TTrue]()
r2 = Result[FFalse]()
m1 = r1.modified_count
m2 = r2.modified_count
reveal_type(m1)
reveal_type(m2)
