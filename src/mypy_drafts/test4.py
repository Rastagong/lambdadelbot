#coding:utf-8
from datetime import datetime
from typing import Any, Dict, Protocol, runtime_checkable, Generic, Literal, NewType, Optional, TypeVar, Union

OptStr = Optional[str]
NNone = NewType("NNone", int)

OT = TypeVar("OT")
O = Union[NNone, OT]
N = NNone(0)

@runtime_checkable
class LockDocument(Protocol):
    free:bool
    process:OptStr
    acquiredTime:Optional[datetime]

@runtime_checkable
class GameDocument(Protocol):
    codename:Union[NNone, str]
    title:Union[NNone, str]
    update_count:Union[NNone, int]
    subreddit:Union[NNone, OptStr]
    lock:Union[NNone, LockDocument]

class GameDoc(Dict[str, Any]):
    def __init__(self, codename:O[str] = N, title:O[str] = N, update_count:O[int] = N,
            subreddit:O[OptStr] = N, lock:O[LockDocument] = N, **kwargs:Any) -> None:
        for kwargName in kwargs:
            parentName, childName = kwargName.split(".")
            print(GameDocument.codename)

    def gameDoc(self) -> GameDocument:
        if isinstance(self, GameDocument):
            return self
        raise ValueError

if __name__ == "__main__":
    print(vars(GameDocument))
