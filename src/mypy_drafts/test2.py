# coding:utf-8
from typing import Generic, Optional, overload, Tuple, Type, TypeVar, Union

OptStr = Optional[str]
OptInt = Optional[int]

T1 = TypeVar("T1", None, str)
T2 = TypeVar("T2", None, str)
T3 = TypeVar("T3", None, int)
T = Union[Tuple[None, None, int], Tuple[str, None, None], Tuple[str, str, None]]
TT = TypeVar("TT", Tuple[None, None, int], Tuple[str, None, None], Tuple[str, str, None])

class Base(Generic[T1, T2, T3]):

    def do(self, val1:T1, val2:T2, val3:T3) -> T:
        if val3 is not None:
            if val1 is not None:
                raise ValueError
            if val2 is not None:
                raise ValueError
            reveal_type(val1)
            reveal_type(val2)
            reveal_type(val3)
            vals = (val1,val2,val3)
            reveal_type(vals)
            return vals


def foo(t:TT) -> None:
    if t[0] is not None:
        reveal_type(t)
        class Truc(Generic[T1]):
            pass
        to = type(t[0])
        reveal_type(to)
        truc = Truc[to]()
    pass
