#! /usr/bin/env python3
# coding: utf-8
import asyncio

async def first():
    i = 0
    while i < 10:
        await asyncio.sleep(2)
        i += 1

async def second():
    await asyncio.sleep(4)
    a += 5
    await asyncio.sleep(9)

async def do():
    await asyncio.gather(first(), second(), return_exceptions=True)

if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(do())
