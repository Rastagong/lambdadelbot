#coding:utf-8
from typing import Any, cast, Generic, Optional, Protocol, overload, TypeVar, Union

class Nothing: pass
No = Nothing()
N = TypeVar("N", None, Nothing)
V = TypeVar("V", covariant=True)

class DBCall(Protocol[V]):
    def __call__(self, *args:Any, null:N) -> Union[N,V]: ...
C = TypeVar("C", bound=DBCall)

def decorate(dbcall:C) -> C:
    reveal_type(dbcall)
    def wrapper(*args:Any, null:N) -> Union[N,V]: ...
    return wrapper #type:ignore

OptInt = Optional[int]
def underlying() -> OptInt:
    return 5

@decorate #type:ignore
def db_call(arg1:str, arg2:int, null:N) -> Union[N, int]:
    value:OptInt = underlying()
    if value is None and null is None:
        return value
    elif value is None:
        raise ValueError
    reveal_type(value)
    return value

class Kind(Generic[V]):

    def __init__(self, value:V) -> None:
        pass

    @classmethod
    def new(cls, value:V) -> Kind[V]:
        return cast(int, cls(value)) #type:ignore

a = Kind[int](5)
reveal_type(a)
