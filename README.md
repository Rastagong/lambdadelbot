This is the source code of [**Lambdadelbot**](https://lambda.rastagong.eu), a Reddit bot designed by Rastagong to facilitate playing games on the [r/MyLittleHouseOfFun](https://reddit.com/r/MyLittleHouseOfFun) subreddit.

# Installation
This application is meant to be deployed using Docker, so you will need to [install Docker](https://docs.docker.com/get-docker/) (min version 19.03.0) and [docker-compose](https://docs.docker.com/compose/install/) (latest is best).

Open a terminal and clone the current repository: `git clone git@bitbucket.org:Rastagong/lambdadelbot.git`.  

# Configuration

## Creating a Reddit application
You will need to create your own Reddit application to access Reddit's API. You can do it at [https://www.reddit.com/prefs/apps](https://www.reddit.com/prefs/apps), and there are [a few explanations here](https://github.com/reddit-archive/reddit/wiki/OAuth2#getting-started). Make sure to choose the **web app** application type.

![](docs/README/create_app.png)

There are 3 fields here that will be important later:

* the **client ID** of the Reddit application
* the **client secret** of the Reddit application (don't share it with anyone, it really is secret)
* the **redirection URI** of the application

You will need to edit the redirection URI yourself. You must put the URL where the app will be hosted, and add `/login/finish` at the end. For instance, my deployment of Lambdadelbot is hosted at [https://lambda.rastagong.eu](), so I put `https://lambda.rastagong.eu/login/finish`.

If you're in production, you should have a domain name or a sort of public URL, so this will be easy to fill in.  
If you're only testing Lambdadelbot in development (on your own machine), then the URL will most likely be `http://localhost:8000`, where 8000 is the public port where you choose to host the app (it could be any other valid and free port number). In this case, choose a port number now, you'll need it later to configure the app.

## Creating the Reddit bot account
Create a Reddit account to use as 'bot', e.g the account which will write PMs on the wiki. My own bot is [u/Lambdadelbot](https://www.reddit.com/user/Lambdadelbot).

## .env
Make a copy of the file named `sample.env` that you rename `.env`.

This contains config variables used by docker-compose for the entire application:

* `USER_ID`: put in the UID of your current user (the one who owns the `lambdadelbot` folder and who will launch Docker). This is used to make the permissions of the web application files inside the web container match those of the files on your system.
* `GROUP_ID`: likewise, put in the GID of your user
* `HOST_PORT`: the port where Lambdadelbot will be accessible on the host machine. In production, this may be port 80 (or not if Lambda is running behind a reverse proxy). In development, this is the port number you should have chosen earlier.
* `HOST_DOMAIN`: choose a domain where the port will be published (most likely leave 127.0.0.1)
* `CONTAINER_PORT`: the port to which the HOST\_PORT will be mapped inside the web container
* `PYTHONDEVMODE`: put 1 to enable debug mode, 0 in production
* `WEB_PROCESS_SRC_BIND_MOUNT_TARGET`: in a development context, put `/app` so that 
a volume containing `src` is mounted inside the web container, allowing you to directly edit the app from your host machine, even while the app is running (in production, put a random folder to disable the bind mount)
* `LOGGING_DRIVER`: logging driver of the worker container; leave `json-file` in development, but you may want to use something more robust like `syslog` or `journald` in production to have persistent logs

## config/secrets\_web.env
Make a copy of `config/secrets_web.sample.env` and rename it `config/secrets_web.env`.

This contains secret config values used by the web application:

* `LAMBDADELBOT_COOKIE_SECRET`: a [random value](https://www.browserling.com/tools/random-hex) used to create secure cookies (cookies signed using HMAC).
* `LAMBDADELBOT_REDDIT_CLIENT_ID`: the client ID of the Reddit application you created earlier
* `LAMBDADELBOT_REDDIT_CLIENT_SECRET`: the client secret of the Reddit application you created earlier
* `LAMBDADELBOT_USER_AGENT_SECRET`: a unique string to identify your bot, that will be used inside Lambdadelbot's User-Agent, per Reddit's API guidelines

## config/secrets\_db.env
Make a copy of `config/secrets_db.sample.env` and rename it `config/secrets_db.env`.

This file contains the credentials of the MongoDB database:

* `LAMBDADELBOT_DATABASE_NAME`: the name of the database (you can leave the default value if you want)
* `LAMBDADELBOT_DATABASE_USERNAME`: the name of the database user
* `LAMBDADELBOT_DATABASE_PASSWORD`: the password of the database user. You can leave the default password that's easy to remember in development, but choose a strong password in production

## config/secrets\_db\_root.env
Make a copy of `config/secrets_db_root.sample.env` and rename it `config/secrets_db_root.env`.

This contains a single value, `LAMBDADELBOT_DATABASE_ROOT_PASSWORD`, which is the password of the database root user. Just like for the database user, you can choose anything in development, but put a strong password in production.

## config/config\_web.env
You can edit this file directly, no need to make a copy.

I'll complete this section to give more info about all these settings later on, but for now, the most important settings are the following ones:

* `LAMBDADELBOT_CONTAINER_PORT`: take care to put the same value that you used in `.env` for `CONTAINER_PORT`
* `LAMBDADELBOT_PUBLIC_PORT`: the port on which the app will be publically acessible. In production, this is most likely port 80. In development, this will rather be the `HOST_PORT` that you indicated in `.env` and in the redirection URI on Reddit.
* `LAMBDADELBOT_PUBLIC_URL`: the domain name where the web application will be hosted (without the port). In development, this is most likely `http://localhost`.
* `LAMBDADELBOT_PUBLIC_URL_INCLUDE_PORT`: put `True` if the `LAMBDADELBOT_PUBLIC_PORT` indicated above should be appended at the end of the `LAMBDADELBOT_PUBLIC_URL` to get the full URL of the web application. This is most likely the case in development: if the app is hosted on `localhost`, then you'll need to type `localhost:8000` (if the `HOST_PORT` is 8000) to access the web app, so you should put `True` here. This may be different in production, where the app will more likely be hosted on port 80, which can be safely omitted from the full URL.
* `LAMBDADELBOT_BOT_USERNAME`: the username of the Reddit bot you created.
* `LAMBDADELBOT_ADMIN_USERNAME`: the Reddit username of the admin, e.g your own Reddit username. 
* `LAMBDADELBOT_GLOBAL_ROUTINE_INTERVAL_IN_MINUTES`: the interval between global routines, in minutes (how often a fetch process is launched for running games)

# Configuration without Docker
If you want to install Lambdadelbot without using Docker, that's also possible.

Install MongoDB 4.4 on your system. You may also simply rely on a a cloud provider like Mongo Atlas.  
Either way, you'll have to manyally update all config variables related to the database in the config files mentioned above.

You'll need to have Python 3.9 installed. On Linux, in 2022, it is the default version on many distributions.  
Then to install the project's dependencies, go inside the `src` folder with a terminal. The next steps depend on your preferences:  

The "easy" way if you're fed-up with the Python packaging ecosystem is to just use [virtualenv](https://virtualenv.pypa.io/en/latest/) (or [virtualenvwrapper](https://virtualenvwrapper.readthedocs.io/en/latest/) which can create virtuale environments from anywhere) to be able to isolate the project's dependencies. You may create a virtual environment in a subfolder named `venv` by running `virtualenv venv` from the `src` folder, and then activating by running the `venv/activate` file. Just delete the `venv` folder if you want to delete the environment.  
If you're using virtualenvwrapper instead, from any folder, you may create a virtual envirnment named `lambdadelbot` by running `mkvirtualenv lambdadelbot`. You may activate it by running `workon lambdadelbot` from anywhere, and delete it by running `rmvirtualenv lambdadelbot` from anywhere.  
To actually install the project's dependencies, from the `src` folder, just run `pip install -r requirements.txt`.

The "fancy" way, if you use [Poetry](https://python-poetry.org) to manage your dependencies, is to go into the `src` folder, to run `poetry shell` to open a virtual environment, and then to run `poetry install` to install the dependencies.  
Said like this, it sounds easier than the virtualenv way, but Poetry isn't universally adopted in the Python ecosystem (some people frankly dislike it), and it can be hard to install on some systems.


# Launching the app
1. In the `lambdadelbot` folder, run `sudo docker-compose build` to build the 3 containers
2. Run `sudo docker-compose up` to launch the whole app

Lambdadelbot should be accessible at the public URL you chose.

## Launching the app if you're not using Docker
If you're running without Docker, the web process can be launched (from an activated virtual environment with all dependencies) with `python main.py web`. You also need to launch another worker process, with `python main.py worker`.  
Both processes will need to have access to the config files mentioned above: for this purpose, you need to set 3 environment variables before starting these processes: 

* `CONFIG_WEB_FILEPATH` must contain a path to the `config/config_web.env` file (so most likely "../config/config_web.env)"
* `SECRETS_WEB_FILEPATH` must contain a path to the `config/secrets_web.env` file (so most likely "../config/secrets_web.env)"
* `SECRETS_DB_FILEPATH` must contain a path to the `config/secrets_db.env` file (so most likely "../config/secrets_db.env)"

Of course, please ensure the database is online and reachable too.

## Logging in for the first time
Go to the public URL you chose, and on the right side of the page, you should see a link asking you to **log in using the Reddit bot account**. Log in on Reddit with that bot acocunt, click on the link, and accept the authorisations asked by Lambdadelbot. 

After this, the admin account will be created, and you'll be able to log in with it too. With the admin account, you'll be able to add new games and to set anyone as host.

# Project Design

## File Hierarchy
* `config`: various configuration files of the application (all described in the `Configuration` section below)
* `data`: ignore this, just a .gitignored folder for database dumps
* `discovery`: mostly ignore this, just a separate folder to inspect responses from the Reddit API using a 'manual' HTTP clients
* `docs`: personal documentation files, you can ignore it too
* `provision`: files used to provision the Docker containers
* `src`: source code of Lambdadelbot

### Source Code Files
* `lambdadelbot`: source code of the `lambdadelbot` Python package
* `mypy_drafts`: ignore as well, just quick files to test how mypy (the static type checker I use) works in special cases
* `stubs`: type hints for 3rd-party libraries which do not come with their own type hints (used by mypy)
* `main.py`: Python entrypoint of the `web` (Web application) and `worker` (fetch process) Docker containers
* `pyproject.toml`: lists the Python dependencies of the package, in a format that can be read by [Poetry](https://python-poetry.org), my Python dependency manager. Also contains configuration settings for mypy. In practice, this is not used in production or inside Docker at all: I use Poetry only locally, mostly for static type checking with mypy. In Docker containers, I rely on a more classical `requirements.txt` generated by [pip-tools](https://pypi.org/project/pip-tools/).
* `poetry.lock`: lockfile of the Python dependencies of the project, in the Poetry format
* `requirements.in`: Python dependencies of the project, but in a format that can be read by pip-tools
* `requirements.txt`: lockfile of the Python dependencies of the project, in the most portable and universal format: anyone can install dependencies from a `requirements.txt` easily. This is the file that's actually used by Docker containers to install dependencies. This file is automatically generated from `requirements.in` by pip-tools.
* `requirements_dev.in`: Python dependencies in the pip-tools format, but contains only development packages, e.g mypy and its dependencies
* `requirements_dev.txt`: lockfile of the development dependencies of the project, in a universal and portable format. This file is automatically generated from `requirements_dev.in` by pip-tools.


## Docker containers
The application uses 3 Docker containers:

* The `web` container for the Web application from which you can create & manage games. The source code is in `src/lambdadelbot`
* The `worker` container for the background worker which regularly fetches PMs. This container is built from the same source as the Web application (an argument provided at startup is used to choose whether the container acts as a Web app or as worker process).
* The `database` container contains a MongoDB 4.4 database

## Web Application/Worker Design
The app uses Python 3.9. The Web server and HTTP client is [Tornado](https://www.tornadoweb.org/en/stable/), which makes use of asynchronous I/O to handle concurrent HTTP requests from clients or to Reddit's API.

# Static type checking using mypy
Although Python is not statically typed by design (it relies on [duck typing](https://en.wikipedia.org/wiki/Duck_typing)), the addition of type hints since Python 3.5 makes it possible to use external static type checkers on the codebase, to detect typing issues before runtime. 

I've added type hints to the entire codebase since the version 2 rewrite, and made use of functional programming patterns where possible (immutable variables, pure functions with no side effects, little OOP). This makes Lambdadelbot much more reliable overall; most issues can be detected during static analysis. The static type checker on which Lambdadelbot relies is [mypy](https://mypy.readthedocs.io/en/stable/). 

### From inside the `web` or `worker` containers
If you do not want to install Python (and associated programs to install Lambdadelbot's dependencies) on your host system, the easiest way to run mypy is from directly inside a Docker container of the Web application or worker process.

To do so, run `sudo docker-compose up -d` to launch the Docker stack as usual, then run `sudo docker-compose exec web sh` to get a terminal inside the Web container.

Once you're inside that terminal, run `pip install -r requirements_dev.txt` to install mypy and its dependencies. Then just run `mypy` to perform static type checking.

### From your host system
If you're running the app without Docker, and with Python installed on your system, then you may install mypu directly.  
If you're using Poetry, mypy should have been installed from the start when running `poetry install`.  
If you're using virtualenv or virtualenvwrapper, you'll need to install additional dependencies by running `pip install -r requirements_dev.txt` from the `src` folder.
Then to type check the codebase, you only have to run `python -m mypy` inside the `src` folder.

# License
The source code is licensed under the terms of the [**GNU Affero General Public License**](https://www.gnu.org/licenses/agpl-3.0.en.html). The full legal terms are available in the LICENSE file.

[As explained by the Free Software Foundation](https://www.gnu.org/licenses/why-affero-gpl.en.html), the GNU AGPL license is very similar to the GNU GPL, in that both share [a copyleft clause](https://www.gnu.org/philosophy/pragmatic.en.html) (if you rely on Lambda's code, you own project must also be licensed under (A)GPL), but the AGPL license provides [additional requirements of open sourcing](https://www.gnu.org/licenses/why-affero-gpl.en.html) applying when running licensed code on a server.
