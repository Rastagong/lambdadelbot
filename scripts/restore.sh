#!/bin/bash
set -e
NOW=$(date +"%Y-%m-%d")
DEST="/bitnami/mongodb/dump${NOW}-$RANDOM"
echo "Dest folder of the restored backup will be $DEST"

source  config/secrets_db.env
source  config/secrets_db_root.env     

SOURCE=$1
if [ -z "$SOURCE" ]; then
  echo "No source folder on the host was provided as argument!"
  exit 1
else
  echo "Source folder: $SOURCE"
fi


CONTAINER=$(docker ps --format "{{.Image}} {{.Names}}" | grep "lambdadelbot_database" | sed "s/^lambdadelbot_database //g")
if [ -z "$CONTAINER" ]; then
  echo "Did not find any database container"
  exit 1 
else
  echo "Found a database container named $CONTAINER"
fi

docker cp $SOURCE $CONTAINER:$DEST

# Per https://hub.docker.com/r/bitnami/mongodb, since this is a non-root container,
# the UID and GID are always 1001
docker exec -u root $CONTAINER chown -R 1001:1001 ${DEST}
docker exec $CONTAINER mongorestore -h localhost:27017 --nsInclude $LAMBDADELBOT_DATABASE_NAME.* -u root -p $LAMBDADELBOT_DATABASE_ROOT_PASSWORD --authenticationDatabase admin $DEST
docker exec -u root $CONTAINER rm -R $DEST
