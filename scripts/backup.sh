#!/bin/bash
NOW=$(date +"%Y-%m-%d")
DUMPFOLDER="/bitnami/mongodb/dump${NOW}-$RANDOM"
echo "The database backup folder will be $DUMPFOLDER in the database container"

source  config/secrets_db.env
source  config/secrets_db_root.env     

DEST=$1
if [ -z "$DEST" ]; then
  echo "No dest folder on the host was provided as argument!"
  exit 1
else
  echo "Dest folder: $DEST"
fi


CONTAINER=$(docker ps --format "{{.Image}} {{.Names}}" | grep "lambdadelbot_database" | sed "s/^lambdadelbot_database //g")
if [ -z "$CONTAINER" ]; then
  echo "Did not find any database container"
  exit 1 
else
  echo "Found a database container named $CONTAINER"
fi

docker exec $CONTAINER mongodump -h localhost:27017 --db $LAMBDADELBOT_DATABASE_NAME -u root -p $LAMBDADELBOT_DATABASE_ROOT_PASSWORD --authenticationDatabase admin -o $DUMPFOLDER
docker cp $CONTAINER:$DUMPFOLDER $DEST
docker exec -u root $CONTAINER rm -R $DUMPFOLDER
