#!/usr/bin/env python3
#coding:utf-8
from pathlib import Path
from typing import cast, Literal, Mapping, Optional, Sequence, TypedDict, Union
import json, re, dataclasses

WORKDIR = Path("scripts")
INPUT_PATH = WORKDIR / "input.json"

class Input:
    path_hashes: tuple[str]

class PathHashDict(TypedDict):
    url:str
    hash_string:str

class PathKeyDict(TypedDict):
    key:str
    users:tuple[Optional[str], Optional[str], bool]

@dataclasses.dataclass
class PathKeyListing:
    key:str
    user1:None = None
    user2:None = None
    listing:Literal[True] = True

@dataclasses.dataclass
class PathKeySingle:
    key:str
    user1:str
    user2:None = None
    listing:Literal[False] = False

@dataclasses.dataclass
class PathKeyDouble:
    key:str
    user1:str
    user2:str
    listing:Literal[False] = False

PathKey = Union[PathKeyListing, PathKeySingle, PathKeyDouble]

def get_input() -> Mapping:
    txt = INPUT_PATH.read_text("utf-8")
    return json.loads(txt)

URL_EXTRACTOR = re.compile(r"^\/r\/KowzHOF\/wiki\/KOA\/(.+?)(?:\/\d+)?$")
LISTING_EXTRACTOR = re.compile(r"^listing_(.+)$")
SINGLE_EXTRACTOR = re.compile(r"^(KOA_[a-zA-Z0-9]+)_(.+)$")
DOUBLE_EXTRACTOR = re.compile(r"^(KOA_[a-zA-Z0-9]+)_(KOA_[a-zA-Z0-9]+)_(.+)$")

def extract_path_key(url:str) -> PathKey:
    match = URL_EXTRACTOR.fullmatch(url)
    if match is None:
        raise ValueError(f"Could not extract an inner path from URL {url}")
    path = match.group(1)
    print(f"Found path {path} from URL {url}")
    match = LISTING_EXTRACTOR.fullmatch(path)
    if match is not None:
        key = match.group(1)
        print(f"Found listing key {key} from path segment {path}")
        return PathKeyListing(key=key)
    match = DOUBLE_EXTRACTOR.fullmatch(path)
    if match is not None:
        user1, user2, key = match.groups()
        print(f"Found double key {key} for user1 {user1} and user2 {user2} from path segment {path}")
        return PathKeyDouble(key=key, user1=user1, user2=user2)
    match = SINGLE_EXTRACTOR.fullmatch(path)
    if match is not None:
        user1, key = match.groups()
        print(f"Found single key {key} for user1 {user1} from path segment {path}")
        return PathKeySingle(key=key, user1=user1)
    raise ValueError(f"Found no matching path key for path segment {path} from URL {url}")

def infer_path_keys_from_path_hashes(data:Mapping) -> Mapping[str, PathKey]:
    path_hashes = cast(list[PathHashDict], data["path_hashes"])
    urls = tuple(dico["url"] for dico in path_hashes)
    path_keys = tuple(extract_path_key(url) for url in urls)
    print(f"Inferred {len(path_keys)} path keys from path hashes")
    return {obj.key:obj for obj in path_keys}

def extract_path_key_from_dict(dico:PathKeyDict) -> PathKey:
    key, users = dico["key"], dico["users"]
    if users[2] is True:
        print(f"Found listing key {key} from path key dict {dico}")
        return PathKeyListing(key=key)
    elif users[1] is None:
        user1str = users[0]
        if user1str is None:
            raise ValueError(f"Expected user1 to be a string, not None in path key dict {dico}")
        print(f"Found single key {key} for user1 {user1str} from path key dict {dico}")
        return PathKeySingle(key=key, user1=user1str)
    user1str, user2str = users[0], users[1]
    if user1str is None or user2str is None:
        raise ValueError(f"Expected both user1 and user2 to be strings, not None in path key dict {dico}")
    print(f"Found double key {key} for user1 {user1str} and user2 {user2str} from path key dict {dico}")
    return PathKeyDouble(key=key, user1=user1str, user2=user2str)

def get_existing_path_keys(data:Mapping) -> Mapping[str, PathKey]:
    path_keys_dict = cast(list[PathKeyDict], data["path_keys"])
    path_keys = tuple(extract_path_key_from_dict(dico) for dico in path_keys_dict)
    print(f"Found {len(path_keys)} existing path keys")
    return {obj.key:obj for obj in path_keys}

def convert_path_key_to_dict(path_key:PathKey) -> PathKeyDict:
    if path_key.listing is True:
        return PathKeyDict(key=path_key.key, users=(None, None, True))
    elif path_key.user2 is None:
        return PathKeyDict(key=path_key.key, users=(path_key.user1, None, False))
    return PathKeyDict(key=path_key.key, users=(path_key.user1, path_key.user2, False))

QUERY = 'db.games.findOneAndUpdate({"_id":"KOA"}, {"$push":{"path_keys":{"$each":AAA}}}, {"returnNewDocument":true})'

def main() -> None:
    data = get_input()
    path_keys_existing = get_existing_path_keys(data)
    path_keys_inferred = infer_path_keys_from_path_hashes(data)
    path_keys_existing_set = frozenset(path_keys_existing)
    path_keys_inferred_set = frozenset(path_keys_inferred)
    path_keys_new = sorted(path_keys_inferred_set - path_keys_existing_set)
    path_keys_already_known = sorted(path_keys_inferred_set & path_keys_existing_set)
    print(f"There are {len(path_keys_new)} new path keys to add to existing ones: {path_keys_new}")
    print(f"There are {len(path_keys_already_known)} inferred path keys which were already known: {path_keys_already_known}")
    path_keys_new_dicts = [convert_path_key_to_dict(path_keys_inferred[key]) for key in path_keys_new]
    print(f"Converted the new path keys to JSON dictionaries ready for Mongo: {path_keys_new_dicts}")
    insertion = json.dumps(path_keys_new_dicts, ensure_ascii=False)
    query = QUERY.replace("AAA", insertion)
    print(f"The insertion query is the following one: {query}")
    breakpoint()

if __name__ == "__main__":
    main()
