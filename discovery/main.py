# coding: utf-8
#!/usr/bin/env python
from pathlib import Path
from typing import Mapping
import praw, json
SECRETS_FILEPATH = "../secrets/test.json"

def connect() -> praw.Reddit:
    secrets:Mapping[str, str] =  json.loads(Path(SECRETS_FILEPATH).read_text())
    secrets.pop("redirect_uri")
    reddit = praw.Reddit(**secrets)
    return reddit

def main() -> None:
    reddit = connect()
    print(reddit.subreddit("test").name)
    breakpoint()

if __name__ == "__main__":
    main()
