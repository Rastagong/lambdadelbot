# coding: utf-8

import random
import json
from typing import Mapping, Optional
import requests
from pathlib import Path
from urllib.parse import urlencode, urlparse, parse_qs, urlunparse
from http.server import BaseHTTPRequestHandler, HTTPServer
__all__ = ["performAuth"]


AUTH_URL = "https://www.reddit.com/api/v1/access_token"
URL_FILE = "url.txt"


def requestHandlerClosure(state, onGet):
    class RequestHandler(BaseHTTPRequestHandler):
        def do_GET(self) -> None:
            print(f"Entering the request handler's get method...")
            onGet(self, state)
    return RequestHandler

def onGetDecorator(receiver):
    def onGet(self:BaseHTTPRequestHandler, state:str) -> str:
        print(f"Entering the actual handler with state {state}")
        queryArgs = parse_qs(urlparse(self.path).query)
        obtainedState = queryArgs.get("state", [""])[0]
        error = queryArgs.get("error", [""])[0]
        code = queryArgs.get("code", [""])[0]
        ok = False
        if state != obtainedState:
            msg = f"Wrong state: obtained state {obtainedState}"
        elif error != "":
            msg = f"Error: {error}"
        elif code == "":
            msg = f"No code was retrieved"
        else:
            msg = f"Code successfully retrieved: {code}"
            receiver.add(code)
            ok = True
        print(msg)
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()
        self.wfile.write(bytes("<html><head><title>Title goes here.</title></head>", "utf-8"))
        self.wfile.write(bytes(f"<body><p>{msg}.</p>", "utf-8"))
        self.wfile.write(bytes("</body></html>", "utf-8"))
        if not ok:
            raise ValueError(msg)
    return onGet

def performAuth(secrets:dict) -> Optional[dict]:
    userAgent, clientID, clientSecret = secrets["user_agent"], secrets["client_id"], secrets["client_secret"] 
    redirectURI = secrets["redirect_uri"]
    port = urlparse(redirectURI).port
    state = str(random.randint(0, 65000))
    urlParams = {"client_id":clientID, "response_type":"code", "state":state, "redirect_uri":redirectURI, "duration":"permanent", "scope":"*"}
    query = urlencode(urlParams)
    url = urlunparse(("https", "www.reddit.com", "/api/v1/authorize/?", query, "", "")).replace(";", "")
    Path(URL_FILE).write_text(url)
    print(f"Now open the URL in url.txt {url}")
    receiver = set()
    httpd = HTTPServer(("", port), requestHandlerClosure(state, onGetDecorator(receiver)))
    httpd.handle_request()
    if not receiver:
        raise SystemExit
    code = next(iter(receiver))
    print(f"Code received: {code}")
    headers = {"User-Agent":userAgent}
    data = {"grant_type":"authorization_code", "code":code, "redirect_uri":redirectURI}
    auth = requests.auth.HTTPBasicAuth(clientID, clientSecret)
    r = requests.post(AUTH_URL, auth=auth, data=data, headers=headers)
    if r.status_code == 200:
        answer = r.json()
        secrets["access_token"] = answer["access_token"]
        secrets["refresh_token"] = answer["refresh_token"]
        print("All good, retrieved the access and refresh tokens!")
        return secrets
    else:
        print(f"Auth request failed: {r}")
        breakpoint()


