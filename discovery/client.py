# coding:utf-8
#!/usr/bin/env python
import sys, json
import requests
from typing import Mapping
from pathlib import Path
from urllib.parse import quote, urlencode
from auth import performAuth
INVITE_URL = "https://oauth.reddit.com/r/{0}/api/accept_moderator_invite"
IDENTITY_URL = "https://oauth.reddit.com//api/v1/me"
REVISIONS_URL = "https://oauth.reddit.com/r/{}/wiki/revisions/{}"
WIKI_PERMISSIONS_URL = "https://oauth.reddit.com/r/{}/wiki/settings/{}"
REFRESH_URL = "https://reddit.com/api/v1/access_token"
SECRETS_FILEPATH = "../config/test.json"
TOKEN_FILEPATH = "../secrets/token"
OUTPUT_FILEPATH = "output.json"

def getSecrets() -> Mapping:
    return json.loads(Path(SECRETS_FILEPATH).read_text())

def writeSecrets(secrets:dict) -> None:
    Path(SECRETS_FILEPATH).write_text(json.dumps(secrets, sort_keys=True, indent=2))

def dump(obj:object) -> str:
    return json.dumps(obj, indent=2)

def writeOutput(content:str) -> None:
    Path(OUTPUT_FILEPATH).write_text(content)

def acceptMod(subredditName:str) -> None:
    secrets = getSecrets()
    userAgent, clientID, clientSecret = secrets["user_agent"], secrets["client_id"], secrets["client_secret"] 
    token = secrets["access_token"]
    auth = requests.auth.HTTPBasicAuth(clientID, clientSecret)
    headers = {"Authorization":f"bearer {token}", "user-agent":userAgent}
    url = INVITE_URL.format(quote(subredditName))
    data = {"raw_json":1, "api_type":"json"}
    r = requests.post(url, headers=headers, data=data)
    Path(OUTPUT_FILEPATH).write_text(json.dumps(r.text, indent=2))
    if not r.ok:
        print("Failure!")
        breakpoint()
    else:
        print("Success")
    breakpoint()

def getLatestRev(subredditName:str, pageUrl:str) -> None:
    secrets = getSecrets()
    userAgent, clientID, clientSecret = secrets["user_agent"], secrets["client_id"], secrets["client_secret"] 
    token = secrets["access_token"]
    headers = {"Authorization":f"bearer {token}", "user-agent":userAgent}
    url = REVISIONS_URL.format(subredditName, quote(pageUrl))
    r = requests.get(url, headers=headers, data={"limit":1, "raw_json":1})
    print(url)
    if r.status_code != 200:
        breakpoint()
    output = dump(r.json())
    writeOutput(output)
    print(output)

def getWikiPermissions(subredditName:str, pageUrl:str) -> None:
    secrets = getSecrets()
    userAgent, clientID, clientSecret = secrets["user_agent"], secrets["client_id"], secrets["client_secret"] 
    token = secrets["access_token"]
    headers = {"Authorization":f"bearer {token}", "user-agent":userAgent}
    url = WIKI_PERMISSIONS_URL.format(subredditName, quote(pageUrl))
    r = requests.get(url, headers=headers)
    print(url)
    if r.status_code != 200:
        breakpoint()
    output = dump(r.json())
    writeOutput(output)
    print(output)


def wiki_write() -> None:
    secrets = getSecrets()
    userAgent, clientID, clientSecret = secrets["user_agent"], secrets["client_id"], secrets["client_secret"] 
    token = secrets["access_token"]
    headers = {"Authorization":f"bearer {token}", "user-agent":userAgent}
    data = {"page":"", "contents":""} #TODO: fix
    url = WIKI_WRITE_URL.format("")
    r = requests.post(url, headers=headers, data=data)

def getIdentity() -> None:
    secrets = getSecrets()
    userAgent, clientID, clientSecret = secrets["user_agent"], secrets["client_id"], secrets["client_secret"] 
    token = secrets["access_token"]
    headers = {"Authorization":f"bearer {token}", "user-agent":userAgent}
    url = IDENTITY_URL
    r = requests.get(url, headers=headers)
    if r.status_code != 200:
        breakpoint()
    output = dump(r.json())
    writeOutput(output)
    print(output)

def refresh() -> None:
    headers = {}
    secrets = getSecrets()
    userAgent, clientID, clientSecret = secrets["user_agent"], secrets["client_id"], secrets["client_secret"] 
    refreshToken = secrets["refresh_token"]
    headers = {"User-Agent":userAgent}
    data = {"grant_type":"refresh_token", "refresh_token":refreshToken}
    auth = requests.auth.HTTPBasicAuth(clientID, clientSecret)
    r = requests.post(REFRESH_URL, auth=auth, data=data, headers=headers)
    breakpoint()

if __name__ == "__main__":
    action = sys.argv[1]
    if action == "auth":
        writeSecrets(performAuth(getSecrets()))
    elif action == "refresh":
        refresh()
    elif action == "identity":
        getIdentity()
    elif action == "accept":
        acceptMod(sys.argv[2])
    elif action == "latestrev":
        getLatestRev(sys.argv[2], sys.argv[3])
    elif action == "wikiperms":
        getWikiPermissions(sys.argv[2], sys.argv[3])
    elif action == "wikiwrite":
        wiki_write()
